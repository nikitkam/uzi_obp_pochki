#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os

import sqlalchemy as sa
from sqlalchemy.engine import reflection
from sqlalchemy.schema import (
    MetaData,
    Table,
    DropTable,
    ForeignKeyConstraint,
    DropConstraint,
    )

import models

class SchemaManager(object):
    def __init__(self, echo_mode=False):
        super(SchemaManager, self).__init__()

        fallback_db_config = {
              'user': 'report_helper_user'
            , 'pass': 'eleventyseven'
            , 'host': 'localhost'
            , 'db_name': 'report_helper_db'
        }

        CONFIG_FILENAME = 'db_config.json'
        if os.path.exists(CONFIG_FILENAME):
            with open(CONFIG_FILENAME, 'rb') as f:
                db_config = json.load(f)
        else:
            print(u"[!] File %s does not exist! Use fall back config insted!" % CONFIG_FILENAME)
            db_config = fallback_db_config

        engine_url = 'postgresql+psycopg2://{0}:{1}@{2}/{3}'.format(
              db_config['user']
            , db_config['pass']
            , db_config['host']
            , db_config['db_name']
        )
        self.engine = sa.create_engine(engine_url, encoding='utf-8', echo=echo_mode)


    def create_db_schema(self):
        decl_base = models._getDeclBase()
        decl_base.metadata.create_all(self.engine)

    def drop_db_schema(self):
        ### reciept to drop everything
        ###
        ### https://bitbucket.org/zzzeek/sqlalchemy/wiki/UsageRecipes/DropEverything
        ###
        conn = self.engine.connect()
        trans = conn.begin()
        inspector = reflection.Inspector.from_engine(self.engine)
        metadata = MetaData()

        tbs = []
        all_fks, fks = [], []

        for table_name in inspector.get_table_names():
            fks = []
            for fk in inspector.get_foreign_keys(table_name):
                if not fk['name']:
                    continue
                fks.append(
                    ForeignKeyConstraint((),(),name=fk['name'])
                )

            t = Table(table_name,metadata,*fks)
            tbs.append(t)
            all_fks.extend(fks)

        for fkc in all_fks:
            conn.execute(DropConstraint(fkc))

        for table in tbs:
            conn.execute(DropTable(table))

        trans.commit()
        

if __name__ == '__main__':
    pass

