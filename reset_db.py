#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import sys

from PySide import QtGui
from PySide.QtCore import QRegExp

import session_manager
import schema_manager
from models import Examination


ssm = schema_manager.SchemaManager()
print('starting ...')
#ssm.drop_db_schema()
print('[+] old schema dropped')
ssm.create_db_schema()
print('[+] new schema created')

sm = session_manager.SessionManager()
session = sm.createNewSession()
print('[+] default session created')


ex_today = datetime.date.today()
ex_time  = [datetime.time(13, 00)]
ex_type = [233]

examinations = []
doctors = [u"Кравченко Шмак Брагимович"]
patients = [u'Эмбарго Куба Советовна']
sex_p  = [u'м']

for i in range(len(ex_time)):
    ex = Examination(
                ex_date = ex_today,
                ex_time = ex_time[i],
                ex_type = ex_type[i],
                doctor_name  = doctors[i],
                patient_name = patients[i],
                sex = sex_p[i]
            )
    examinations.append(ex)


session.add_all(examinations)
session.commit()
print('[+] data written')
