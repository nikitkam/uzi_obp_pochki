#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, datetime, os

from PySide import QtGui, QtCore

from inter_dialog import  InterDialog
from brushnaya.obp_pochki import Obp_pochki
from brushnaya.obp_pochki_deti import Obp_pochki_deti
from brushnaya.pech_zhel_podzh import Pech_zhelch_podzh
from urologia.mochevoi import Mochevoi
from urologia.urologia_full import UrologiaFull
from urologia.poch import Pochki
from urologia.predst_moch import PredstMoch
from shitovidka.shitovidka_nodes import ShitovidkaNodes
from shitovidka.shitovidka_no_nodes import ShitovidkaNoNodes
from serdce.obsled_serdce import ObsledSerdceWindow

import _common_parts.common as common

# словарь с настройками. Реализует основные фунции обычного dict +
# заполняется при старте из файла с кофигурацией, имеет интерфейс
# записи в этот же файл: object.save()
from settings import SettingsDict

from MainMenuWindowDesign import Ui_MainMenuWindow
from ReportViewerWindowDesign import Ui_ReportViewer

from models import Examination
import session_manager

def ClearTable(table_widget):
    table_widget.clearContents()
    table_widget.setRowCount(0)
    table_widget.setEnabled(False)
def to_py_date(qtDate):
    return datetime.date(qtDate.year(), qtDate.month(), qtDate.day())
def to_py_time(qtTime):
    return datetime.time(qtTime.hour(), qtTime.minute(), qtTime.second())


class MainMenuWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainMenuWindow, self).__init__()

        self._MENU_INDEX = 0
        self._SETTINGS_INDEX = 1
        self._DATABASE_INDEX = 2
        self.obp_pochki = None
        self.pech_zhel_podzh = None
        self.obp_pochki_deti = None
        self.mochevoi = None
        self.urologia_full = None
        self.shitovidka_nodes = None
        self.shitovidka_no_nodes = None

        # переменные, используемые БД
        self.db_session = None
        self._exam_res = None

        self._settings = SettingsDict(self)
        self.initUI()

# ======================== MenuWidget  ========================

    def onOBPPochkiPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.obp_pochki = Obp_pochki(self._settings)
        self.obp_pochki.showMaximized()

    def onOBPPochkiDetiPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.obp_pochki_deti = Obp_pochki_deti(self._settings)
        self.obp_pochki_deti.showMaximized()

    def onPechenPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.pech_zhel_podzh = Pech_zhelch_podzh(self._settings)
        self.pech_zhel_podzh.showMaximized()

    def onMochevoiPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.mochevoi = Mochevoi(self._settings)
        self.mochevoi.showMaximized()

    def onPochNadpPredsMochPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.urologia_full = UrologiaFull(self._settings)
        self.urologia_full.showMaximized()

    def onPochNadpPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.urologia_full = Pochki(self._settings)
        self.urologia_full.showMaximized()

    def onPredstMochPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.urologia_full = PredstMoch(self._settings)
        self.urologia_full.showMaximized()


    def onShitZhelPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.shitovidka_nodes = ShitovidkaNodes(self._settings)
        self.shitovidka_nodes.showMaximized()


    def onShitZhelBezUzlovPBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.shitovidka_no_nodes = ShitovidkaNoNodes(self._settings)
        self.shitovidka_no_nodes.showMaximized()


    def onSerdcePBClicked(self):
        inter_dialog_ = InterDialog(self._settings)
        ret_status = inter_dialog_.exec_()
        if ret_status:
            return
        self.serdce = ObsledSerdceWindow(self._settings)
        self.serdce.showMaximized()

# ===================== SettingsWidget =========================

    def onSettingsPBClicked(self):
        self.ui.stackedWidget.setCurrentIndex(self._SETTINGS_INDEX)

    def onBackPBClicked(self):
        self.ui.stackedWidget.setCurrentIndex(self._MENU_INDEX)

    def FillDoctorCB(self, doctors):
        """ Заполнение комбо-бокса с именами врачей """
        if type(doctors) != dict:
            print('error: typecheck failed!')
            return

        cb = self.ui.DoctorCB
        # чистим ComboBox
        cb.clear()
        # заполняем
        for index, doctor in doctors.iteritems():
            cb.insertItem(index, doctor)

        ci = self._settings.get('_current_doctor_index')
        if ci:
            cb.setCurrentIndex(ci)


    def GetDoctorNames(self):
        doctor_cb = self.ui.DoctorCB
        # запоминаем выбранного доктора
        self._settings['_current_doctor_index'] = doctor_cb.currentIndex()
        return common.get_combobox_values(doctor_cb)


    def onSavePBClicked(self):
        self._settings['DoctorName'] = self.ui.DoctorCB.currentText()
        self._settings['Uzi'] = self.ui.UziLE.text()
        self._settings['Shapka'] = self.ui.textEditShapka.toHtml()
        self._settings['DoctorNames'] = self.GetDoctorNames()
        self._settings.save()
        self.ui.stackedWidget.setCurrentIndex(self._MENU_INDEX)

    def onAddDoctorPBClicked(self):
        text, ok = QtGui.QInputDialog.getText(self, u'Добавление нового врача',
                                              u'ФИО')
        if not ok:
            return

        cb = self.ui.DoctorCB
        BOTTOM_INDEX = cb.count()
        cb.insertItem(BOTTOM_INDEX, text)
        cb.setCurrentIndex(BOTTOM_INDEX)


    def onRemoveDoctorPBClicked(self):
        ci = self.ui.DoctorCB.currentIndex()
        self.ui.DoctorCB.removeItem(ci)


# ==============================================================
# database tab
    def onBackDatabasePBClicked(self):
        self.ui.stackedWidget.setCurrentIndex(self._MENU_INDEX)
        if self.db_session:
            self.db_session.close()
        html_report_path = "tmp.html"
        if os.path.exists(html_report_path):
            os.remove(html_report_path)

    def onDatabasePBClicked(self):
        self.ui.stackedWidget.setCurrentIndex(self._DATABASE_INDEX)
        sm = session_manager.SessionManager()
        self.db_session = sm.createNewSession()


    def oneCellDoubleClicked(self, row=None):
        exam = self._exam_res[row]
        report_html = exam.report_html_blob
        if not report_html:
            QtGui.QMessageBox.warning(self,u'Ошибка!', u'Файл обследования не найден!')
            return

        self.report_viewer_window = ReportViewerWindow(report_html)
        self.report_viewer_window.show()



    def ShowExaminationResults(self, results):
        res_table = self.ui.eSearchResultTableWidget
        res_table.setEnabled(True)
        self.ui.eStatusMessageL.setText(u'')

        #Enable/Disable view survey buttons
        #self.ui.eFilesViewPushB.setDisabled(len(results) == 0)
        self.ui.eReportViewPushB.setDisabled(len(results) == 0)

        if len(results) == 0:
            error_text = u"<font color='Red'>Ничего не найдено!</font>"
            self.ui.eStatusMessageL.setText(error_text)
            ClearTable(res_table)
            return

        res_table.setRowCount(len(results))
        for i, exam in enumerate(results):
            if not exam:
                continue
            res_table.setItem(i, 0, QtGui.QTableWidgetItem(unicode(exam.ex_date)))

            time_str = unicode(exam.ex_time.strftime("%H:%M"))
            res_table.setItem(i, 1, QtGui.QTableWidgetItem(time_str))

            if exam.ex_type:
                res_table.setItem(i, 2, QtGui.QTableWidgetItem(exam.ex_type))
            else:
                res_table.setItem(i, 2, QtGui.QTableWidgetItem(u''))

            if exam.doctor_name:
                res_table.setItem(i, 3, QtGui.QTableWidgetItem(exam.doctor_name))
            else:
                res_table.setItem(i, 3, QtGui.QTableWidgetItem(u''))

            if exam.patient_name:
                res_table.setItem(i, 4, QtGui.QTableWidgetItem(exam.patient_name))
            else:
                res_table.setItem(i, 4, QtGui.QTableWidgetItem(u''))

        res_table.resizeColumnsToContents()
        res_table.horizontalHeader().setStretchLastSection(True)


    def oneSearchPushBPressed(self):
        # collect data for query
        start_date = to_py_date(self.ui.eDateEdit_from.date())
        end_date = to_py_date(self.ui.eDateEdit_to.date())
        start_time = to_py_time(self.ui.eExamTimeTE.time())
        end_time = to_py_time(self.ui.eExamTimeTE_2.time())
        p_last_name = self.ui.eLastNameLE.text().strip()
        d_last_name = self.ui.eLastName2LE.text().strip()
        exam_number = self.ui.eProtocolNumLE.text().strip()

        q = self.db_session.query(Examination)
        if self.ui.checkBoxSearchDate.isChecked() == True:
            q = q.filter(Examination.ex_date >= start_date )
            q = q.filter(Examination.ex_date <= end_date )

        if self.ui.checkBoxSearchTime.isChecked() == True:
            q = q.filter(Examination.ex_time >= start_time)
            q = q.filter(Examination.ex_time <= end_time)

        if p_last_name:
            q = q.filter(Examination.patient_name.like(u"%{0}%".format(p_last_name)))

        if exam_number:
            q = q.filter(Examination.ex_type.like(u"%{0}%".format(exam_number)))

        if d_last_name:
            q = q.filter(Examination.doctor_name.like(u"%{0}%".format(d_last_name)))

        self._exam_res = q.order_by(
            Examination.ex_date
        ).all()

        #print(u'Found examinations: ')
        #for x in self._exam_res:
        #    print(x)

        self.ShowExaminationResults(self._exam_res)


    def oneReportViewPushBPressed(self):
        #get the row highlighted and call doubleclick func
        widget = self.ui.eSearchResultTableWidget
        self.oneCellDoubleClicked(widget.currentRow())


    def onclearFormExamsPBPressed(self):
        clearListLE = [self.ui.eProtocolNumLE, self.ui.eLastNameLE]
        for inputForm in clearListLE:
            inputForm.clear()




# ==============================================================
    def initUI(self):
        self.ui = Ui_MainMenuWindow()
        self.ui.setupUi(self)

        if len(self._settings):
            # "безопасный" доступ к элементам словаря.
            # если элемента в словаре нет, создаем пустой (для
            # случая, когда добавляются новые настройки)
            dn = self._settings.get('DoctorName')
            if dn:
                pass
                # self.ui.DoctorLE.setText(dn)
            else:
                self._settings['DoctorName'] = None

            uzi = self._settings.get('Uzi')
            if uzi:
                self.ui.UziLE.setText(uzi)
            else:
                self._settings['Uzi'] = None

            shapka = self._settings.get('Shapka')
            if shapka:
                self.ui.textEditShapka.setHtml(shapka)
            else:
                self._settings['Shapka'] = None

            dns = self._settings.get('DoctorNames')
            if dns:
                self.FillDoctorCB(dns)
            else:
                self._settings['DoctorNames'] = None
        else:
            self._settings['DoctorName'] = self.ui.DoctorCB.currentText()
            self._settings['Uzi'] = self.ui.UziLE.text()
            self._settings['Shapka'] = self.ui.textEditShapka.toHtml()
            self._settings['DoctorNames'] = self.GetDoctorNames()

        exitAction = QtGui.QAction(u'Exit', self)
        exitAction.setShortcut(u'Ctrl+Q')
        exitAction.setStatusTip(u'Exit application')
        exitAction.triggered.connect(self.close)

        fileMenu = self.ui.menubar.addMenu(u'&File')
        fileMenu.addAction(exitAction)

        # устанавливаем на виджете поиска по БД поле "дата обследования по"
        # в текущую дату
        self.ui.eDateEdit_to.setDate(QtCore.QDate.currentDate())

        # принудительно устанавливаем по дефолту вкладку с меню,
        # даже если в дизайнере осталась выбранной вкладка настроек
        self.ui.stackedWidget.setCurrentIndex(self._MENU_INDEX)

        #signals
        self.ui.OBPPochkiPB.clicked.connect(self.onOBPPochkiPBClicked)
        self.ui.OBPPochkiDetiPB.clicked.connect(self.onOBPPochkiDetiPBClicked)
        self.ui.PechenPB.clicked.connect(self.onPechenPBClicked)
        self.ui.MochPuzPB.clicked.connect(self.onMochevoiPBClicked)
        self.ui.PochNadpPredsMochPB.clicked.connect(self.onPochNadpPredsMochPBClicked)
        self.ui.PochNadpPB.clicked.connect(self.onPochNadpPBClicked)
        self.ui.PredstMochPB.clicked.connect(self.onPredstMochPBClicked)
        self.ui.ShitZhelPB.clicked.connect(self.onShitZhelPBClicked)
        self.ui.ShitZhelBezUzlovPB.clicked.connect(self.onShitZhelBezUzlovPBClicked)
        self.ui.SerdcePB.clicked.connect(self.onSerdcePBClicked)

        self.ui.ExitPB.clicked.connect(self.close)

        self.ui.SettingsPB.clicked.connect(self.onSettingsPBClicked)
        self.ui.DatabasePB.clicked.connect(self.onDatabasePBClicked)
        self.ui.BackPB.clicked.connect(self.onBackPBClicked)
        self.ui.SavePB.clicked.connect(self.onSavePBClicked)

        # сигналы на вкладке "Настройки"
        self.ui.AddDoctorPB.clicked.connect(self.onAddDoctorPBClicked)
        self.ui.RemoveDoctorPB.clicked.connect(self.onRemoveDoctorPBClicked)

        # сигналы на вкладке "Поиск по БД"
        self.ui.pushButtonBack.clicked.connect(self.onBackDatabasePBClicked)
        self.ui.clearFormExamsPB.pressed.connect(self.onclearFormExamsPBPressed)
        self.ui.eReportViewPushB.pressed.connect(self.oneReportViewPushBPressed)
        self.ui.eSearchPushB.pressed.connect(self.oneSearchPushBPressed)


class ReportViewerWindow(QtGui.QMainWindow):
    def __init__(self, html=None):
        super(ReportViewerWindow, self).__init__()
        self.ui = Ui_ReportViewer()
        self.ui.setupUi(self)
        self.ui.textBrowser.setHtml(html.decode('utf-8'))


def main():
    app = QtGui.QApplication(sys.argv)
    main_menu_window = MainMenuWindow()
    main_menu_window.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

