#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import base64
from PySide import QtCore, QtGui
from PIL import Image, ImageDraw, ImageFont
from SerdceWidgetDesign import Ui_SerdceWidget


# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common

from _common_parts.final_report_widget import FinalReportWidget



class SerdceWidget(QtGui.QWidget):
    def __init__(self):
        super(SerdceWidget, self).__init__()

        self.ui = None

        self._CLICKER_INDEX = 0
        self._TEXT_INDEX = 1

        self.initUI()


        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # сигналы
        self.ui.pushButtonGotovoSerdce.clicked.connect(self.onGotovoPBClicked)
        self.ui.tabWidget.currentChanged.connect(self.onTabChanged)
        # Автоматические вычисления
        QtCore.QObject.connect(self.ui.lineEdit035,QtCore.SIGNAL("textChanged(const QString&)"), self.check1)
        QtCore.QObject.connect(self.ui.lineEdit038,QtCore.SIGNAL("textChanged(const QString&)"), self.check2)
        QtCore.QObject.connect(self.ui.lineEdit049,QtCore.SIGNAL("textChanged(const QString&)"), self.check3)
        QtCore.QObject.connect(self.ui.lineEdit052,QtCore.SIGNAL("textChanged(const QString&)"), self.check3)
        QtCore.QObject.connect(self.ui.lineEdit055,QtCore.SIGNAL("textChanged(const QString&)"), self.check4)
        QtCore.QObject.connect(self.ui.lineEdit055,QtCore.SIGNAL("textChanged(const QString&)"), self.check5)
        QtCore.QObject.connect(self.ui.lineEdit007,QtCore.SIGNAL("textChanged(const QString&)"), self.check5)
        QtCore.QObject.connect(self.ui.lineEdit003,QtCore.SIGNAL("textChanged(const QString&)"), self.check6)
        QtCore.QObject.connect(self.ui.lineEdit061,QtCore.SIGNAL("textChanged(const QString&)"), self.check6)
        QtCore.QObject.connect(self.ui.lineEdit035,QtCore.SIGNAL("textChanged(const QString&)"), self.check7)
        QtCore.QObject.connect(self.ui.lineEdit038,QtCore.SIGNAL("textChanged(const QString&)"), self.check7)

    def check7(self):
        self.ui.lineEdit064.clear()
        curVal = 0
        try:
            curVal = round(((float(self.ui.lineEdit035.text())-float(self.ui.lineEdit038.text()))/float(self.ui.lineEdit038.text())),2)*100
        except ValueError:
            print("Warning: inputed value is not a float value")       
        if (float(curVal) > 0 ):
            self.ui.lineEdit064.insert(str(curVal))
        else:
            self.ui.lineEdit064.insert('0')


    def check6(self):
        self.ui.lineEdit067.clear()
        curVal = 0
        try:
            curVal = round((float(self.ui.lineEdit061.text())/float(self.ui.lineEdit003.text())),2)
        except ValueError:
            print("Warning: inputed value is not a float value")       
        if (float(curVal) > 0 ):
            self.ui.lineEdit067.insert(str(curVal))
        else:
            self.ui.lineEdit067.insert('0')


    def check5(self):
        self.ui.lineEdit061.clear()
        curVal = 0
        try:
            curVal = round((float(self.ui.lineEdit055.text())*float(self.ui.lineEdit007.text())))
        except ValueError:
            print("Warning: inputed value is not a float value")       
        if (float(curVal) > 0 ):
            self.ui.lineEdit061.insert(str(curVal))
        else:
            self.ui.lineEdit061.insert('0')

    def check4(self):
        self.ui.lineEdit058.clear()
        curVal = 0
        try:
            curVal = round((float(self.ui.lineEdit055.text())/(float(self.ui.lineEdit049.text())))*100)
        except ValueError:
            print("Warning: inputed value is not a float value")       
        if (float(curVal) > 0 ):
            self.ui.lineEdit058.insert(str(curVal))
        else:
            self.ui.lineEdit058.insert('0')

    def check3(self):
        self.ui.lineEdit055.clear()
        curVal = 0
        try:
            curVal = float(self.ui.lineEdit049.text())-float(self.ui.lineEdit052.text())
        except ValueError:
            print("Warning: inputed value is not a float value")       
        if (float(curVal) > 0 ):
            self.ui.lineEdit055.insert(str(curVal))
        else:
            self.ui.lineEdit055.insert('0')

    def check2(self):
        self.ui.lineEdit052.clear()
        curVal = 0
    	if self.ui.lineEdit038.text() <= 0:
            return(0)
        curVal = 0 
        try:
            curVal = float(self.ui.lineEdit038.text())/10
        except ValueError:
            print("Warning: inputed value is not a float value")
        curVal = round((7/(2.4+curVal))*curVal*curVal*curVal)
        if (float(curVal) > 0 ):
            self.ui.lineEdit052.insert(str(curVal))
        else:
            self.ui.lineEdit052.insert('0')
    	
    def check1(self):
        self.ui.lineEdit049.clear() 
        curVal = 0
    	if self.ui.lineEdit035.text() <= 0:
            return(0)
        curVal = 0 
        try:
            curVal = float(self.ui.lineEdit035.text())/10
        except ValueError:
            print("Warning: inputed value is not a float value")
        curVal = round((7/(2.4+curVal))*curVal*curVal*curVal)
        if (float(curVal) > 0 ):
            self.ui.lineEdit049.insert(str(curVal))
        else:
            self.ui.lineEdit049.insert('0')
    	
    def updateScheme(self):
        coordList = [(140,10), (20,100), (25,185), (140,260), (260,170), (260,85), (140,55), (60,105), (60,160), (140,215), (220,160), (210,105), (115,110), (110,160), (160,160), (160,110)]
        im = Image.open("/home/medical/Documents/uzi_obp_pochki/serdce/background.bmp")
        font_filename = "/home/medical/Documents/uzi_obp_pochki/serdce/FreeMonoBold.ttf"
        if not os.path.exists(font_filename):
            raise IOError('No such file %s' % font_filename)
        font = ImageFont.truetype(font_filename, 25)
        for i in range(1, 17):
            comboBox = self.ui.__getattribute__("comboBox_"+str(i))
            digit = comboBox.currentIndex()
            if digit == 0:
                continue
            draw = ImageDraw.Draw(im)
            draw.text(coordList[i-1], str(digit), fill=256, font=font)
            del draw

        # convert image to base64 html and pass to report
        img_name = "image.bmp"
        im.save(img_name)
        img_filename = os.path.abspath(img_name)
        if not os.path.exists(img_filename):
            print('Warning: %s is not found' % img_filename)
            encoded_string = base64.b64encode('error')
        else:
            with open(img_filename, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
        # удаляем image.bmp
        if os.path.exists(img_filename):
            os.remove(img_filename)

        #imageBase64
        imageHtml = '<p style="margin-bottom: 0in; line-height: 100%"><img src="data:image/jpeg;base64,' + encoded_string + '" name="Image1" align="left" width="300" height="300" border="0"><br></p>'
        return imageHtml



    @QtCore.Slot(int)
    def onTabChanged(self, index):
        if index == self._TEXT_INDEX:
            self.updateReportPreview()


    def switchTabToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)



    def onGotovoPBClicked(self):
        self.updateReportPreview()
        self.switchTabToText()


    def updateReportPreview(self):
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        repTextHtml = common.replace_lineedits(repTextHtml
                                               , ui=self.ui
                                               , regexp=r"lineEdit[0-9]{3}"
                                               , debug=False)

        repTextHtml = common.replace_radiobuttons(repTextHtml
                                                  , ui=self.ui
                                                  , regexp=r"radioButton[0-9]{3}"
                                                  , debug=False)

        repTextHtml = common.replace_comboboxes(repTextHtml
                                                , ui=self.ui
                                                , regexp=r"comboBox[0-9]{3}"
                                                , debug=False)

        repTextHtml = common.replace_textedits(repTextHtml
                                               , ui=self.ui
                                               , regexp=r"textEdit[0-9]{3}"
                                               , debug=False)

        imageHtml = self.updateScheme()
        repTextHtml = repTextHtml.replace("imageBase64", imageHtml)

        self.ui.textBrowserReport.insertHtml(repTextHtml)



    def initUI(self):
        self.ui = Ui_SerdceWidget()
        self.ui.setupUi(self)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = SerdceWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
