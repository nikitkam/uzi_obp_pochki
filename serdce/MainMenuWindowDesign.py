# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainMenuWindowDesign.ui'
#
# Created: Fri Apr 17 10:22:16 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainMenuWindow(object):
    def setupUi(self, MainMenuWindow):
        MainMenuWindow.setObjectName("MainMenuWindow")
        MainMenuWindow.resize(865, 654)
        font = QtGui.QFont()
        font.setFamily("Ubuntu")
        font.setPointSize(11)
        MainMenuWindow.setFont(font)
        MainMenuWindow.setLocale(QtCore.QLocale(QtCore.QLocale.Byelorussian, QtCore.QLocale.Belarus))
        self.centralwidget = QtGui.QWidget(MainMenuWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.stackedWidget = QtGui.QStackedWidget(self.centralwidget)
        self.stackedWidget.setObjectName("stackedWidget")
        self.MenuWidget = QtGui.QWidget()
        self.MenuWidget.setObjectName("MenuWidget")
        self.gridLayout = QtGui.QGridLayout(self.MenuWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.SettingsPB = QtGui.QPushButton(self.MenuWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.SettingsPB.setFont(font)
        self.SettingsPB.setObjectName("SettingsPB")
        self.gridLayout.addWidget(self.SettingsPB, 4, 0, 1, 1)
        self.ExitPB = QtGui.QPushButton(self.MenuWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.ExitPB.setFont(font)
        self.ExitPB.setObjectName("ExitPB")
        self.gridLayout.addWidget(self.ExitPB, 5, 0, 1, 1)
        self.DatabasePB = QtGui.QPushButton(self.MenuWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.DatabasePB.setFont(font)
        self.DatabasePB.setObjectName("DatabasePB")
        self.gridLayout.addWidget(self.DatabasePB, 3, 0, 1, 1)
        self.line_8 = QtGui.QFrame(self.MenuWidget)
        self.line_8.setFrameShape(QtGui.QFrame.VLine)
        self.line_8.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_8.setObjectName("line_8")
        self.gridLayout.addWidget(self.line_8, 0, 1, 1, 1)
        self.line_9 = QtGui.QFrame(self.MenuWidget)
        self.line_9.setFrameShape(QtGui.QFrame.HLine)
        self.line_9.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_9.setObjectName("line_9")
        self.gridLayout.addWidget(self.line_9, 2, 0, 1, 1)
        self.line = QtGui.QFrame(self.MenuWidget)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 1, 1, 4, 1)
        self.SerdcePB = QtGui.QPushButton(self.MenuWidget)
        self.SerdcePB.setEnabled(True)
        self.SerdcePB.setMinimumSize(QtCore.QSize(0, 120))
        font = QtGui.QFont()
        font.setFamily("Ubuntu")
        font.setPointSize(11)
        self.SerdcePB.setFont(font)
        self.SerdcePB.setObjectName("SerdcePB")
        self.gridLayout.addWidget(self.SerdcePB, 1, 2, 1, 1)
        self.stackedWidget.addWidget(self.MenuWidget)
        self.SettingsWidget = QtGui.QWidget()
        self.SettingsWidget.setObjectName("SettingsWidget")
        self.gridLayout_2 = QtGui.QGridLayout(self.SettingsWidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.textEditShapka = QtGui.QTextEdit(self.SettingsWidget)
        self.textEditShapka.setLineWrapColumnOrWidth(2)
        self.textEditShapka.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByKeyboard|QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextBrowserInteraction|QtCore.Qt.TextEditable|QtCore.Qt.TextEditorInteraction|QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.textEditShapka.setObjectName("textEditShapka")
        self.gridLayout_2.addWidget(self.textEditShapka, 2, 1, 2, 3)
        self.UziL = QtGui.QLabel(self.SettingsWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.UziL.setFont(font)
        self.UziL.setObjectName("UziL")
        self.gridLayout_2.addWidget(self.UziL, 1, 0, 1, 1)
        self.SavePB = QtGui.QPushButton(self.SettingsWidget)
        self.SavePB.setObjectName("SavePB")
        self.gridLayout_2.addWidget(self.SavePB, 4, 4, 1, 1)
        self.BackPB = QtGui.QPushButton(self.SettingsWidget)
        self.BackPB.setObjectName("BackPB")
        self.gridLayout_2.addWidget(self.BackPB, 4, 3, 1, 1)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem, 3, 0, 1, 1)
        self.UziLE = QtGui.QLineEdit(self.SettingsWidget)
        self.UziLE.setLocale(QtCore.QLocale(QtCore.QLocale.Byelorussian, QtCore.QLocale.Belarus))
        self.UziLE.setObjectName("UziLE")
        self.gridLayout_2.addWidget(self.UziLE, 1, 1, 1, 1)
        self.ShapkaL = QtGui.QLabel(self.SettingsWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.ShapkaL.setFont(font)
        self.ShapkaL.setObjectName("ShapkaL")
        self.gridLayout_2.addWidget(self.ShapkaL, 2, 0, 1, 1)
        self.DoctorL2 = QtGui.QLabel(self.SettingsWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.DoctorL2.setFont(font)
        self.DoctorL2.setObjectName("DoctorL2")
        self.gridLayout_2.addWidget(self.DoctorL2, 0, 0, 1, 1)
        self.DoctorCB = QtGui.QComboBox(self.SettingsWidget)
        self.DoctorCB.setObjectName("DoctorCB")
        self.DoctorCB.addItem("")
        self.gridLayout_2.addWidget(self.DoctorCB, 0, 1, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.AddDoctorPB = QtGui.QPushButton(self.SettingsWidget)
        self.AddDoctorPB.setMaximumSize(QtCore.QSize(30, 40))
        self.AddDoctorPB.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/iconPrefix/plus_sign.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.AddDoctorPB.setIcon(icon)
        self.AddDoctorPB.setObjectName("AddDoctorPB")
        self.horizontalLayout_2.addWidget(self.AddDoctorPB)
        self.RemoveDoctorPB = QtGui.QPushButton(self.SettingsWidget)
        self.RemoveDoctorPB.setMaximumSize(QtCore.QSize(30, 40))
        self.RemoveDoctorPB.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/iconPrefix/minus_sign.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.RemoveDoctorPB.setIcon(icon1)
        self.RemoveDoctorPB.setObjectName("RemoveDoctorPB")
        self.horizontalLayout_2.addWidget(self.RemoveDoctorPB)
        self.gridLayout_2.addLayout(self.horizontalLayout_2, 0, 3, 1, 1)
        self.stackedWidget.addWidget(self.SettingsWidget)
        self.page = QtGui.QWidget()
        self.page.setObjectName("page")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.page)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.widget = QtGui.QWidget(self.page)
        self.widget.setObjectName("widget")
        self.gridLayout_3 = QtGui.QGridLayout(self.widget)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.eDiagnosisL_3 = QtGui.QLabel(self.widget)
        self.eDiagnosisL_3.setEnabled(True)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.eDiagnosisL_3.setFont(font)
        self.eDiagnosisL_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.eDiagnosisL_3.setObjectName("eDiagnosisL_3")
        self.gridLayout_3.addWidget(self.eDiagnosisL_3, 1, 2, 1, 1)
        self.eTitle1L = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setWeight(75)
        font.setItalic(False)
        font.setUnderline(False)
        font.setStrikeOut(False)
        font.setBold(True)
        self.eTitle1L.setFont(font)
        self.eTitle1L.setObjectName("eTitle1L")
        self.gridLayout_3.addWidget(self.eTitle1L, 0, 0, 1, 4)
        self.eDateEdit_from = QtGui.QDateEdit(self.widget)
        self.eDateEdit_from.setDateTime(QtCore.QDateTime(QtCore.QDate(2014, 12, 31), QtCore.QTime(0, 0, 0)))
        self.eDateEdit_from.setDate(QtCore.QDate(2014, 12, 31))
        self.eDateEdit_from.setTime(QtCore.QTime(0, 0, 0))
        self.eDateEdit_from.setCalendarPopup(True)
        self.eDateEdit_from.setObjectName("eDateEdit_from")
        self.gridLayout_3.addWidget(self.eDateEdit_from, 1, 1, 1, 1)
        self.eExamTimeTE = QtGui.QTimeEdit(self.widget)
        self.eExamTimeTE.setCalendarPopup(True)
        self.eExamTimeTE.setObjectName("eExamTimeTE")
        self.gridLayout_3.addWidget(self.eExamTimeTE, 2, 1, 1, 1)
        self.eDiagnosisL_2 = QtGui.QLabel(self.widget)
        self.eDiagnosisL_2.setEnabled(True)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.eDiagnosisL_2.setFont(font)
        self.eDiagnosisL_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.eDiagnosisL_2.setObjectName("eDiagnosisL_2")
        self.gridLayout_3.addWidget(self.eDiagnosisL_2, 2, 2, 1, 1)
        self.eLastNameL = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.eLastNameL.setFont(font)
        self.eLastNameL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.eLastNameL.setObjectName("eLastNameL")
        self.gridLayout_3.addWidget(self.eLastNameL, 3, 0, 1, 1)
        self.eLastNameLE = QtGui.QLineEdit(self.widget)
        self.eLastNameLE.setObjectName("eLastNameLE")
        self.gridLayout_3.addWidget(self.eLastNameLE, 3, 1, 1, 2)
        self.eSearchPushB = QtGui.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.eSearchPushB.setFont(font)
        self.eSearchPushB.setObjectName("eSearchPushB")
        self.gridLayout_3.addWidget(self.eSearchPushB, 6, 3, 1, 1)
        self.eTitle2L = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setBold(True)
        self.eTitle2L.setFont(font)
        self.eTitle2L.setObjectName("eTitle2L")
        self.gridLayout_3.addWidget(self.eTitle2L, 7, 0, 1, 1)
        self.eSearchResultTableWidget = QtGui.QTableWidget(self.widget)
        self.eSearchResultTableWidget.setEnabled(False)
        self.eSearchResultTableWidget.setObjectName("eSearchResultTableWidget")
        self.eSearchResultTableWidget.setColumnCount(5)
        self.eSearchResultTableWidget.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.eSearchResultTableWidget.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.eSearchResultTableWidget.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.eSearchResultTableWidget.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.eSearchResultTableWidget.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.eSearchResultTableWidget.setHorizontalHeaderItem(4, item)
        self.eSearchResultTableWidget.horizontalHeader().setDefaultSectionSize(125)
        self.eSearchResultTableWidget.horizontalHeader().setStretchLastSection(True)
        self.gridLayout_3.addWidget(self.eSearchResultTableWidget, 8, 0, 1, 5)
        self.eReportViewPushB = QtGui.QPushButton(self.widget)
        self.eReportViewPushB.setEnabled(False)
        self.eReportViewPushB.setObjectName("eReportViewPushB")
        self.gridLayout_3.addWidget(self.eReportViewPushB, 9, 4, 1, 1)
        self.eExamTimeTE_2 = QtGui.QTimeEdit(self.widget)
        self.eExamTimeTE_2.setTime(QtCore.QTime(23, 59, 0))
        self.eExamTimeTE_2.setCalendarPopup(True)
        self.eExamTimeTE_2.setObjectName("eExamTimeTE_2")
        self.gridLayout_3.addWidget(self.eExamTimeTE_2, 2, 3, 1, 1)
        self.clearFormExamsPB = QtGui.QPushButton(self.widget)
        self.clearFormExamsPB.setObjectName("clearFormExamsPB")
        self.gridLayout_3.addWidget(self.clearFormExamsPB, 6, 4, 1, 1)
        self.eDateEdit_to = QtGui.QDateEdit(self.widget)
        self.eDateEdit_to.setDateTime(QtCore.QDateTime(QtCore.QDate(2014, 12, 31), QtCore.QTime(0, 0, 0)))
        self.eDateEdit_to.setDate(QtCore.QDate(2014, 12, 31))
        self.eDateEdit_to.setTime(QtCore.QTime(0, 0, 0))
        self.eDateEdit_to.setCalendarPopup(True)
        self.eDateEdit_to.setObjectName("eDateEdit_to")
        self.gridLayout_3.addWidget(self.eDateEdit_to, 1, 3, 1, 1)
        self.eLastNameL_2 = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.eLastNameL_2.setFont(font)
        self.eLastNameL_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.eLastNameL_2.setObjectName("eLastNameL_2")
        self.gridLayout_3.addWidget(self.eLastNameL_2, 5, 0, 1, 1)
        self.eProtocolNumLE = QtGui.QLineEdit(self.widget)
        self.eProtocolNumLE.setObjectName("eProtocolNumLE")
        self.gridLayout_3.addWidget(self.eProtocolNumLE, 5, 1, 1, 2)
        self.eDeleteReportPushB = QtGui.QPushButton(self.widget)
        self.eDeleteReportPushB.setEnabled(False)
        self.eDeleteReportPushB.setObjectName("eDeleteReportPushB")
        self.gridLayout_3.addWidget(self.eDeleteReportPushB, 9, 3, 1, 1)
        self.pushButtonBack = QtGui.QPushButton(self.widget)
        self.pushButtonBack.setObjectName("pushButtonBack")
        self.gridLayout_3.addWidget(self.pushButtonBack, 9, 0, 1, 1)
        self.eStatusMessageL = QtGui.QLabel(self.widget)
        self.eStatusMessageL.setText("")
        self.eStatusMessageL.setObjectName("eStatusMessageL")
        self.gridLayout_3.addWidget(self.eStatusMessageL, 7, 1, 1, 1)
        self.checkBoxSearchDate = QtGui.QCheckBox(self.widget)
        self.checkBoxSearchDate.setChecked(True)
        self.checkBoxSearchDate.setObjectName("checkBoxSearchDate")
        self.gridLayout_3.addWidget(self.checkBoxSearchDate, 1, 0, 1, 1)
        self.eLastNameL_3 = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.eLastNameL_3.setFont(font)
        self.eLastNameL_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.eLastNameL_3.setObjectName("eLastNameL_3")
        self.gridLayout_3.addWidget(self.eLastNameL_3, 4, 0, 1, 1)
        self.checkBoxSearchTime = QtGui.QCheckBox(self.widget)
        self.checkBoxSearchTime.setChecked(True)
        self.checkBoxSearchTime.setObjectName("checkBoxSearchTime")
        self.gridLayout_3.addWidget(self.checkBoxSearchTime, 2, 0, 1, 1)
        self.eLastName2LE = QtGui.QLineEdit(self.widget)
        self.eLastName2LE.setObjectName("eLastName2LE")
        self.gridLayout_3.addWidget(self.eLastName2LE, 4, 1, 1, 2)
        self.verticalLayout_2.addWidget(self.widget)
        self.stackedWidget.addWidget(self.page)
        self.verticalLayout.addWidget(self.stackedWidget)
        MainMenuWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainMenuWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 865, 22))
        self.menubar.setObjectName("menubar")
        MainMenuWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainMenuWindow)
        self.statusbar.setObjectName("statusbar")
        MainMenuWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainMenuWindow)
        self.stackedWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.checkBoxSearchDate, QtCore.SIGNAL("clicked(bool)"), self.eDateEdit_from.setEnabled)
        QtCore.QObject.connect(self.checkBoxSearchDate, QtCore.SIGNAL("clicked(bool)"), self.eDateEdit_to.setEnabled)
        QtCore.QObject.connect(self.checkBoxSearchTime, QtCore.SIGNAL("clicked(bool)"), self.eExamTimeTE.setEnabled)
        QtCore.QObject.connect(self.checkBoxSearchTime, QtCore.SIGNAL("clicked(bool)"), self.eExamTimeTE_2.setEnabled)
        QtCore.QMetaObject.connectSlotsByName(MainMenuWindow)

    def retranslateUi(self, MainMenuWindow):
        MainMenuWindow.setWindowTitle(QtGui.QApplication.translate("MainMenuWindow", "Главное Меню", None, QtGui.QApplication.UnicodeUTF8))
        self.SettingsPB.setText(QtGui.QApplication.translate("MainMenuWindow", "Настройки", None, QtGui.QApplication.UnicodeUTF8))
        self.ExitPB.setText(QtGui.QApplication.translate("MainMenuWindow", "Выход", None, QtGui.QApplication.UnicodeUTF8))
        self.DatabasePB.setText(QtGui.QApplication.translate("MainMenuWindow", "Поиск по БД", None, QtGui.QApplication.UnicodeUTF8))
        self.SerdcePB.setText(QtGui.QApplication.translate("MainMenuWindow", "Исследование Сердца", None, QtGui.QApplication.UnicodeUTF8))
        self.textEditShapka.setHtml(QtGui.QApplication.translate("MainMenuWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:14pt; font-weight:600;\">КАБИНЕТ УЛЬТРАЗВУКОВОЙ ДИАГНОСТИКИ</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:14pt; font-weight:600;\">Городская больница</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Врач ультразвуковой диагностики Иванов И. И.</p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.UziL.setText(QtGui.QApplication.translate("MainMenuWindow", "УЗИ аппарат", None, QtGui.QApplication.UnicodeUTF8))
        self.SavePB.setText(QtGui.QApplication.translate("MainMenuWindow", "Сохранить", None, QtGui.QApplication.UnicodeUTF8))
        self.BackPB.setText(QtGui.QApplication.translate("MainMenuWindow", "Назад", None, QtGui.QApplication.UnicodeUTF8))
        self.UziLE.setText(QtGui.QApplication.translate("MainMenuWindow", "ALOKA SSD-3500 ProSound", None, QtGui.QApplication.UnicodeUTF8))
        self.ShapkaL.setText(QtGui.QApplication.translate("MainMenuWindow", "Шапка\n"
"отчета", None, QtGui.QApplication.UnicodeUTF8))
        self.DoctorL2.setText(QtGui.QApplication.translate("MainMenuWindow", "Врач", None, QtGui.QApplication.UnicodeUTF8))
        self.DoctorCB.setItemText(0, QtGui.QApplication.translate("MainMenuWindow", "Иванов Иван Иванович", None, QtGui.QApplication.UnicodeUTF8))
        self.eDiagnosisL_3.setText(QtGui.QApplication.translate("MainMenuWindow", "по", None, QtGui.QApplication.UnicodeUTF8))
        self.eTitle1L.setText(QtGui.QApplication.translate("MainMenuWindow", "Поиск по информации об обследованиях", None, QtGui.QApplication.UnicodeUTF8))
        self.eDateEdit_from.setDisplayFormat(QtGui.QApplication.translate("MainMenuWindow", "dd/MM/yyyy", None, QtGui.QApplication.UnicodeUTF8))
        self.eExamTimeTE.setDisplayFormat(QtGui.QApplication.translate("MainMenuWindow", "h:mm", None, QtGui.QApplication.UnicodeUTF8))
        self.eDiagnosisL_2.setText(QtGui.QApplication.translate("MainMenuWindow", "по", None, QtGui.QApplication.UnicodeUTF8))
        self.eLastNameL.setText(QtGui.QApplication.translate("MainMenuWindow", "ФИО пациента", None, QtGui.QApplication.UnicodeUTF8))
        self.eSearchPushB.setText(QtGui.QApplication.translate("MainMenuWindow", "Найти", None, QtGui.QApplication.UnicodeUTF8))
        self.eTitle2L.setText(QtGui.QApplication.translate("MainMenuWindow", "Результат поиска:", None, QtGui.QApplication.UnicodeUTF8))
        self.eSearchResultTableWidget.horizontalHeaderItem(0).setText(QtGui.QApplication.translate("MainMenuWindow", "Дата ", None, QtGui.QApplication.UnicodeUTF8))
        self.eSearchResultTableWidget.horizontalHeaderItem(1).setText(QtGui.QApplication.translate("MainMenuWindow", "Время", None, QtGui.QApplication.UnicodeUTF8))
        self.eSearchResultTableWidget.horizontalHeaderItem(2).setText(QtGui.QApplication.translate("MainMenuWindow", "прот.№", None, QtGui.QApplication.UnicodeUTF8))
        self.eSearchResultTableWidget.horizontalHeaderItem(3).setText(QtGui.QApplication.translate("MainMenuWindow", "Доктор", None, QtGui.QApplication.UnicodeUTF8))
        self.eSearchResultTableWidget.horizontalHeaderItem(4).setText(QtGui.QApplication.translate("MainMenuWindow", "Пациент", None, QtGui.QApplication.UnicodeUTF8))
        self.eReportViewPushB.setText(QtGui.QApplication.translate("MainMenuWindow", "Открыть отчет", None, QtGui.QApplication.UnicodeUTF8))
        self.eExamTimeTE_2.setDisplayFormat(QtGui.QApplication.translate("MainMenuWindow", "h:mm", None, QtGui.QApplication.UnicodeUTF8))
        self.clearFormExamsPB.setText(QtGui.QApplication.translate("MainMenuWindow", "Очистить форму", None, QtGui.QApplication.UnicodeUTF8))
        self.eDateEdit_to.setDisplayFormat(QtGui.QApplication.translate("MainMenuWindow", "dd/MM/yyyy", None, QtGui.QApplication.UnicodeUTF8))
        self.eLastNameL_2.setText(QtGui.QApplication.translate("MainMenuWindow", "Протокол №", None, QtGui.QApplication.UnicodeUTF8))
        self.eDeleteReportPushB.setText(QtGui.QApplication.translate("MainMenuWindow", "Удалить запись", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonBack.setText(QtGui.QApplication.translate("MainMenuWindow", "В Главное меню", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBoxSearchDate.setText(QtGui.QApplication.translate("MainMenuWindow", "Дата обследования с", None, QtGui.QApplication.UnicodeUTF8))
        self.eLastNameL_3.setText(QtGui.QApplication.translate("MainMenuWindow", "ФИО врача", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBoxSearchTime.setText(QtGui.QApplication.translate("MainMenuWindow", "Время обследования с", None, QtGui.QApplication.UnicodeUTF8))

import icons_rc
