#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, base64

from PySide import QtGui,QtCore

from ObsledovanieDesign   import Ui_Obsledovanie

from serdce_widget import SerdceWidget

from _common_parts.final_report_widget import FinalReportWidget
from _common_parts import common


class ObsledSerdceWindow(QtGui.QMainWindow):
    def __init__(self, store):
        super(ObsledSerdceWindow, self).__init__()
        self.insertSettings = store

        # Используемые переменные
        self.ui = None
        self.endReportFlag = None

        self.serdce_widget = None
        self.final_report = None

        self.initUI()


    def initUI(self):
        self.ui = Ui_Obsledovanie()
        self.ui.setupUi(self)
        self.endReportFlag = False

        # add widgets
        self.serdce_widget = SerdceWidget()
        self.ui.stackedWidgetMain.addWidget(self.serdce_widget)
        self.serdce_widget.updateReportPreview()

        self.final_report = FinalReportWidget(self.insertSettings)
        self.ui.stackedWidgetMain.addWidget(self.final_report)

        # hide add image button
        self.ui.pushButtonAddImage.hide()

        # signals
        self.ui.pushButtonAddImage.clicked.connect(self.onPushButtonAddImage)

        self.ui.pushButtonEnd.clicked.connect(self.onPushButtonEndClicked)
        self.ui.pushButtonExit.clicked.connect(self.close)
    

    def onPushButtonAddImage(self):
        img_filename = QtGui.QFileDialog.getOpenFileName()[0]
        if not os.path.exists(img_filename):
            print('Warning: %s is not found' % img_filename)
            encoded_string = base64.b64encode('error')
        else :
            with open(img_filename, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                
        imageHtml = '<html><p><img src="data:image/jpeg;base64,' + encoded_string + '" name="Image1" align="left" border="0"></p></html>'
        htmlToInsert =  self.final_report.ui.textBrowser.toHtml()
        htmlToInsert = htmlToInsert + imageHtml
        self.final_report.ui.textBrowser.clear()
        self.final_report.ui.textBrowser.insertHtml(htmlToInsert)   



    def onPushButtonEndClicked(self):
        if self.endReportFlag == False:
            #show add image button
            self.ui.pushButtonAddImage.show()
            
            self.endReportFlag = True
            self.ui.stackedWidgetMain.setCurrentWidget(self.final_report)
            self.ui.pushButtonEnd.setText(u"Отправить \nна печать")
            htmlToInsert = self.final_report.ui.textBrowser.toHtml()

            # замена идентификаторов в шаблоне типа shapkaString на значения
            # из настроек и значения, введенные в inter_dialog пользователем
            htmlToInsert = common.process_html_template(htmlToInsert, self.insertSettings)
            self.final_report.ui.textBrowser.clear()
            self.final_report.ui.textBrowser.insertHtml(htmlToInsert)

            widgets = [self.serdce_widget]
            for w in widgets:
                html = w.ui.textBrowserReport.toHtml()
                self.final_report.ui.textBrowser.insertHtml(html)

            # TODO: убрать 2 textedit-а в дизайне FinalReportWidget
            htmlToInsert =  self.final_report.ui.textBrowserDoctorName.toHtml()

            doctor_name = self.insertSettings.get('DoctorName')
            htmlToInsert = htmlToInsert.replace("doctorNameString", doctor_name)
            self.final_report.ui.textBrowser.insertHtml(htmlToInsert)
        else:
            # close the file and open libre office to work with new generated report file
            print("Printing the file!")
            printer = QtGui.QPrinter()
            self.final_report.ui.textBrowser.print_(printer)



def main():
    app = QtGui.QApplication(sys.argv)
    dummy = dict()
    main_window = ObsledSerdceWindow(dummy)
    main_window.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
