#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import sqlalchemy as sa
from sqlalchemy import orm

import models


class SessionManager(object):
    def __init__(self, echo_mode=False):
        super(SessionManager, self).__init__()

        fallback_db_config = {
              'user': 'report_helper_user'
            , 'pass': 'eleventyseven'
            , 'host': 'localhost'
            , 'db_name': 'report_helper_db'
        }

        CONFIG_FILENAME = 'db_config.json'
        if os.path.exists(CONFIG_FILENAME):
            with open(CONFIG_FILENAME, 'rb') as f:
                db_config = json.load(f)
        else:
            print(u"[!] File %s does not exist! Use fall back config insted!" % CONFIG_FILENAME)
            db_config = fallback_db_config

        engine_url = 'postgresql+psycopg2://{0}:{1}@{2}/{3}'.format(
              db_config['user']
            , db_config['pass']
            , db_config['host']
            , db_config['db_name']
        )
        self.engine = sa.create_engine(engine_url, encoding='utf-8', echo=echo_mode)

        # models.DeclBase.metadata.create_all(engine)
        # Set up the session
        self.sm = orm.sessionmaker(bind=self.engine, autoflush=True, autocommit=False,
                              expire_on_commit=True)


    def createNewSession(self):
        return orm.scoped_session(self.sm)

def main():
    pass

if __name__ == '__main__':
    main()

