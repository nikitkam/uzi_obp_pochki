#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys

from PySide import QtCore, QtGui

from ZhelchnyWidgetDesign import Ui_ZhelchnyWidget

class ZhelchnyWidget(QtGui.QWidget):
    def __init__(self):
        super(ZhelchnyWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_ZhelchnyWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()


        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoGelshnyPuzir, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovoGelshnyPuzir, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        QtCore.QObject.connect(self.ui.lineEdit2_48_fval, QtCore.SIGNAL("textChanged(const QString&)"), self.check5)


        # signals to react on users actions
        QtCore.QObject.connect(self.ui.comboBox2_9, QtCore.SIGNAL("activated(int)"), self.switchStackedWidget1)
        QtCore.QObject.connect(self.ui.comboBox2_27, QtCore.SIGNAL("activated(int)"), self.switchStackedWidget2)
        QtCore.QObject.connect(self.ui.comboBox2_13, QtCore.SIGNAL("activated(int)"), self.switchStackedWidget3)


    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)


    def InstallInputValidators(self):
        report_template = self.ui.textBrowserReport.toHtml()

        # float line edits
        float_re = 'lineEdit[0-9]{1,2}_[0-9]{1,2}_fval'
        float_le_names = re.findall(float_re, report_template)
        float_les = [self.ui.__getattribute__(le) for le in float_le_names]
        floatValidator = QtGui.QDoubleValidator(self)
        map(lambda x: x.setValidator(floatValidator), float_les)

        # string line edits
        string_re   = 'lineEdit[0-9]{1,2}_[0-9]{1,2}_sval'
        string_le_names = re.findall(string_re, report_template)
        string_les = [self.ui.__getattribute__(le) for le in string_le_names]
        stringValidator = QtGui.QRegExpValidator(QtCore.QRegExp(r".{0,512}"), self)
        map(lambda x: x.setValidator(stringValidator), string_les)


    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoGelshnyPuzir.click()


    def check5(self):
        curVal = 0
        inputString = self.ui.lineEdit2_48_fval.text()
        CMP_VALUE = 5

        if not inputString:
            return None
        try:
            curVal = float(inputString)
        except ValueError:
            print("ERROR: inputed value is not a float value")
            return None
        if curVal < CMP_VALUE:
            self.ui.comboBox2_50.setCurrentIndex(0)
        else:
            self.ui.comboBox2_50.setCurrentIndex(1)


    def switchStackedWidget1(self):
        if self.ui.comboBox2_9.currentIndex() == 3:
            self.ui.stackedWidget_3.setCurrentIndex(0)
        else:
            self.ui.stackedWidget_3.setCurrentIndex(1)


    def switchStackedWidget2(self):
        if self.ui.comboBox2_27.currentIndex() == 1:
            self.ui.stackedWidget_4.setCurrentIndex(0)
        else:
            self.ui.stackedWidget_4.setCurrentIndex(1)


    def switchStackedWidget3(self):
        sw1 = self.ui.stackedWidget_1
        sw2 = self.ui.stackedWidget_2
        cb_ci  = self.ui.comboBox2_13.currentIndex()
        if cb_ci == 0 or cb_ci == 3:
            sw1.setCurrentIndex(0)
            sw2.setCurrentIndex(0)
        elif cb_ci == 1:
            sw1.setCurrentIndex(1)
            sw2.setCurrentIndex(1)
        elif cb_ci == 2:
            sw1.setCurrentIndex(2)
            sw2.setCurrentIndex(0)


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        # заменяем все lineEdit
        le_re = 'lineEdit[0-9]{1,2}_[0-9]{1,2}_.val'
        line_edits = re.findall(le_re, repTextHtml)
        print("line edits to update: %s" % line_edits)
        for le_name in line_edits:
            le = self.ui.__getattribute__(le_name)
            text = le.text() if le.isEnabled() else ''
            print('%s: %s' % (le_name, text))
            repTextHtml = repTextHtml.replace(le_name, text)
        print

        # заменяем все comboBox
        combo_boxes = re.findall('comboBox[0-9]{1,2}_[0-9]{1,2}', repTextHtml)
        for cb_name in combo_boxes:
            cb = self.ui.__getattribute__(cb_name)
            text = cb.currentText() if cb.isEnabled() else ''
            print('%s: %s' % (cb_name, text))
            repTextHtml = repTextHtml.replace(cb_name, text)
        print

        # заменяем все radioButton
        radio_buttons = re.findall('radioButton[0-9]{1,2}_[0-9]{1,2}', repTextHtml)
        for rb in radio_buttons:
            button_obj = self.ui.__getattribute__(rb)
            if button_obj.isChecked() and button_obj.isEnabled():
                text = button_obj.text()
            else:
                text = u''
            print('%s: %s' % (rb, text))
            repTextHtml = repTextHtml.replace(rb, text)
        print

        # заменяем подстроки "stackedWidget_[0-9]" на текст из радиобаттонов
        stacked_widgets = re.findall('stackedWidget_[0-9]', repTextHtml)
        print("Stacked widgets to update: %s" % stacked_widgets)
        for sw_name in stacked_widgets:
            # получаем объект self.ui.stackedWidget_X
            sw = self.ui.__getattribute__(sw_name)
            # получаем page, на котором расположены радио-баттоны
            cw = sw.currentWidget()
            # если есть дочерние объекты, значит радио-баттоны отображаются на виджете
            # и их нужно обрабатывать
            children = cw.children()
            if children:
                # получаем имена объектов, расположенных на странице stackedWidget.
                obj_names = [ x.objectName() for x in children ]
                # собираем имена радио-баттонов (РБ), получаем объекты
                radio_buttons = [ self.ui.__getattribute__(name)
                                      for name in obj_names
                                          if re.match('radioButton[0-9]_[0-9]{1,3}', name) ]
                # собираем чекнутые РБ (должен быть 1)
                checked = [ rb for rb in radio_buttons if rb.isChecked() ]
                try:
                    sole_checked_rb_text = checked[0].text()
                except IndexError as e:
                    sole_checked_rb_text = ''
                # заменяем в шаблоне
                repTextHtml = repTextHtml.replace(sw_name, sole_checked_rb_text)
            else:
                repTextHtml = repTextHtml.replace(sw_name, '')

        self.ui.textBrowserReport.insertHtml(repTextHtml)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = ZhelchnyWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
