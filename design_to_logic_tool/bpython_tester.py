#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""usage: 1) запустить bpython
          2) ввести "import bpython_tester"
          3) в переменной bpython_tester.d - объект класса,
             с настроенным d.ui.
"""

from __future__ import print_function

import os
import sys

from PySide import QtGui
from ZhelchnyWidgetDesign import Ui_ZhelchnyWidget

class Dummy(QtGui.QWidget):
    def __init__(self):
        super(Dummy, self).__init__()
        self.ui = Ui_ZhelchnyWidget()
        self.ui.setupUi(self)


app = QtGui.QApplication('dummy')
d = Dummy()
print("now in %s.d you can find object with working d.ui in it" % __name__)


