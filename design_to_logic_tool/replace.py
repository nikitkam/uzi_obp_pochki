#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import re

from PySide import QtGui

from ZhelchnyWidgetDesign import Ui_ZhelchnyWidget




class DummyWidget(QtGui.QWidget):
    def __init__(self):
        super(DummyWidget, self).__init__()
        self.initUI()

    def initUI(self):
        self.ui = Ui_ZhelchnyWidget()
        self.ui.setupUi(self)

    def get_textBrowserReport(self):
        return self.ui.textBrowserReport.toHtml()



def main():
    dummy_app = QtGui.QApplication('dummy str')
    dummy = DummyWidget()
    report_template = dummy.get_textBrowserReport()

    # заменяем все lineEdit
    line_edits = re.findall('lineEdit[0-9]{1,2}_[0-9]{1,2}', report_template)
    for le in line_edits:
        text = dummy.ui.__getattribute__(le).text()
        report_template = report_template.replace(le, text)

    # заменяем все comboBox
    combo_boxes = re.findall('comboBox[0-9]{1,2}_[0-9]{1,2}', report_template)
    for cb in combo_boxes:
        text = dummy.ui.__getattribute__(cb).currentText()
        report_template = report_template.replace(cb, text)

    # заменяем все radioButton
    radio_buttons = re.findall('radioButton[0-9]{1,2}_[0-9]{1,2}', report_template)
    for rb in radio_buttons:
        button_obj = dummy.ui.__getattribute__(rb)
        if button_obj.isChecked():
            text = button_obj.text()
        else:
            text = u''
        report_template = report_template.replace(rb, text)


    with open('template.html', 'wb') as f:
        f.write(report_template.encode('utf-8'))


if __name__ == '__main__':
    main()

