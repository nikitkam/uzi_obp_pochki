# NAMEOFMODULE_ - тип отчета

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from PySide import QtCore, QtGui

from NAMEOFMODULE_WidgetDesign import Ui_NAMEOFMODULE_Widget

class NAMEOFMODULE_Widget(QtGui.QWidget):
    def __init__(self):
        super(NAMEOFMODULE_Widget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_NAMEOFMODULE_Widget()
        self.ui.setupUi(self)



"""        self.InstallInputValidators()
"""



        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovo, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovo, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)


        # signals to react on users actions:

        # реакция на ввод
        #QtCore.QObject.connect(self.ui.lineEdit, QtCore.SIGNAL("textChanged(const QString&)"), self.inputReact)

        # реакция на выбор комбобокс
        #QtCore.QObject.connect(self.ui.comboBox, QtCore.SIGNAL("activated(int)"), self.chooseReact)




    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)

"""
    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit2_4, self.ui.lineEdit2_7, self.ui.lineEdit2_25,self.ui.lineEdit2_37, self.ui.lineEdit2_42, self.ui.lineEdit2_48]
        map(lambda x: x.setValidator(floatValidator), text_edits)
"""

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovo.click()



    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        #line_edits
        #repTextHtml = (repTextHtml.replace("lineEdit2_4", self.ui.lineEdit2_4.text(), 1))


        #comboBoxes
        #repTextHtml = (repTextHtml.replace("comboBox2_9", self.ui.comboBox2_9.currentText()))



        #radiobuttons
        #mid_str = self.ui.radioButton2_34.text() if self.ui.radioButton2_34.isChecked() else " " 
        #repTextHtml = repTextHtml.replace("radioButton2_34", mid_str)



        # замена в зависимости от enabled/disabled widget 
"""
        if self.ui.QWidget_21.isEnabled() == True :
            mid_str = self.ui.label2_36.text() + " " + self.ui.lineEdit2_37.text()+ " " + self.ui.label2_38.text()+ " " + self.ui.radioButton2_39.text() + self.ui.radioButton2_40.text() + " " + self.ui.label2_41.text() + " " + self.ui.lineEdit2_42.text() + " " + self.ui.label2_43.text() + " " + self.ui.radioButton2_44.text() + self.ui.radioButton2_45.text()
            repTextHtml = repTextHtml.replace("QWidget_21", mid_str)
        else :
            repTextHtml = repTextHtml.replace("QWidget_21", "")
"""

        #insert finally generated html
        self.ui.textBrowserReport.insertHtml(repTextHtml)




""" Функции реакции на ввод и выбор """

def main():
    app = QtGui.QApplication(sys.argv)
    ex = NAMEOFMODULE_Widget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
