#!/bin/bash

usage() {
    cat <<EOF
Input number of file to generate it's design:

    1  - MainMenuWindowDesign.ui > MainMenuWindowDesign.py
EOF
}


read_input() {
    local bldred='\e[1;31m'
    read -p "Enter file number: " filenum ;

    re='^[0-9]{1,2}$'
    if ! [[ $filenum =~ $re ]]; then
        echo -e "${bldred}[-] error: you have to enter a 2-digit number!" >&2  && exit 1;
    fi
}

generate() {
    local green='\e[01;32m' regular='\e[00m' bldred='\e[1;31m'

    case $filenum in
        1)
            ui_file=MainMenuWindowDesign.ui
            design_file=MainMenuWindowDesign.py
            ;;

        *)
            echo -e "${bldred}[-] wrong file number!"  && exit 1
    esac

    local ui_dne="${bldred}[-] error: file ${ui_file} does not exists!"
    if ! [ -f ${ui_file} ] ; then
        echo -e $ui_dne && exit 1
    fi

    pyside-uic -o $design_file $ui_file
    echo -e "...\n[+] Generate ${green}${design_file}${regular} from ${green}${ui_file}${regular}"

}


usage
read_input
generate
