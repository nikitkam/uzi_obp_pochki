#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys

from PySide import QtCore, QtGui

from SelezenkaWidgetDesign import Ui_SelezenkaWidget

# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common



class SelezenkaWidget(QtGui.QWidget):
    def __init__(self):
        super(SelezenkaWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_SelezenkaWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()


        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoSelezenka, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovoSelezenka, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        QtCore.QObject.connect(self.ui.lineEdit5_41, QtCore.SIGNAL("textChanged(const QString&)"), self.check25)

        # signals to react on users actions
        QtCore.QObject.connect(self.ui.comboBox5_3 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget1)
        QtCore.QObject.connect(self.ui.lineEdit5_9, QtCore.SIGNAL("textChanged(const QString&)"), self.checkNorma)
        QtCore.QObject.connect(self.ui.lineEdit5_12, QtCore.SIGNAL("textChanged(const QString&)"), self.checkNorma)
        QtCore.QObject.connect(self.ui.lineEdit5_15, QtCore.SIGNAL("textChanged(const QString&)"), self.checkNorma)


    def checkNorma(self): 
        curVal1 = 0 
        inputString = self.ui.lineEdit5_9.text()
        if inputString == "":
            return(0)
        try:
            curVal1 = float(inputString)
        except ValueError:
            print("Warning: inputed value is not a float value")

        curVal2 = 0 
        inputString = self.ui.lineEdit5_12.text()
        if inputString == "":
            return(0)
        try:
            curVal2 = float(inputString)
        except ValueError:
            print("Warning: inputed value is not a float value")

        curVal3 = 0 
        inputString = self.ui.lineEdit5_15.text()
        if inputString == "":
            return(0)
        try:
            curVal3 = float(inputString)
        except ValueError:
            print("Warning: inputed value is not a float value")


        if curVal1 < 120 and curVal2 < 50 and curVal3 < 70:
            self.ui.comboBox5_3.setCurrentIndex(0)
        else:
            self.ui.comboBox5_3.setCurrentIndex(2)
        self.countIndex()
        self.switchStackedWidget1()


    def countIndex(self):
        selIndex = float(self.ui.lineEdit5_9.text()) * float(self.ui.lineEdit5_12.text()) / 2
        self.ui.lineEdit5_5.setText(str(selIndex))

    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)


    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [
              self.ui.lineEdit5_5
            , self.ui.lineEdit5_9
            , self.ui.lineEdit5_12
            , self.ui.lineEdit5_15
            , self.ui.lineEdit5_38
            , self.ui.lineEdit5_41
        ]
        map(lambda x: x.setValidator(floatValidator), text_edits)

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)

    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoSelezenka.click()


    def check25(self):
        curVal = 0
        inputString = self.ui.lineEdit5_41.text()
        if inputString == "":
            return 0
        try:
            curVal = float(inputString)
        except ValueError:
            print("Warning: inputed value is not a float value")
        if curVal < 25:
            self.ui.comboBox5_43.setCurrentIndex(0)
        else:
            self.ui.comboBox5_43.setCurrentIndex(1)

    def switchStackedWidget1(self):
        if self.ui.comboBox5_3.currentIndex() == 2:
            self.ui.stackedWidget_7.setCurrentIndex(0)
        else:
            self.ui.stackedWidget_7.setCurrentIndex(1)


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        #line_edits
        repTextHtml = common.replace_lineedits(repTextHtml
                                               , self.ui
                                               , regexp=r"lineEdit5_[0-9]{1,3}")

        #textEdit
        repTextHtml = repTextHtml.replace("textEdit5_47", self.ui.textEdit5_47.toPlainText())

        #comboBoxes
        repTextHtml = common.replace_comboboxes(repTextHtml
                                                , self.ui
                                                , regexp=r"comboBox5_[0-9]{1,3}")

        #radiobuttons
        repTextHtml = common.replace_radiobuttons(repTextHtml
                                                  , self.ui
                                                  , regexp=r"radioButton5_[0-9]{1,3}"
                                                  , debug=True)

        if self.ui.widget_21.isEnabled() == True:
            mid_str = self.ui.radioButton5_21.text() if self.ui.radioButton5_21.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_21", mid_str)
            mid_str = self.ui.radioButton5_22.text() if self.ui.radioButton5_22.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_22", mid_str)
        else:
            repTextHtml = repTextHtml.replace("radioButton5_21", "")
            repTextHtml = repTextHtml.replace("radioButton5_22", "")

        if self.ui.widget_22.isEnabled() == True:
            mid_str = self.ui.radioButton5_26.text() if self.ui.radioButton5_26.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_26", mid_str)
            mid_str = self.ui.radioButton5_27.text() if self.ui.radioButton5_27.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_27", mid_str)
        else:
            repTextHtml = repTextHtml.replace("radioButton5_26", "")
            repTextHtml = repTextHtml.replace("radioButton5_27", "")

        if self.ui.stackedWidget_7.currentIndex() == 0:
            mid_str = self.ui.label5_4.text() + " " + self.ui.lineEdit5_5.text() + '<span style=" font-size:8pt;"> CM</span><span style=" vertical-align:super;">2</span>' + "  " + self.ui.label5_7.text()
            repTextHtml = repTextHtml.replace("stackedWidget_7", mid_str)
        else:
            repTextHtml = repTextHtml.replace("stackedWidget_7", "")

        if self.ui.QWidget24_2.isEnabled() == True:
            mid_str = self.ui.label5_35.text() + " "+ self.ui.comboBox5_36.currentText()+ "   " + self.ui.label5_37.text() + self.ui.lineEdit5_38.text() + self.ui.label5_39.text()
            repTextHtml = repTextHtml.replace("QWidget24_2", mid_str)
        else:
            repTextHtml = repTextHtml.replace("QWidget24_2", "")




        # ==============================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = SelezenkaWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
