#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from PySide import QtCore, QtGui

from PochkiDetiWidgetDesign import Ui_PochkiDetiWidget

class PochkiWidget(QtGui.QWidget):
    def __init__(self):
        super(PochkiWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_PochkiDetiWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoPochki, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovoPochki, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)


        # signals to react on users actions
        QtCore.QObject.connect(self.ui.comboBox_2_22 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget1)
        QtCore.QObject.connect(self.ui.comboBox_2_25 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget2)
        QtCore.QObject.connect(self.ui.comboBox_2_28 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget3)
        QtCore.QObject.connect(self.ui.comboBox_3_22 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget4)
        QtCore.QObject.connect(self.ui.comboBox_3_25 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget5)
        QtCore.QObject.connect(self.ui.comboBox_3_28 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget6)
        QtCore.QObject.connect(self.ui.comboBox_4_1 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget7)
        QtCore.QObject.connect(self.ui.comboBox_2_5 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget8)
        QtCore.QObject.connect(self.ui.comboBox_3_5 , QtCore.SIGNAL("activated(int)"), self.switchStackedWidget9)



    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit_2_3, self.ui.lineEdit_2_8  , self.ui.lineEdit_2_11  , self.ui.lineEdit_2_14  , self.ui.lineEdit_2_19    , self.ui.lineEdit_2_35
                      , self.ui.lineEdit_3_3  , self.ui.lineEdit_3_8  , self.ui.lineEdit_3_11 , self.ui.lineEdit_3_14 , self.ui.lineEdit_3_19   , self.ui.lineEdit_3_35
        ]
        map(lambda x: x.setValidator(floatValidator), text_edits)
      

    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoPochki.click()

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)

    def switchStackedWidget1(self):
        if self.ui.comboBox_2_22.currentIndex() == 1:
            self.ui.stackedWidget_10.setCurrentIndex(0)
        if self.ui.comboBox_2_22.currentIndex() == 0:
            self.ui.stackedWidget_10.setCurrentIndex(1)

    def switchStackedWidget2(self):
        if self.ui.comboBox_2_25.currentIndex() == 1 or self.ui.comboBox_2_25.currentIndex() == 2 :
            self.ui.stackedWidget_11.setCurrentIndex(0)
        if self.ui.comboBox_2_25.currentIndex() == 0:
            self.ui.stackedWidget_11.setCurrentIndex(1)

    def switchStackedWidget3(self):
        if self.ui.comboBox_2_28.currentIndex() == 4 :
            self.ui.stackedWidget_15.setCurrentIndex(0)
        else :
            self.ui.stackedWidget_15.setCurrentIndex(1)

    def switchStackedWidget4(self):
        if self.ui.comboBox_3_22.currentIndex() == 1:
            self.ui.stackedWidget_12.setCurrentIndex(0)
        if self.ui.comboBox_3_22.currentIndex() == 0:
            self.ui.stackedWidget_12.setCurrentIndex(1)

    def switchStackedWidget5(self):
        if self.ui.comboBox_3_25.currentIndex() == 1 or self.ui.comboBox_3_25.currentIndex() == 2 :
            self.ui.stackedWidget_13.setCurrentIndex(0)
        if self.ui.comboBox_3_25.currentIndex() == 0:
            self.ui.stackedWidget_13.setCurrentIndex(1)

    def switchStackedWidget6(self):
        if self.ui.comboBox_3_28.currentIndex() == 4 :
            self.ui.stackedWidget_14.setCurrentIndex(0)
        else :
            self.ui.stackedWidget_14.setCurrentIndex(1)

    def switchStackedWidget7(self):
        if self.ui.comboBox_4_1.currentIndex() == 1:
            self.ui.stackedWidget_16.setCurrentIndex(0)
        if self.ui.comboBox_4_1.currentIndex() == 0:
            self.ui.stackedWidget_16.setCurrentIndex(1)

    def switchStackedWidget8(self):
        if self.ui.comboBox_2_5.currentIndex() == 1:
            self.ui.stackedWidget.setCurrentIndex(0)
        else :
            self.ui.stackedWidget.setCurrentIndex(1)
            self.ui.lineEdit_2_3.clear()

    def switchStackedWidget9(self):
        if self.ui.comboBox_3_5.currentIndex() == 1:
            self.ui.stackedWidget_2.setCurrentIndex(0)
        else :
            self.ui.stackedWidget_2.setCurrentIndex(1)
            self.ui.lineEdit_3_3.clear()


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        #line_edits
        repTextHtml = (repTextHtml.replace("lineEdit_2_8", self.ui.lineEdit_2_8.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_2_11", self.ui.lineEdit_2_11.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_2_14", self.ui.lineEdit_2_14.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_2_19", self.ui.lineEdit_2_19.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_2_32", self.ui.lineEdit_2_32.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_2_35", self.ui.lineEdit_2_35.text()))
        #repTextHtml = (repTextHtml.replace("lineEdit_2_3", self.ui.lineEdit_2_3.text(), 1)) # for not to replace 2_32, 2_25, 2_39 strings
        repTextHtml = (repTextHtml.replace("lineEdit_3_8", self.ui.lineEdit_3_8.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_3_11", self.ui.lineEdit_3_11.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_3_14", self.ui.lineEdit_3_14.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_3_19", self.ui.lineEdit_3_19.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_3_32", self.ui.lineEdit_3_32.text()))
        repTextHtml = (repTextHtml.replace("lineEdit_3_35", self.ui.lineEdit_3_35.text()))
        #repTextHtml = (repTextHtml.replace("lineEdit_3_3", self.ui.lineEdit_3_3.text(), 1)) # for not to replace 3_32, 3_25, 3_39 strings
        repTextHtml = (repTextHtml.replace("lineEdit_4_4", self.ui.lineEdit_4_4.text()))

        if self.ui.lineEdit_2_3.text() == "":
            mid_str = "" 
        else:
            mid_str = "cm"
        repTextHtml = (repTextHtml.replace("lineEdit_2_3", self.ui.lineEdit_2_3.text() + mid_str, 1)) # for not to replace 2_32, 2_25, 2_39 strings
        if self.ui.lineEdit_3_3.text() == "":
            mid_str = "" 
        else:
            mid_str = "cm"
        repTextHtml = (repTextHtml.replace("lineEdit_3_3", self.ui.lineEdit_3_3.text() + mid_str, 1)) # for not to replace 3_32, 3_25, 3_39 strings

        #check boxes
        mid_str = self.ui.checkBox_4_5.text() + "," if self.ui.checkBox_4_5.isChecked() else " "
        repTextHtml = (repTextHtml.replace("checkBox_4_5",mid_str))
        mid_str = self.ui.checkBox_4_6.text() + "." if self.ui.checkBox_4_6.isChecked() else " "
        repTextHtml = (repTextHtml.replace("checkBox_4_6",mid_str))
        #comboBoxes
        repTextHtml = (repTextHtml.replace("comboBox_2_6", self.ui.comboBox_2_6.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_16", self.ui.comboBox_2_16.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_17", self.ui.comboBox_2_17.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_18", self.ui.comboBox_2_18.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_21", self.ui.comboBox_2_21.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_22", self.ui.comboBox_2_22.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_25", self.ui.comboBox_2_25.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_28", self.ui.comboBox_2_28.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_31", self.ui.comboBox_2_31.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_34", self.ui.comboBox_2_34.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_6", self.ui.comboBox_3_6.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_16", self.ui.comboBox_3_16.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_17", self.ui.comboBox_3_17.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_18", self.ui.comboBox_3_18.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_21", self.ui.comboBox_3_21.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_22", self.ui.comboBox_3_22.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_25", self.ui.comboBox_3_25.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_28", self.ui.comboBox_3_28.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_31", self.ui.comboBox_3_31.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_34", self.ui.comboBox_2_34.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_4_1", self.ui.comboBox_4_1.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_2_5", self.ui.comboBox_2_5.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox_3_5", self.ui.comboBox_3_5.currentText()))
        #radiobuttons
        if self.ui.stackedWidget_10.currentIndex() == 0:
            mid_str = self.ui.radioButton_2_23.text() if self.ui.radioButton_2_23.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_2_23", mid_str)
            mid_str = self.ui.radioButton_2_24.text() if self.ui.radioButton_2_24.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_2_24", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton_2_23", "")
            repTextHtml = repTextHtml.replace("radioButton_2_24", "")

        if self.ui.stackedWidget_12.currentIndex() == 0:
            mid_str = self.ui.radioButton_3_23.text() if self.ui.radioButton_3_23.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_3_23", mid_str)
            mid_str = self.ui.radioButton_3_24.text() if self.ui.radioButton_3_24.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_3_24", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton_3_23", "")
            repTextHtml = repTextHtml.replace("radioButton_3_24", "")

        if self.ui.stackedWidget_11.currentIndex() == 0:
            mid_str = self.ui.radioButton_2_26.text() if self.ui.radioButton_2_26.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_2_26", mid_str)
            mid_str = self.ui.radioButton_2_27.text() if self.ui.radioButton_2_27.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_2_27", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton_2_26", "")
            repTextHtml = repTextHtml.replace("radioButton_2_27", "")


        if self.ui.stackedWidget_13.currentIndex() == 0:
            mid_str = self.ui.radioButton_3_26.text() if self.ui.radioButton_3_26.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_3_26", mid_str)
            mid_str = self.ui.radioButton_3_27.text() if self.ui.radioButton_3_27.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_3_27", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton_3_26", "")
            repTextHtml = repTextHtml.replace("radioButton_3_27", "")


        if self.ui.stackedWidget_15.currentIndex() == 0:
            mid_str = self.ui.radioButton_2_29.text() if self.ui.radioButton_2_29.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_2_29", mid_str)
            mid_str = self.ui.radioButton_2_30.text() if self.ui.radioButton_2_30.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_2_30", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton_2_29", "")
            repTextHtml = repTextHtml.replace("radioButton_2_30", "")


        if self.ui.stackedWidget_14.currentIndex() == 0:
            mid_str = self.ui.radioButton_3_29.text() if self.ui.radioButton_3_29.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_3_29", mid_str)
            mid_str = self.ui.radioButton_3_30.text() if self.ui.radioButton_3_30.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_3_30", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton_3_29", "")
            repTextHtml = repTextHtml.replace("radioButton_3_30", "")

        if self.ui.stackedWidget_16.currentIndex() == 0:
            mid_str = self.ui.checkBox_4_2.text() if self.ui.checkBox_4_2.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_4_2", mid_str)
            mid_str = self.ui.checkBox_4_3.text() if self.ui.checkBox_4_3.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton_4_3", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton_4_2", "")
            repTextHtml = repTextHtml.replace("radioButton_4_3", "")

        #radio buttons with lineedits
        if self.ui.radioButton_2_37.isChecked() :
            repTextHtml = repTextHtml.replace("radioButton_2_37", self.ui.radioButton_2_37.text())
            repTextHtml = repTextHtml.replace("radioButton_2_38", "")
            repTextHtml = repTextHtml.replace("lineEdit_2_39", "")
        else :
            repTextHtml = repTextHtml.replace("radioButton_2_37", "")
            repTextHtml = repTextHtml.replace("radioButton_2_38", self.ui.radioButton_2_38.text())
            repTextHtml = repTextHtml.replace("lineEdit_2_39", self.ui.lineEdit_2_39.text())


        if self.ui.radioButton_3_37.isChecked() :
            repTextHtml = repTextHtml.replace("radioButton_3_37", self.ui.radioButton_3_37.text())
            repTextHtml = repTextHtml.replace("radioButton_3_38", "")
            repTextHtml = repTextHtml.replace("lineEdit_3_39", "")
        else :
            repTextHtml = repTextHtml.replace("radioButton_3_37", "")
            repTextHtml = repTextHtml.replace("radioButton_3_38", self.ui.radioButton_2_38.text())
            repTextHtml = repTextHtml.replace("lineEdit_3_39", self.ui.lineEdit_3_39.text())



        self.ui.textBrowserReport.insertHtml(repTextHtml)



def main():
    app = QtGui.QApplication(sys.argv)
    ex = PochkiWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
