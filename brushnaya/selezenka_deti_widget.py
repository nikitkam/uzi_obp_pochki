#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from PySide import QtCore, QtGui

from SelezenkaDetiWidgetDesign import Ui_SelezenkaDetiWidget

class SelezenkaWidget(QtGui.QWidget):
    def __init__(self):
        super(SelezenkaWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_SelezenkaDetiWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()


        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoSelezenka, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovoSelezenka, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)


    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)


    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit5_9, self.ui.lineEdit5_12, self.ui.lineEdit5_15, self.ui.lineEdit5_38]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoSelezenka.click()


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        #line_edits
        repTextHtml = (repTextHtml.replace("lineEdit5_9", self.ui.lineEdit5_9.text()))
        repTextHtml = (repTextHtml.replace("lineEdit5_12", self.ui.lineEdit5_12.text()))
        repTextHtml = (repTextHtml.replace("lineEdit5_15", self.ui.lineEdit5_15.text()))
        repTextHtml = (repTextHtml.replace("lineEdit5_31", self.ui.lineEdit5_31.text()))
        repTextHtml = (repTextHtml.replace("lineEdit5_38", self.ui.lineEdit5_38.text()))
        #comboBoxes
        repTextHtml = (repTextHtml.replace("comboBox5_3", self.ui.comboBox5_3.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox5_36", self.ui.comboBox5_36.currentText()))
        #radiobuttons
        mid_str = self.ui.radioButton5_18.text() if self.ui.radioButton5_18.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_18", mid_str)
        mid_str = self.ui.radioButton5_19.text() if self.ui.radioButton5_19.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_19", mid_str)
        mid_str = self.ui.radioButton5_20.text() if self.ui.radioButton5_20.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_20", mid_str)
        mid_str = self.ui.radioButton5_24.text() if self.ui.radioButton5_24.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_24", mid_str)
        mid_str = self.ui.radioButton5_25.text() if self.ui.radioButton5_25.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_25", mid_str)
        mid_str = self.ui.radioButton5_29.text() if self.ui.radioButton5_29.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_29", mid_str)
        #mid_str = self.ui.radioButton5_30.text() if self.ui.radioButton5_30.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_30", "")
        mid_str = self.ui.radioButton5_33.text() if self.ui.radioButton5_33.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_33", mid_str)
        #mid_str = self.ui.radioButton5_34.text() if self.ui.radioButton5_34.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton5_34", "")


        if self.ui.widget_21.isEnabled() == True:
            mid_str = self.ui.radioButton5_21.text() if self.ui.radioButton5_21.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_21", mid_str)
            mid_str = self.ui.radioButton5_22.text() if self.ui.radioButton5_22.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_22", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton5_21", "")
            repTextHtml = repTextHtml.replace("radioButton5_22", "")

        if self.ui.widget_22.isEnabled() == True:
            mid_str = self.ui.radioButton5_26.text() if self.ui.radioButton5_26.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_26", mid_str)
            mid_str = self.ui.radioButton5_27.text() if self.ui.radioButton5_27.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton5_27", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton5_26", "")
            repTextHtml = repTextHtml.replace("radioButton5_27", "")


        if self.ui.QWidget24_2.isEnabled() == True :
            mid_str = self.ui.label5_35.text() + " "+ self.ui.comboBox5_36.currentText()+ " " + self.ui.label5_37.text() + " " + self.ui.lineEdit5_38.text() + " " + self.ui.label5_39.text()
            repTextHtml = repTextHtml.replace("QWidget24_2", mid_str)
        else :
            repTextHtml = repTextHtml.replace("QWidget24_2", "")
          

        # ==============================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = SelezenkaWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
