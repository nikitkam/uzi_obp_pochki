#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

from PySide import QtCore, QtGui

from ZhelchnyDetiWidgetDesign import Ui_ZhelchnyDetiWidget


# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common


class ZhelchnyWidget(QtGui.QWidget):
    def __init__(self):
        super(ZhelchnyWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_ZhelchnyDetiWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoGelshnyPuzir, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovoGelshnyPuzir, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        self.ui.lineEdit2_25.textChanged.connect(self.check3)

        # signals to react on users actions
        self.ui.comboBox2_9.currentIndexChanged.connect(self.switchStackedWidget1)
        self.ui.comboBox2_27.currentIndexChanged.connect(self.switchStackedWidget2)
        self.ui.comboBox2_13.currentIndexChanged.connect(self.switchStackedWidget3)


    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)


    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit2_4, self.ui.lineEdit2_7, self.ui.lineEditB2_12, self.ui.lineEditE2_12, self.ui.lineEditH2_12, self.ui.lineEdit2_25,self.ui.lineEdit2_37, self.ui.lineEdit2_42, self.ui.lineEdit2_48]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoGelshnyPuzir.click()

    def check3(self):
        common.check_le_and_switch_cb(
            self.ui.lineEdit2_25
            , self.ui.comboBox2_27
            , check_value=3
            , index_less=0
            , index_bigger=1 )

    def switchStackedWidget1(self):
        if self.ui.comboBox2_9.currentIndex() == 3:
            self.ui.stackedWidget_3.setCurrentIndex(0)
        if self.ui.comboBox2_9.currentIndex() == 0 or self.ui.comboBox2_9.currentIndex() == 1 or self.ui.comboBox2_9.currentIndex() == 2:
            self.ui.stackedWidget_3.setCurrentIndex(1)


    def switchStackedWidget2(self):
        if self.ui.comboBox2_27.currentIndex() == 1:
            self.ui.stackedWidget_4.setCurrentIndex(0)
        if self.ui.comboBox2_27.currentIndex() == 0 or self.ui.comboBox2_27.currentIndex() == 2:
            self.ui.stackedWidget_4.setCurrentIndex(1)


    def switchStackedWidget3(self):
        if self.ui.comboBox2_13.currentIndex() == 0 or self.ui.comboBox2_13.currentIndex() ==3:
            self.ui.stackedWidget.setCurrentIndex(0)
            self.ui.stackedWidget_2.setCurrentIndex(0)           
        if self.ui.comboBox2_13.currentIndex() == 1:
            self.ui.stackedWidget.setCurrentIndex(1)
            self.ui.stackedWidget_2.setCurrentIndex(1)
        if self.ui.comboBox2_13.currentIndex() == 2:
            self.ui.stackedWidget.setCurrentIndex(2)
            self.ui.stackedWidget_2.setCurrentIndex(0)



    def switchStackedWidget4(self):
        if self.ui.comboBox2_13.currentIndex() == 2:
            self.ui.stackedWidget.setCurrentIndex(2)
        if self.ui.comboBox2_13.currentIndex() == 0 or self.ui.comboBox2_13.currentIndex() == 1 or self.ui.comboBox2_13.currentIndex() == 3:
            self.ui.stackedWidget.setCurrentIndex(0)


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        #line_edits
        repTextHtml = (repTextHtml.replace("lineEdit2_4", self.ui.lineEdit2_4.text(), 1))
        repTextHtml = (repTextHtml.replace("lineEdit2_7", self.ui.lineEdit2_7.text()))
        repTextHtml = (repTextHtml.replace("lineEditB2_12", self.ui.lineEditB2_12.text()))
        repTextHtml = (repTextHtml.replace("lineEditE2_12", self.ui.lineEditE2_12.text()))
        repTextHtml = (repTextHtml.replace("lineEditH2_12", self.ui.lineEditH2_12.text()))
        repTextHtml = (repTextHtml.replace("lineEdit2_25", self.ui.lineEdit2_25.text()))
        repTextHtml = (repTextHtml.replace("lineEdit2_37", self.ui.lineEdit2_37.text()))
        repTextHtml = (repTextHtml.replace("lineEdit2_42", self.ui.lineEdit2_42.text()))
        repTextHtml = (repTextHtml.replace("lineEdit2_46", self.ui.lineEdit2_46.text()))
        repTextHtml = (repTextHtml.replace("lineEdit2_48", self.ui.lineEdit2_48.text()))
        repTextHtml = (repTextHtml.replace("lineEdit2_54", self.ui.lineEdit2_54.text()))
        #comboBoxes
        repTextHtml = (repTextHtml.replace("comboBox2_9", self.ui.comboBox2_9.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox2_13", self.ui.comboBox2_13.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox2_22", self.ui.comboBox2_22.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox2_23", self.ui.comboBox2_23.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox2_27", self.ui.comboBox2_27.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox2_50", self.ui.comboBox2_50.currentText()))
        #radiobuttons
        mid_str = self.ui.radioButton2_34.text() if self.ui.radioButton2_34.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton2_34", mid_str)
        mid_str = self.ui.radioButton2_35.text() if self.ui.radioButton2_35.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton2_35", mid_str)

        mid_str = self.ui.radioButton2_39.text() if self.ui.radioButton2_39.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton2_39", mid_str)
        mid_str = self.ui.radioButton2_40.text() if self.ui.radioButton2_40.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton2_40", mid_str)

        mid_str = self.ui.radioButton2_44.text() if self.ui.radioButton2_44.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton2_44", mid_str)
        mid_str = self.ui.radioButton2_45.text() if self.ui.radioButton2_45.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton2_45", mid_str)

        if self.ui.stackedWidget.currentIndex() == 1:
            mid_str = ""
            if self.ui.radioButton2_16.isChecked():
                mid_str = self.ui.radioButton2_16.text()
            if self.ui.radioButton2_17.isChecked():
                mid_str = self.ui.radioButton2_17.text()
            if self.ui.radioButton2_18.isChecked():
                mid_str = self.ui.radioButton2_18.text()
            repTextHtml = repTextHtml.replace("stackedWidgetPage1", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("stackedWidgetPage1", "")

        if self.ui.stackedWidget.currentIndex() == 2:
            mid_str = ""
            if self.ui.radioButton2_14.isChecked():
                mid_str = self.ui.radioButton2_14.text()
            if self.ui.radioButton2_15.isChecked():
                mid_str = self.ui.radioButton2_15.text()
            repTextHtml = repTextHtml.replace("stackedWidgetPage2", mid_str)
        else:
            repTextHtml = repTextHtml.replace("stackedWidgetPage2", "")

        if self.ui.stackedWidget_2.currentIndex() == 1:
            mid_str = self.ui.radioButton2_19.text() if self.ui.radioButton2_19.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton2_19", mid_str)
            mid_str = self.ui.radioButton2_20.text() if self.ui.radioButton2_20.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton2_20", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton2_19", "")
            repTextHtml = repTextHtml.replace("radioButton2_20", "")

        if self.ui.stackedWidget_3.currentIndex() == 0:
            mid_str = self.ui.radioButton2_10.text() if self.ui.radioButton2_10.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton2_10", mid_str)
            mid_str = self.ui.radioButton2_11.text() if self.ui.radioButton2_11.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton2_11", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton2_10", "")
            repTextHtml = repTextHtml.replace("radioButton2_11", "")

        if self.ui.stackedWidget_4.currentIndex() == 0:
            mid_str = self.ui.radioButton2_27.text() if self.ui.radioButton2_27.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton2_27", mid_str)
            mid_str = self.ui.radioButton2_28.text() if self.ui.radioButton2_28.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton2_28", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton2_27", "")
            repTextHtml = repTextHtml.replace("radioButton2_28", "")
        #radiobuttons with lineEdits
        if self.ui.radioButton2_52.isChecked() :
            repTextHtml = repTextHtml.replace("radioButton2_52", self.ui.radioButton2_52.text())
            repTextHtml = repTextHtml.replace("lineEdit2_54", "")
        else :
            repTextHtml = repTextHtml.replace("radioButton2_52", "")
            repTextHtml = repTextHtml.replace("lineEdit2_54", self.ui.lineEdit2_54.text())

        if self.ui.QWidget_21.isEnabled() == True :
            mid_str = self.ui.label2_36.text() + " " + self.ui.lineEdit2_37.text()+ " " + self.ui.label2_38.text()+ " " + self.ui.radioButton2_39.text() + self.ui.radioButton2_40.text() + " " + self.ui.label2_41.text() + " " + self.ui.lineEdit2_42.text() + " " + self.ui.label2_43.text() + " " + self.ui.radioButton2_44.text() + self.ui.radioButton2_45.text()
            repTextHtml = repTextHtml.replace("QWidget_21", mid_str)
        else :
            repTextHtml = repTextHtml.replace("QWidget_21", "")



        # ==============================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = ZhelchnyWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
