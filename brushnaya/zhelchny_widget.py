#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from PySide import QtCore, QtGui

from ZhelchnyWidgetDesign import Ui_ZhelchnyWidget


# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common



class ZhelchnyWidget(QtGui.QWidget):
    def __init__(self):
        super(ZhelchnyWidget, self).__init__()
        self.initUI()


    def initUI(self):
        self.ui = Ui_ZhelchnyWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()
        self.reportTemplateUdalen = self.ui.textBrowserUdalen.toHtml()
        self.reportTemplateSW1 = self.ui.textBrowserSW1.toHtml()



        self.ui.textBrowserUdalen.hide()
        self.ui.textBrowserSW1.hide()
        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoGelshnyPuzir, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovoGelshnyPuzir, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        self.ui.LE_25.textChanged.connect(self.check3)
        self.ui.LE_48.textChanged.connect(self.check5)

        # signals to react on users actions
        self.ui.ComboB_09.currentIndexChanged.connect(self.switchStackedWidget1)
        self.ui.ComboB_27.currentIndexChanged.connect(self.switchStackedWidget2)
        self.ui.ComboB_13.currentIndexChanged.connect(self.switchStackedWidget3)


    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)


    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.LE_04, self.ui.LE_07, \
                      self.ui.LE_25, self.ui.LE_37, \
                      self.ui.LE_48]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoGelshnyPuzir.click()


    def check3(self):
        common.check_le_and_switch_cb(
            self.ui.LE_25
            , self.ui.ComboB_27
            , check_value=3
            , index_less=0
            , index_bigger=1)

    def check5(self):
        common.check_le_and_switch_cb(
            self.ui.LE_48
            , self.ui.ComboB_50
            , check_value=5
            , index_less=0
            , index_bigger=1)


    def switchStackedWidget1(self):
        if self.ui.ComboB_09.currentIndex() == 5:
            self.ui.widget_8.setDisabled(True)
        if self.ui.ComboB_09.currentIndex() == 0 or \
           self.ui.ComboB_09.currentIndex() == 1 or \
           self.ui.ComboB_09.currentIndex() == 2 or \
           self.ui.ComboB_09.currentIndex() == 3 or \
           self.ui.ComboB_09.currentIndex() == 4:
            self.ui.widget_8.setEnabled(True)


    def switchStackedWidget2(self):
        if self.ui.ComboB_27.currentIndex() == 1:
            self.ui.stackedWidget_4.setCurrentIndex(0)
        if self.ui.ComboB_27.currentIndex() == 0 or \
           self.ui.ComboB_27.currentIndex() == 2:
            self.ui.stackedWidget_4.setCurrentIndex(1)


    def switchStackedWidget3(self):
        if self.ui.ComboB_13.currentIndex() == 0:
            self.ui.stackedWidget.setCurrentIndex(0)
            self.ui.stackedWidget_2.setCurrentIndex(0)
        if self.ui.ComboB_13.currentIndex() == 1:
            self.ui.stackedWidget.setCurrentIndex(1)
            self.ui.stackedWidget_2.setCurrentIndex(2)
        if self.ui.ComboB_13.currentIndex() == 2:
            self.ui.stackedWidget.setCurrentIndex(1)
            self.ui.stackedWidget_2.setCurrentIndex(1)
        if self.ui.ComboB_13.currentIndex() == 3:
            self.ui.stackedWidget.setCurrentIndex(2)
            self.ui.stackedWidget_2.setCurrentIndex(0)
        if self.ui.ComboB_13.currentIndex() == 4:
            self.ui.stackedWidget.setCurrentIndex(1)
            self.ui.stackedWidget_2.setCurrentIndex(0)
        if self.ui.ComboB_13.currentIndex() == 5:
            self.ui.stackedWidget.setCurrentIndex(1)
            self.ui.stackedWidget_2.setCurrentIndex(0)



    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate
        repTextHtmlUdalen = self.reportTemplateUdalen
        repTextHtmlSW1 = self.reportTemplateSW1

        #line_edits
        repTextHtmlUdalen = common.replace_lineedits(repTextHtmlUdalen
                                                     , ui=self.ui
                                                     , regexp="LE_[0-9]{2}"
                                                     , debug=True)
        repTextHtml = common.replace_lineedits(repTextHtml
                                               , ui=self.ui
                                               , regexp="LE_[0-9]{2}"
                                               , debug=True)

        #comboBoxes
        repTextHtmlUdalen = common.replace_comboboxes(repTextHtmlUdalen
                                                      , ui=self.ui
                                                      , regexp="ComboB_[0-9]{2}"
                                                      , debug=True)
        repTextHtml = common.replace_comboboxes(repTextHtml
                                                , ui=self.ui
                                                , regexp="ComboB_[0-9]{2}"
                                                , debug=True)

        #radiobuttons
        repTextHtmlUdalen = common.replace_radiobuttons(repTextHtmlUdalen
                                                        , ui=self.ui
                                                        , regexp="RB_[0-9]{2}"
                                                        , debug=True)

        repTextHtml = common.replace_radiobuttons(repTextHtml
                                                  , ui=self.ui
                                                  , regexp="RB_[0-9]{2}"
                                                  , debug=True)

        # checkboxes
        repTextHtmlSW1 = common.replace_checkboxes(repTextHtmlSW1
                                                   , ui=self.ui
                                                   , regexp="CheckB_[0-9]{2}[a-z]?"
                                                   , debug=True)



        if self.ui.ComboB_13.currentIndex() == 2:
            mid_str = repTextHtmlSW1
            repTextHtmlUdalen = repTextHtmlUdalen.replace("stackedWidgetPage1", mid_str)
        else:
            if self.ui.ComboB_13.currentIndex() == 1 or \
               self.ui.ComboB_13.currentIndex() == 4 or \
               self.ui.ComboB_13.currentIndex() == 5:
                mid_str1 = self.ui.CheckB_16.text() if self.ui.CheckB_16.isChecked() else " "
                mid_str2 = self.ui.CheckB_17.text() if self.ui.CheckB_17.isChecked() else " "
                mid_str3 = self.ui.CheckB_18.text() if self.ui.CheckB_18.isChecked() else " "
                repTextHtmlUdalen = repTextHtmlUdalen.replace("stackedWidgetPage1", mid_str1 + mid_str2 + mid_str3)
            else:
                repTextHtmlUdalen = repTextHtmlUdalen.replace("stackedWidgetPage1", "")

        if self.ui.stackedWidget.currentIndex() == 2:
            mid_str = ""
            if self.ui.RB_14.isChecked():
                mid_str = self.ui.RB_14.text()
            if self.ui.RB_15.isChecked():
                mid_str = self.ui.RB_15.text()
            repTextHtmlUdalen = repTextHtmlUdalen.replace("stackedWidgetPage2", mid_str)
        #else:
        #    repTextHtmlUdalen = repTextHtmlUdalen.replace("stackedWidgetPage2", "")

        if self.ui.stackedWidget_2.currentIndex() == 2:
            mid_str = ""
            if self.ui.radioButton_sw2_1.isChecked():
                mid_str = self.ui.radioButton_sw2_1.text()
            if self.ui.radioButton_sw2_2.isChecked():
                mid_str = self.ui.radioButton_sw2_2.text()
            print(mid_str)
            repTextHtmlUdalen = repTextHtmlUdalen.replace("stackedWidgetPage2", mid_str)
        else:
            repTextHtmlUdalen = repTextHtmlUdalen.replace("stackedWidgetPage2", "")

        if self.ui.stackedWidget_4.currentIndex() == 0:
            mid_str = self.ui.RB_27.text() if self.ui.RB_27.isChecked() else " "
            repTextHtmlUdalen = repTextHtmlUdalen.replace("RB_27", mid_str)
            mid_str = self.ui.RB_28.text() if self.ui.RB_28.isChecked() else " "
            repTextHtmlUdalen = repTextHtmlUdalen.replace("RB_28", mid_str)
        else:
            repTextHtmlUdalen = repTextHtmlUdalen.replace("RB_27", "")
            repTextHtmlUdalen = repTextHtmlUdalen.replace("RB_28", "")

        if self.ui.QWidget_21.isEnabled() == True:

            repTextHtmlUdalen = repTextHtmlUdalen.replace("rb2_34", self.ui.comboBox_37.currentText())

            mid_str39_40 = self.ui.RB_39.text() if self.ui.RB_39.isChecked() else self.ui.RB_40.text() 

            mid_str44_45 = self.ui.RB_44.text() if self.ui.RB_44.isChecked() else self.ui.RB_45.text()

            mid_str = self.ui.L_36.text() + " "  + self.ui.LE_37.text() + '</span></p>\n<p style=" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-family:'+ 'Times New Roman, serif' + '; font-size:12pt;">   ' + self.ui.L_41.text() + " " + self.ui.LE_42.text() + " " + self.ui.L_56.text() + " " + '</span></p>\n<p style=" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-family:'+ 'Times New Roman, serif' + '; font-size:12pt;">   ' + self.ui.L_38.text() + " " + mid_str39_40 + '</span></p>\n<p style=" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-family:'+ 'Times New Roman, serif' + '; font-size:12pt;">   ' +self.ui.L_43.text() + " " + mid_str44_45 + '</span></p>'
            repTextHtmlUdalen = repTextHtmlUdalen.replace("QWidget_21", mid_str)
        else:
            repTextHtmlUdalen = repTextHtmlUdalen.replace("QWidget_21", "")


        if self.ui.ComboB_09.currentIndex() == 5:
            mid_str = u"Удален"
        else:
            mid_str = repTextHtmlUdalen

        repTextHtml = repTextHtml.replace("textBrowserUdalen", mid_str)

        # ==============================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = ZhelchnyWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
