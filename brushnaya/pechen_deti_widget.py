#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, array

from PySide import QtCore, QtGui

from PechenDetiWidgetDesign import Ui_PechenDetiWidget

class PechenWidget(QtGui.QWidget):
    def __init__(self):
        super(PechenWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_PechenDetiWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoPechen, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.PechenWidgetTab, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        QtCore.QObject.connect(self.ui.pushButtonGotovoPechen, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.PechenWidgetTab.currentIndex() == 1:
            self.ui.pushButtonGotovoPechen.click()

    def backToClicker(self):
        self.ui.PechenWidgetTab.setCurrentIndex(0)

    def switchTabToText(self):
        self.ui.PechenWidgetTab.setCurrentIndex(1)


    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit5, self.ui.lineEdit8, self.ui.lineEdit12, self.ui.lineEdit15, self.ui.lineEdit40, self.ui.lineEdit44, self.ui.lineEdit48]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        #line_edits
        repTextHtml = (repTextHtml.replace("lineEdit5", self.ui.lineEdit5.text()))
        repTextHtml = (repTextHtml.replace("lineEdit8", self.ui.lineEdit8.text()))
        repTextHtml = (repTextHtml.replace("lineEdit12", self.ui.lineEdit12.text()))
        repTextHtml = (repTextHtml.replace("lineEdit15", self.ui.lineEdit15.text()))
        repTextHtml = (repTextHtml.replace("lineEdit40", self.ui.lineEdit40.text()))
        repTextHtml = (repTextHtml.replace("lineEdit44", self.ui.lineEdit44.text()))
        repTextHtml = (repTextHtml.replace("lineEdit48", self.ui.lineEdit48.text()))

        #textEdit
        repTextHtml = (repTextHtml.replace("textEdit37", self.ui.textEdit37.toPlainText()))
        repTextHtml = (repTextHtml.replace("textEdit53", self.ui.textEdit53.toPlainText()))

        #comboBoxes
        repTextHtml = (repTextHtml.replace("comboBox2", self.ui.comboBox2.currentText(),1))
        repTextHtml = (repTextHtml.replace("comboBoxR18", self.ui.comboBoxR18.currentText()))
        repTextHtml = (repTextHtml.replace("comboBoxL18", self.ui.comboBoxL18.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox20", self.ui.comboBox20.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox42", self.ui.comboBox42.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox46", self.ui.comboBox46.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox50", self.ui.comboBox50.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox52", self.ui.comboBox52.currentText()))


        #radiobuttons
        mid_str = self.ui.radioButton24.text() if self.ui.radioButton24.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton24", mid_str)
        mid_str = self.ui.radioButton25.text() if self.ui.radioButton25.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton25", mid_str)
        mid_str = self.ui.radioButton26.text() if self.ui.radioButton26.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton26", mid_str)
        mid_str = self.ui.radioButton30.text() if self.ui.radioButton30.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton30", mid_str)
        mid_str = self.ui.radioButton31.text() if self.ui.radioButton31.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton31", mid_str)
        mid_str = self.ui.radioButton35.text() if self.ui.radioButton35.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton35", mid_str)
        mid_str = self.ui.radioButton36.text() if self.ui.radioButton36.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton36", mid_str)



        if self.ui.verticalWidget.isEnabled() == True:
            mid_str = self.ui.radioButton27.text() if self.ui.radioButton27.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton27", mid_str)
            mid_str = self.ui.radioButton28.text() if self.ui.radioButton28.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton28", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton27", "")
            repTextHtml = repTextHtml.replace("radioButton28", "")

        if self.ui.verticalWidget_2.isEnabled() == True:
            mid_str = self.ui.radioButton32.text() if self.ui.radioButton32.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton32", mid_str)
            mid_str = self.ui.radioButton33.text() if self.ui.radioButton33.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton33", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton32", "")
            repTextHtml = repTextHtml.replace("radioButton33", "")

        # ==============================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)

def main():
    app = QtGui.QApplication(sys.argv)
    ex = PechenWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
