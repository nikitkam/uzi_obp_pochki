#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from PySide import QtCore, QtGui

from PodzheludochnayaDetiWidgetDesign import Ui_PodzheludochnayaDetiWidget

class PodzheludochnayaWidget(QtGui.QWidget):
    def __init__(self):
        super(PodzheludochnayaWidget, self).__init__()
        self.initUI()


    def initUI(self):
        self.ui = Ui_PodzheludochnayaDetiWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoPodzhelZheleza, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.pushButtonGotovoPodzhelZheleza, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)

        # signals to react on users actions
        QtCore.QObject.connect(self.ui.comboBox3_22, QtCore.SIGNAL("activated(int)"), self.switchStackedWidget2)


    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)


    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit3_10, self.ui.lineEdit3_13, self.ui.lineEdit3_16, self.ui.lineEdit3_35]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoPodzhelZheleza.click()


    def switchStackedWidget2(self):
        if self.ui.comboBox3_22.currentIndex() == 2 or self.ui.comboBox3_22.currentIndex() == 3:
            self.ui.stackedWidget_9.setCurrentIndex(0)
        if self.ui.comboBox3_22.currentIndex() == 0:
            self.ui.stackedWidget_9.setCurrentIndex(1)


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate

        # example of changing one value in teport
        #labels
        repTextHtml = (repTextHtml.replace("lineEdit3_10", self.ui.lineEdit3_10.text()))
        repTextHtml = (repTextHtml.replace("lineEdit3_13", self.ui.lineEdit3_13.text()))
        repTextHtml = (repTextHtml.replace("lineEdit3_16", self.ui.lineEdit3_16.text()))
        repTextHtml = (repTextHtml.replace("lineEdit3_35", self.ui.lineEdit3_35.text()))
        #textEdits
        repTextHtml = (repTextHtml.replace("textEdit3_33", self.ui.textEdit3_33.toPlainText()))
        repTextHtml = (repTextHtml.replace("textEdit3_41", self.ui.textEdit3_41.toPlainText()))
        #comboBoxes
        repTextHtml = (repTextHtml.replace("comboBox3_3", self.ui.comboBox3_3.currentText(), 1))
        repTextHtml = (repTextHtml.replace("comboBox3_8", self.ui.comboBox3_8.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox3_19", self.ui.comboBox3_19.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox3_20", self.ui.comboBox3_20.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox3_22", self.ui.comboBox3_22.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox3_37", self.ui.comboBox3_37.currentText()))
        #radiobuttons
        if self.ui.stackedWidget_9.currentIndex() == 0:
            mid_str = self.ui.radioButton3_23.text() if self.ui.radioButton3_23.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton3_23", mid_str)
            mid_str = self.ui.radioButton3_24.text() if self.ui.radioButton3_24.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton3_24", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton3_23", "")
            repTextHtml = repTextHtml.replace("radioButton3_24", "")

        mid_str = self.ui.radioButton3_26.text() if self.ui.radioButton3_26.isChecked() else " "
        repTextHtml = (repTextHtml.replace("radioButton3_26",mid_str))
        mid_str = self.ui.radioButton3_27.text() if self.ui.radioButton3_27.isChecked() else " "
        repTextHtml = (repTextHtml.replace("radioButton3_27",mid_str))

        if self.ui.widget_9.isEnabled() == True:
            mid_str = self.ui.radioButton3_28.text() if self.ui.radioButton3_28.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton3_28", mid_str)
            mid_str = self.ui.radioButton3_29.text() if self.ui.radioButton3_29.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton3_29", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton3_28", "")
            repTextHtml = repTextHtml.replace("radioButton3_29", "")

        #radiobuttons with textEdits
        if self.ui.radioButton3_31.isChecked() :
            repTextHtml = repTextHtml.replace("radioButton3_31", self.ui.radioButton3_31.text())
            repTextHtml = repTextHtml.replace("radioButton3_32", "")
            repTextHtml = repTextHtml.replace("textEdit3_33", "")
        else :
            repTextHtml = repTextHtml.replace("radioButton3_31", "")
            repTextHtml = repTextHtml.replace("radioButton3_32", self.ui.radioButton3_32.text())
            repTextHtml = repTextHtml.replace("textEdit3_33", self.ui.textEdit3_33.toPlainText())

        if self.ui.radioButton3_39.isChecked() :
            repTextHtml = repTextHtml.replace("radioButton3_39", self.ui.radioButton3_39.text())
            repTextHtml = repTextHtml.replace("radioButton3_40", "")
            repTextHtml = repTextHtml.replace("textEdit3_41", "")
        else :
            repTextHtml = repTextHtml.replace("radioButton3_39", "")
            repTextHtml = repTextHtml.replace("radioButton3_40", self.ui.radioButton3_40.text())
            repTextHtml = repTextHtml.replace("textEdit3_41", self.ui.textEdit3_41.toPlainText())

        self.ui.textBrowserReport.insertHtml(repTextHtml)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = PodzheludochnayaWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
