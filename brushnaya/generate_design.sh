#!/bin/bash

usage() {
    cat <<EOF
Input number of file to generate it's design:

    2  - MainMenuWindowDesign.ui > MainMenuWindowDesign.py
    3  - ObsledovanieDesign.ui > ObsledovanieDesign.py
    4  - PechenWidgetDesign.ui > PechenWidgetDesign.py
    5  - PochkiWidgetDesign.ui > PochkiWidgetDesign.py
    6  - PodzheludochnayaWidgetDesign.ui > PodzheludochnayaWidgetDesign.py
    7  - SelezenkaWidgetDesign.ui > SelezenkaWidgetDesign.py
    8  - ZhelchnyWidgetDesign.ui > ZhelchnyWidgetDesign.py
    9  - FinalReportWidgetDesign.ui > FinalReportWidgetDesign.py
    10  - PodzheludochnayaWidgetDesign2.ui > PodzheludochnayaWidgetDesign2.py
    11  - ZhelchnyWidgetDesign2.ui > ZhelchnyWidgetDesign2.py
EOF
}


read_input() {
    local bldred='\e[1;31m'
    read -p "Enter file number: " filenum ;

    re='^[0-9]{1,2}$'
    if ! [[ $filenum =~ $re ]]; then
        echo -e "${bldred}[-] error: you have to enter a 2-digit number!" >&2  && exit 1;
    fi
}

generate() {
    local green='\e[01;32m' regular='\e[00m' bldred='\e[1;31m'

    case $filenum in
        # 1)
        #     ui_file='InterDialogDesign.ui'
        #     design_file='InterDialogDesign.py'
        #     ;;
        2)
            ui_file=MainMenuWindowDesign.ui
            design_file=MainMenuWindowDesign.py
            ;;
        3)
            ui_file=ObsledovanieDesign.ui
            design_file=ObsledovanieDesign.py
            ;;
        4)
            ui_file=PechenWidgetDesign.ui
            design_file=PechenWidgetDesign.py
            ;;
        5)
            ui_file=PochkiWidgetDesign.ui
            design_file=PochkiWidgetDesign.py
            ;;
        6)
            ui_file=PodzheludochnayaWidgetDesign.ui
            design_file=PodzheludochnayaWidgetDesign.py
            ;;
        7)
            ui_file=SelezenkaWidgetDesign.ui
            design_file=SelezenkaWidgetDesign.py
            ;;
        8)
            ui_file=ZhelchnyWidgetDesign.ui
            design_file=ZhelchnyWidgetDesign.py
            ;;
        9)
            ui_file=FinalReportWidgetDesign.ui
            design_file=FinalReportWidgetDesign.py
            ;;
        10)
            ui_file=PodzheludochnayaWidgetDesign2.ui
            design_file=PodzheludochnayaWidgetDesign2.py
            ;;
        11)
            ui_file=ZhelchnyWidgetDesign2.ui
            design_file=ZhelchnyWidgetDesign2.py
            ;;
        *)
            echo -e "${bldred}[-] wrong file number!"  && exit 1
    esac

    local ui_dne="${bldred}[-] error: file ${ui_file} does not exists!"
    if ! [ -f ${ui_file} ] ; then
        echo -e $ui_dne && exit 1
    fi

    pyside-uic -o $design_file $ui_file
    echo -e "...\n[+] Generate ${green}${design_file}${regular} from ${green}${ui_file}${regular}"

}


usage
read_input
generate
