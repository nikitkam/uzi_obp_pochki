#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Модуль реализует dict-подобный класс настроек.
Сначала в качестве настроек использовался просто словарь. Считыванием и
записыванием этого словаря в файл на диске занимался файл main_menu_window.
Но понадобилось сохранять настройки в другом месте (inter_dialog).
Чтобы не передавать колбэк на функцию сохранения, было решено сделать настройки
в виде dict-подобного класса. Это позволяет не менять работающий код и
реализовать необходимые методы (сохранение) прямо в классе.

TODO: для таких задач есть стандартные инструменты в модуле collections.
"""
from __future__ import print_function

import os
import pickle

from PySide import QtGui


class SettingsDict(object):
    def __init__(self, qt_widget):
        super(SettingsDict, self).__init__()
        self.parent_widget = qt_widget
        self._SETTINGS_FILENAME = u'st.conf'
        self.settings = self.load()

################### определяем методы дикта #################
    def __contains__(self, key):
        return key in self.settings

    def __getitem__(self, key):
        if key in self.settings:
            return self.settings[key]
        raise KeyError('%s not in settings.keys()' % key)

    def __setitem__(self, key, value):
        self.settings[key] = value

    def __len__(self):
        return len(self.settings)


    def get(self, key, default_value=None):
        return self.settings[key] if key in self.settings \
            else default_value

    def copy(self):
        return self.settings.copy()


################### интерфейс для работы с настройками ###################
    def load(self):
        if not os.path.exists(self._SETTINGS_FILENAME):
            return dict()
        try:
            return  pickle.load(
                open(self._SETTINGS_FILENAME)
            )
        except IOError as e:
            print("[!]Can't load settings. Msg: %s", e)
            return dict()


    def save(self, silent=False):
        """ Сохранение настрек в файл.
              silent - если True, то сообщение об успешном
                       сохранении не выводится. И наоборот.
        """
        if not self.settings:
            print('settings dict is empty!')
            self.settings = dict()
        msg = u'Настройки успешно сохранены'
        try:
            pickle.dump(self.settings,
                        open(self._SETTINGS_FILENAME, 'wb')
                        )
        except IOError as e:
            print("[!] Can't save settings! (%s)" % e)
            msg = u'Не удалось сохранить настройки!'
        if not silent:
            reply = QtGui.QMessageBox.information(self.parent_widget, u'Сохранено', msg)

    # def add_settings_entry(self, entry):
    #     settings = self.load()
    #     if type(entry) != dict:
    #         print("ERROR: entry should be dict!")
    #         return
    #     settings.update(entry)
    #     self.save()

    def __str__(self):
        return str(self.settings)


def main():
    pass


if __name__ == '__main__':
    main()

