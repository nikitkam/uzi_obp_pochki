#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys
import datetime

from PySide import QtCore, QtGui

from _common_parts.FinalReportWidgetDesign import Ui_FinalReportWidget

import session_manager
from models import Examination


class FinalReportWidget(QtGui.QWidget):
    def __init__(self, settings=None):
        super(FinalReportWidget, self).__init__()
        # настройки из главного меню и промежуточного диалога.
        # сделаем копию, чтобы случайно не испаганить их
        self._settings = settings.copy() if settings else dict()
        self.initUI()

    def initUI(self):
        self.ui = Ui_FinalReportWidget()
        self.ui.setupUi(self)
        self.ui.textBrowserDoctorName.hide()
        self.ui.pushButtonSave.clicked.connect(self.onPushButtonSaveClicked)
        self.ui.pushButtonSaveDB.clicked.connect(self.onPushButtonSaveDBClicked)

    def onPushButtonSaveDBClicked(self):
        #print('saving to DB', self._settings)
        # save report html to binary blob
        htmlString = self.ui.textBrowser.toHtml().encode('utf-8')

        # create database examination object
        ex = Examination(
                ex_date = datetime.date.today(),
                ex_time = datetime.datetime.now().time()      ,
                ex_type = self._settings['protocol_num']      ,
                doctor_name  =  self._settings['DoctorName'],
                patient_name = self._settings['patient_fio'] ,
                sex = self._settings['patient_sex'],
                report_html_blob = htmlString
            )
        # start database session and store object
        sm = session_manager.SessionManager()
        session = sm.createNewSession()
        session.add(ex)
        session.commit()

        reply = QtGui.QMessageBox.information(self, u'Сообщение', u'Запись успешно добавлена в БД!')



    def onPushButtonSaveClicked(self):
        home_dir = os.path.expanduser('~')
        # если не удалось зарезолвить юзера, то сохраняем на папку выше.
        if home_dir == '~':
            home_dir = '../'

        patient_fio = self._settings.get('patient_fio', '_empty_')
        patient_fio = patient_fio.replace(' ', '_')
        td = str(QtCore.QDate.currentDate().toPython())
        suggest_name = patient_fio + u'_' + td
        fileName = QtGui.QFileDialog.getSaveFileName(self
                                                     , u'Сохранить отчет как...'
                                                     , home_dir + os.path.sep + suggest_name + u'.html'
                                                     , u'HTML files(*.html *.htm)')
        file_name = fileName[0]
        if not file_name:
            return
        basename, ext = os.path.splitext(file_name)

        if not ext:
            file_name += '.html'

        if ext and ext != '.html' and ext != '.htm':
            error_msg = u'В данный момент файлы с расширением %s не поддерживаются! '\
                        u'Файл будет сохранен с расширением .html!'
            QtGui.QMessageBox.warning(self, u'Ошибка!', error_msg)
            file_name = basename + '.html'

        if file_name:
            try:
                with open(file_name, 'wb') as f:
                    htmlToInsert = self.ui.textBrowser.toHtml().encode('utf-8')
                    htmlToInsert = htmlToInsert.replace('<meta name="qrichtext" content="1" />','<meta http-equiv="content-type" content="text/html; charset=utf-8">') # this is needed to open report with a browser
                    f.write(htmlToInsert)
                    f.close()
            except IOError as e:
                print('IO erorr: (%s)' % e)
            QtGui.QMessageBox.information(self, u'Сохранено.',
                                          u'Отчет сохранен в файл ' + file_name)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = FinalReportWidget()
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
