# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FinalReportWidgetDesign.ui'
#
# Created: Tue Dec  9 16:15:13 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_FinalReportWidget(object):
    def setupUi(self, FinalReportWidget):
        FinalReportWidget.setObjectName("FinalReportWidget")
        FinalReportWidget.resize(825, 604)
        self.gridLayout = QtGui.QGridLayout(FinalReportWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.textBrowserDoctorName = QtGui.QTextBrowser(FinalReportWidget)
        self.textBrowserDoctorName.setMaximumSize(QtCore.QSize(800, 16777215))
        self.textBrowserDoctorName.setReadOnly(False)
        self.textBrowserDoctorName.setObjectName("textBrowserDoctorName")
        self.gridLayout.addWidget(self.textBrowserDoctorName, 1, 1, 1, 3)
        self.pushButtonSave = QtGui.QPushButton(FinalReportWidget)
        self.pushButtonSave.setObjectName("pushButtonSave")
        self.gridLayout.addWidget(self.pushButtonSave, 2, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 3, 1, 1)
        self.textBrowser = QtGui.QTextBrowser(FinalReportWidget)
        self.textBrowser.setMaximumSize(QtCore.QSize(800, 16777215))
        self.textBrowser.setReadOnly(False)
        self.textBrowser.setObjectName("textBrowser")
        self.gridLayout.addWidget(self.textBrowser, 0, 1, 1, 3)
        self.pushButtonSaveDB = QtGui.QPushButton(FinalReportWidget)
        self.pushButtonSaveDB.setObjectName("pushButtonSaveDB")
        self.gridLayout.addWidget(self.pushButtonSaveDB, 2, 2, 1, 1)

        self.retranslateUi(FinalReportWidget)
        QtCore.QMetaObject.connectSlotsByName(FinalReportWidget)

    def retranslateUi(self, FinalReportWidget):
        FinalReportWidget.setWindowTitle(QtGui.QApplication.translate("FinalReportWidget", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowserDoctorName.setHtml(QtGui.QApplication.translate("FinalReportWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"right\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt; font-weight:600;\">Врач doctorNameString</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonSave.setText(QtGui.QApplication.translate("FinalReportWidget", "Сохранить как...", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("FinalReportWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">shapkaString</p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt; font-weight:600;\">УЛЬТРАЗВУКОВОЕ ИССЛЕДОВАНИЕ </span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt; font-weight:600;\">ОРГАНОВ БРЮШНОЙ ПОЛОСТИ, ПОЧЕК</span></p>\n"
"<p align=\"justify\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt;\">Дата исследования dateNowString  г.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt;\">Ф.И.О. пациента FIOString Год рождения dateOfBirthString Пол sexString</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:14pt; font-weight:600;\">Протокол исследования protocolNumString</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt; font-weight:600;\">Ультразвуковой аппарат: </span><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt;\">ultrasoundTypeString</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt; font-weight:600;\">Условия визуализации:</span><span style=\" font-family:\'Times New Roman, serif\'; font-size:12pt;\"> visualizationString</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Times New Roman, serif\'; font-size:12pt;\"><br /></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonSaveDB.setText(QtGui.QApplication.translate("FinalReportWidget", "Сохранить в базу данных", None, QtGui.QApplication.UnicodeUTF8))

