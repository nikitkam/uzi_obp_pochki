#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import re

from PySide import QtCore


def process_html_template(template, settings):
    """
    Вставляет значения из 'настроек' приложения в итоговый шаблон.
    template - html шаблон содержащий определенные идентификаторы,
               вроде 'shapkaString', которые будем заменять.
    settings - 'настройки', которые приходят из настроек программы
               и диалога ввода информации о пациенте.
    """
    # TODO: добавить нормальное логирование logging,
    # без сраных костылей.
    try:
        filename = __file__
    except AttributeError:
        filename = 'obp_pochki'

    if not template:
        print('ERROR: %s: Empty template!', filename)
    if not settings:
        print('ERROR: %s: Empty settings!', filename)

    # формируем словарь какие строки на что заменять
    replace_dict = {
          "ultrasoundTypeString": 'Uzi'
        , "FIOString"           : 'patient_fio'
        , "dateOfBirthString"   : 'patient_age'
        , "sexString"           : 'patient_sex'
        , "shapkaString"        : 'Shapka'
        , "protocolNumString"   : 'protocol_num'
        , "visualizationString" : 'visualization'
    }

    replace_dict = { key: settings.get(val, u'')
                     for key,val in replace_dict.iteritems() }
    now = QtCore.QDateTime.currentDateTime()
    replace_dict['dateNowString'] = now.toString("dd MMMM yyyy ")

    # заменяем
    for key, replace_val in replace_dict.iteritems():
        template = template.replace(key, replace_val)
    return template


def check_le_and_switch_cb(lineEdit, comboBox, check_value, index_less, index_bigger):
    """ Проверяет значение в lineEdit, и если оно меньше check_value, то
    переключает текущий индекс comboBox в index_less, иначе в index_bigger
    Если передаются не те значения, то явно валится с исключением """
    inputString = lineEdit.text()
    if not inputString:
        return
    val = float(inputString)
    if val > check_value:
        comboBox.setCurrentIndex(index_bigger)
    else:
        comboBox.setCurrentIndex(index_less)


def get_all_qt_children(obj):
    """Рекурсивно получает всех детей qt объекта и возвращает в списке"""

    if not isinstance(obj, QtCore.QObject):
        raise Exception("Not Qt Object!")
    children = obj.children()

    new_children = []
    for child in children:
        next_gen = get_all_qt_children(child)
        if not next_gen:
            continue

        new_children += next_gen
    return children + new_children



#################### функций обработки html шаблона ###########################
# заменяют имена объектов в шаблоне на значения.
# Типа 'comboBox98' -> 'увеличенные узлы'

# TODO: написать 1 функцию для замены любых элементов

def replace_comboboxes(html_template, ui, regexp=None, debug=False):
    """ Заменяем все элементы comboBox в шаблоне на их значения.
        ui - объект дизайна, в котором содержатся комбобоксы, т.е.
            можно обратится к объекту через ui.comboBox1.
        html_template - шаблон, в котором ищутся строки с именами
            объектов, а затем заменяются на значения объектов.
        Функция возвращает шаблон, с замененными значениями.
    """
    if not regexp:
        regexp = r'comboBox[0-9]{1,3}'

    combo_boxes = re.findall(regexp, html_template)
    for cb_name in combo_boxes:
        cb = ui.__getattribute__(cb_name)
        text = cb.currentText() if cb.isEnabled() else ''
        html_template = html_template.replace(cb_name, text)
        if debug:
            print('%s: %s' % (cb_name, text))

    if debug:
        print()
    return html_template


def replace_lineedits(html_template, ui, regexp=None, debug=False):
    """ заменяем все lineEdit """
    if not regexp:
        regexp = r'lineEdit[0-9]{1,3}'

    line_edits = re.findall(regexp, html_template)
    for le_name in line_edits:
        le = ui.__getattribute__(le_name)
        text = le.text() if le.isEnabled() else ''
        html_template = html_template.replace(le_name, text)
        if debug:
            print('%s: %s' % (le_name, text))
    if debug:
        print()

    return html_template

def replace_radiobuttons(html_template, ui, regexp=None, debug=False):
    """ аналогичино replace_comboboxes """
    if not regexp:
        regexp = r'radioButton[0-9]{1,3}'

    radio_buttons = re.findall(regexp, html_template)
    for rb in radio_buttons:
        button_obj = ui.__getattribute__(rb)
        if button_obj.isChecked() and button_obj.isEnabled():
            text = button_obj.text()
        else:
            text = u''
        html_template = html_template.replace(rb, text)
        if debug:
            print('%s: %s' % (rb, text))
    if debug:
        print()

    return html_template


def replace_checkboxes(html_template, ui, regexp=None, debug=False):
    """ заменяем все checkBoxes """
    if not regexp:
        regexp = r'checkBox[0-9]{1,3}'

    check_boxes = re.findall(regexp, html_template)
    for chb in check_boxes:
        checkbox_obj = ui.__getattribute__(chb)
        if checkbox_obj.isChecked() and checkbox_obj.isEnabled():
            text = checkbox_obj.text()
        else:
            text = u''
        html_template = html_template.replace(chb, text)
        if debug:
            print('%s: %s' % (chb, text))
    if debug:
        print()
    return html_template


def replace_textedits(html_template, ui, regexp=None, debug=False):
    """ заменяем все textEdit """
    if not regexp:
        regexp = r'textEdit[0-9]{1,3}'

    textedits = re.findall(regexp, html_template)
    for textedit in textedits:
        textedit_obj = ui.__getattribute__(textedit)
        if textedit_obj.isEnabled():
            text = textedit_obj.toPlainText()
        else:
            text = u''
        html_template = html_template.replace(textedit, text)
        if debug:
            print('%s: %s' % (textedit, text))
    if debug:
        print()
    return html_template


def replace_stackedwidgets(html_template, ui, regexp=None, debug=False):
    # пока этим пользоваться нельзя
    raise Exception()
    # заменяем подстроки "stackedWidget_[0-9]" на текст из радиобаттонов
    stacked_widgets = re.findall('stackedWidget_[0-9]{0,1}', html_template)
    if debug:
        print("Stacked widgets to update: %s" % stacked_widgets)
    for sw_name in stacked_widgets:
        # получаем объект self.ui.stackedWidget_X
        sw = ui.__getattribute__(sw_name)
        # получаем page, на котором расположены радио-баттоны
        cw = sw.currentWidget()
        # если есть дочерние объекты, значит радио-баттоны отображаются
        # на виджете и их нужно обрабатывать
        children = cw.children()
        if children:
            # получаем имена объектов, расположенных на странице stackedWidget.
            obj_names = [ x.objectName() for x in children ]
            # собираем имена радио-баттонов (РБ), получаем объекты
            radio_buttons = [ ui.__getattribute__(name)
                              for name in obj_names
                                  if re.match('RB_[0-9]{1,3}', name) ]
            # собираем чекнутые РБ (должен быть 1)
            checked = [ rb for rb in radio_buttons if rb.isChecked() ]
            try:
                sole_checked_rb_text = checked[0].text()
            except IndexError:
                sole_checked_rb_text = ''
            # заменяем в шаблоне
            html_template = html_template.replace(sw_name, sole_checked_rb_text)
        else:
            html_template = html_template.replace(sw_name, '')
    if debug:
        print()
    return html_template


################# конец функций обработки html шаблона #################

def get_combobox_values(combo_box):
    """
    Функция возвращает словарь вида {номер_строчки : строчка}
    """
    cb = combo_box
    return { i: cb.itemText(i) for i in range(cb.count()) }


def main():
    print('Should be imported only!')

if __name__ == '__main__':
    main()
