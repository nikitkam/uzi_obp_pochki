# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ObsledovanieDesign.ui'
#
# Created: Wed Apr  1 10:43:49 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Obsledovanie(object):
    def setupUi(self, Obsledovanie):
        Obsledovanie.setObjectName("Obsledovanie")
        Obsledovanie.resize(1255, 606)
        self.centralwidget = QtGui.QWidget(Obsledovanie)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.stackedWidgetMain = QtGui.QStackedWidget(self.centralwidget)
        self.stackedWidgetMain.setObjectName("stackedWidgetMain")
        self.verticalLayout.addWidget(self.stackedWidgetMain)
        self.gridLayout.addLayout(self.verticalLayout, 0, 2, 2, 1)
        self.widget_4 = QtGui.QWidget(self.centralwidget)
        self.widget_4.setObjectName("widget_4")
        self.gridLayout_11 = QtGui.QGridLayout(self.widget_4)
        self.gridLayout_11.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_11.setObjectName("gridLayout_11")
        spacerItem = QtGui.QSpacerItem(2, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_11.addItem(spacerItem, 1, 1, 1, 1)
        self.pushButtonEnd = QtGui.QPushButton(self.widget_4)
        self.pushButtonEnd.setObjectName("pushButtonEnd")
        self.gridLayout_11.addWidget(self.pushButtonEnd, 2, 1, 1, 1)
        self.pushButtonExit = QtGui.QPushButton(self.widget_4)
        self.pushButtonExit.setObjectName("pushButtonExit")
        self.gridLayout_11.addWidget(self.pushButtonExit, 3, 1, 1, 1)
        self.pushButtonAddImage = QtGui.QPushButton(self.widget_4)
        self.pushButtonAddImage.setObjectName("pushButtonAddImage")
        self.gridLayout_11.addWidget(self.pushButtonAddImage, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.widget_4, 1, 1, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        Obsledovanie.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(Obsledovanie)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1255, 25))
        self.menubar.setObjectName("menubar")
        Obsledovanie.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(Obsledovanie)
        self.statusbar.setObjectName("statusbar")
        Obsledovanie.setStatusBar(self.statusbar)

        self.retranslateUi(Obsledovanie)
        self.stackedWidgetMain.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(Obsledovanie)

    def retranslateUi(self, Obsledovanie):
        Obsledovanie.setWindowTitle(QtGui.QApplication.translate("Obsledovanie", "Составление отчета обследования", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonEnd.setText(QtGui.QApplication.translate("Obsledovanie", "Завершить \n"
"составление\n"
"отчета", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonExit.setText(QtGui.QApplication.translate("Obsledovanie", "Выход", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonAddImage.setText(QtGui.QApplication.translate("Obsledovanie", "Добавить\n"
" изображение", None, QtGui.QApplication.UnicodeUTF8))

