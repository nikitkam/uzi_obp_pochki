#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import sys, os


from PySide import QtCore, QtGui

from NewNodeWidgetDesign import Ui_NewNodeWidget
from ShitovidkaWidgetDesign import Ui_ShitovidkaWidget

from new_node_widget import NewNodeWidget


# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common



## My Own Colour Class, simple and light weight
class Colour3:
    R = 0
    G = 0
    B = 0
    #CONSTRUCTOR
    def __init__(self): 
        self.R = 0
        self.G = 0
        self.B = 0
    #CONSTRUCTOR - with the values to give it
    def __init__(self, nR, nG, nB):
        self.R = nR
        self.G = nG
        self.B = nB

## My Own Point Class, simple and light weight
class Point:
    #X Coordinate Value
    X = 0
    #Y Coordinate Value
    Y = 0
    #CONSTRUCTOR
    def __init__(self):
        self.X = 0
        self.Y = 0
    #CONSTRUCTOR - with the values to give it
    def __init__(self, nX, nY):
        self.X = nX
        self.Y = nY
    #So we can set both values at the same time
    def Set(self,nX, nY):
        self.X = nX
        self.Y = nY


## Shape class; holds data on the drawing point
class Shape:
    Location = Point(0,0)
    Width = 0.0
    Colour = Colour3(0,0,0)
    ShapeNumber = 0
    #CONSTRUCTOR - with the values to give it
    def __init__(self, L, W, C, S):
        self.Location = L
        self.Width = W
        self.Colour = C
        self.ShapeNumber = S


class Shapes:
    #Stores all the shapes
    __Shapes = []
    def __init__(self):
        self.__Shapes = []
    #Returns the number of shapes being stored.
    def NumberOfShapes(self):
        return len(self.__Shapes)
    #Add a shape to the database, recording its position,
    #width, colour and shape relation information
    def NewShape(self,L,W,C,S):
        Sh = Shape(L,W,C,S)
        self.__Shapes.append(Sh)
    #returns a shape of the requested data.
    def GetShape(self, Index):
        return self.__Shapes[Index]
    #Removes any point data within a certain threshold of a point.
    def RemoveShape(self, L, threshold):
        #do while so we can change the size of the list and it wont come back to bite me in the ass!!
        i = 0
        while True:
            if(i==len(self.__Shapes)):
                break
            #Finds if a point is within a certain distance of the point to remove.
            if((abs(L.X - self.__Shapes[i].Location.X) < threshold) and (abs(L.Y - self.__Shapes[i].Location.Y) < threshold)):
                #removes all data for that number
                del self.__Shapes[i]
                #goes through the rest of the data and adds an extra
                #1 to defined them as a seprate shape and shuffles on the effect.
                for n in range(len(self.__Shapes)-i):
                    self.__Shapes[n+i].ShapeNumber += 1
                #Go back a step so we dont miss a point.
                i -= 1
            i += 1


class Painter(QtGui.QWidget):
    ParentLink = 0
    MouseLoc = Point(0,0)
    LastPos = Point(0,0)
    #print(pixmap)

    def __init__(self,parent,pixmap):
        super(Painter, self).__init__()
        self.ParentLink = parent
        self.MouseLoc = Point(0,0)
        self.LastPos = Point(0,0)
        self.pixmap = pixmap
    #Mouse down event
    def mousePressEvent(self, event):
        if(self.ParentLink.Brush == True):
            self.ParentLink.IsPainting = True
            self.ParentLink.ShapeNum += 1
            self.LastPos = Point(0,0)
        else:
            self.ParentLink.IsEraseing = True

        self.DownPos =  Point(event.x(),event.y())
        self.ParentLink.DrawingShapes.NewShape(self.DownPos,self.ParentLink.CurrentWidth,self.ParentLink.CurrentColour,self.ParentLink.ShapeNum)

    #Mouse Move event
    def mouseMoveEvent(self, event):
        if(self.ParentLink.IsEraseing == True):
            self.MouseLoc = Point(event.x(),event.y())
            self.ParentLink.DrawingShapes.RemoveShape(self.MouseLoc,10)     
            self.repaint()        
                
    #Mouse Up Event         
    def mouseReleaseEvent(self, event):
        if(self.ParentLink.IsPainting == True):
            self.ParentLink.IsPainting = False
            self.UpPos =  Point(event.x(),event.y())
            self.ParentLink.DrawingShapes.NewShape(self.UpPos,self.ParentLink.CurrentWidth,self.ParentLink.CurrentColour,self.ParentLink.ShapeNum)
            self.repaint()
        if(self.ParentLink.IsEraseing == True):
            self.ParentLink.IsEraseing = False  
    
    def paintEvent(self,event):
        painter = QtGui.QPainter(self.pixmap)
        if painter.isActive() == False:
            painter.begin(self)
        self.drawLines(event, painter)
        painter.end()


        
        painter2 = QtGui.QPainter()
        painter2.begin(self)
        backgrnd = os.path.abspath("Documents/uzi_obp_pochki/shitovidka/backgrnd.jpg")
        if not os.path.exists(backgrnd):
            print("[Warn] File %s is not found!" % backgrnd)
            painter2.end()
            return

        pixmap = QtGui.QPixmap(backgrnd)
        painter2.drawPixmap(QtCore.QRectF(0.0, 0.0, 798.0, 350.0), pixmap, QtCore.QRectF(0.0, 0.0, 798.0, 350.0))

        self.drawLines(event, painter2)
        painter2.end()



    def drawLines(self, event, painter):
        painter.setRenderHint(QtGui.QPainter.Antialiasing);
        
        for i in range(self.ParentLink.DrawingShapes.NumberOfShapes()-1):
            
            T = self.ParentLink.DrawingShapes.GetShape(i)
            T1 = self.ParentLink.DrawingShapes.GetShape(i+1)
        
            if(T.ShapeNumber == T1.ShapeNumber):
                pen = QtGui.QPen(QtGui.QColor(T.Colour.R,T.Colour.G,T.Colour.B), T.Width/2, QtCore.Qt.SolidLine)
                painter.setPen(pen)
                #painter.drawLine(T.Location.X,T.Location.Y,T1.Location.X,T1.Location.Y)
                painter.drawEllipse(T.Location.X,T.Location.Y,(T1.Location.X - T.Location.X),(T1.Location.Y - T.Location.Y))




class  ShitovidkaWidget(QtGui.QWidget):

    Brush = True
    DrawingShapes = Shapes()
    IsPainting = False
    IsEraseing = False

    CurrentColour = Colour3(0,0,0)
    CurrentWidth = 10
    ShapeNum = 0
    IsMouseing = False
    PaintPanel = 0



    def __init__(self, _settings):
        super(ShitovidkaWidget, self).__init__()
        self.initUI()

        if _settings['patient_sex'] == u'м':
            self.ui.lineEdit25.setText('25')
        else:
            self.ui.lineEdit25.setText('18')
        self._settings = _settings

        img_path = os.path.abspath("Documents/uzi_obp_pochki/shitovidka/image350x798.jpg")
        self.pixmap = QtGui.QPixmap(img_path)
        self.PaintPanel = Painter(self, self.pixmap)

        self.PaintPanel.close()
        self.ui.DrawingFrame.insertWidget(0,self.PaintPanel)
        self.ui.DrawingFrame.setCurrentWidget(self.PaintPanel)
        self.Establish_Connections()

        # словарь с хранящимися в нем виджетами узловых патологий.
        # вида {порядковый_номер: виджет}
        self._nodes = dict()
        self.hide_nodes_buttons()
        self.toggle_DeleteNewNodePB()

        self.ui.widget_BrushErase_Button.hide()

    def SwitchBrush(self):
        if(self.Brush == True):
            self.Brush = False
        else:
            self.Brush = True
    
    def ChangeColour(self):
        col = QtGui.QColorDialog.getColor()
        if col.isValid():
            self.CurrentColour = Colour3(col.red(),col.green(),col.blue())

    def ChangeThickness(self,num):
        self.CurrentWidth = num

    def ClearSlate(self):
        self.DrawingShapes = Shapes()
        self.PaintPanel.repaint()
        img_path = os.path.abspath("Documents/uzi_obp_pochki/shitovidka/image350x798.jpg")
        self.PaintPanel.pixmap = QtGui.QPixmap(img_path)





    def Establish_Connections(self):
        QtCore.QObject.connect(self.ui.BrushErase_Button, QtCore.SIGNAL("clicked()"),self.SwitchBrush)
        QtCore.QObject.connect(self.ui.ChangeColour_Button, QtCore.SIGNAL("clicked()"),self.ChangeColour)
        QtCore.QObject.connect(self.ui.Clear_Button, QtCore.SIGNAL("clicked()"),self.ClearSlate)
        QtCore.QObject.connect(self.ui.Thickness_Spinner, QtCore.SIGNAL("valueChanged(int)"),self.ChangeThickness)
        self.ui.checkBoxAddNodes.clicked.connect(self.disablePainter)
        self.ui.checkBoxNodesManualy.clicked.connect(self.disablePainter)


    def disablePainter(self):
        if self.ui.checkBoxAddNodes.isChecked() == False:
            self.ui.checkBoxNodesManualy.setChecked(False)
            self.ui.checkBoxNodesManualy.setEnabled(False)
            self.ui.widget_5.hide()
        else :
            self.ui.widget_5.show()
            self.ui.checkBoxNodesManualy.setEnabled(True)

        if  self.ui.checkBoxNodesManualy.isChecked() == True :
            self.ui.widget_5.setEnabled(False)
        else :
            self.ui.widget_5.setEnabled(True)

    def hide_nodes_buttons(self):
        """
        Скрывает кнопки узловых патологий. Кнопки остаются скрытыми, пока 
        в нужный момент не показываются пользователю. Это проще, чем на лету 
        добавлять в дизайн новые кнопки
        """
        ui = self.ui
        nodes_buttons = [
            ui.Node1PB, ui.Node2PB, ui.Node3PB, ui.Node4PB, ui.Node5PB,
            ui.Node6PB, ui.Node7PB, ui.Node8PB, ui.Node9PB, ui.Node10PB
        ]
        for button in nodes_buttons:
            button.hide()


    def create_and_show_node(self, number=0):
        """
        если number=0, то создается виджет и показываем кнопку
        под первым свободным порядковым номером.
        """
        MAX_NODES = 10
        if (number > MAX_NODES or number < 0):
            print("[W]. Bad number!")
            return None
        if len(self._nodes) >= MAX_NODES:
            print("[W] Too much nodes!")
            return None

        # добавляем в первое "свободное" место от 1 до 10
        if number == 0:
            numbers = sorted(self._nodes.keys())
            for x in range(1, MAX_NODES+1):
                if x not in numbers:
                    self._nodes[x] = NewNodeWidget(self._settings, x)
                    self._nodes[x].show()
                    push_button_name = 'Node%dPB' % x
                    push_button = self.ui.__getattribute__(push_button_name)
                    push_button.show()
                    break

        elif number not in self._nodes:
            self._nodes[number] = NewNodeWidget(self._settings, x)
            self._nodes[number].show()
            push_button_name = 'Node%dPB' % number
            push_button = self.ui.__getattribute__(push_button_name)
            push_button.show()
        # окно узловой патологии с порядковым номером number уже есть
        else:
            self._nodes[number].show()


    def show_node(self, number):
        """ Показывает виджет с узловым образованием под номером number"""
        if not number:
            return
        if number in self._nodes:
            self._nodes[number].show()


    # TODO: сделать что-нибудь с кучей одинакового кода
    def onUzel1Clicked(self):
        self.show_node(1)
    def onUzel2Clicked(self):
        self.show_node(2)
    def onUzel3Clicked(self):
        self.show_node(3)
    def onUzel4Clicked(self):
        self.show_node(4)
    def onUzel5Clicked(self):
        self.show_node(5)
    def onUzel6Clicked(self):
        self.show_node(6)
    def onUzel7Clicked(self):
        self.show_node(7)
    def onUzel8Clicked(self):
        self.show_node(8)
    def onUzel9Clicked(self):
        self.show_node(9)
    def onUzel10Clicked(self):
        self.show_node(10)


    def onAddNewNodePBClicked(self):
        """ По нажатию должно выскакивать диалоговое окно заполнения узловой патологии
        и появляться кнопка, по которой можно открыть окно узла.
        """
        self.create_and_show_node(0)
        self.toggle_DeleteNewNodePB()


    def toggle_DeleteNewNodePB(self):
        """ включает\выключает кнопку DeleteNewNodePB в зависимости от того,
        есть ли узловые образования"""
        if len(self._nodes):
            self.ui.DeleteNewNodePB.setEnabled(True)
            return True
        else:
            self.ui.DeleteNewNodePB.setEnabled(False)
            return False

    # TODO: redmine, Reoprt Helper, баг #1
    def onDeleteNewNodePBClicked(self):
        if not self.toggle_DeleteNewNodePB():
            return

        dialog = QtGui.QInputDialog()
        dialog.setInputMode(QtGui.QInputDialog.IntInput)
        dialog.setIntRange(0, len(self._nodes))
        number, ok = dialog.getInt(self, u'Введите номер удаляемого образования',\
                                              u'Удаление узловового образования')
        if not ok:
            return

        if number not in self._nodes:
            msg = u'Узел с номером %d отсутствует!' % number
            QtGui.QMessageBox.warning(self, u'Warning!', msg)
            return

        # закрываем и удаляем виджет узлового образования
        node_widget = self._nodes[number]
        node_widget.close()
        self._nodes.pop(number)

        # скрываем кнопку, с надписью 'УзелX'
        btn_name = "Node%dPB" % number
        button = self.ui.__getattribute__(btn_name)
        button.hide()


    def initUI(self):
        self.ui = Ui_ShitovidkaWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # Поле, из которого берется html шаблон заголовка таблицы.
        # Скрываем его
        self.ui.textBrowserShapkaNodes.hide()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoChitovidnayaZheleza, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        QtCore.QObject.connect(self.ui.pushButtonGotovoChitovidnayaZheleza, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)




        self.ui.lineEdit9.textChanged.connect(self.volume19)
        self.ui.lineEdit12.textChanged.connect(self.volume19)
        self.ui.lineEdit15.textChanged.connect(self.volume19)

        self.ui.lineEdit10.textChanged.connect(self.volume20)
        self.ui.lineEdit13.textChanged.connect(self.volume20)
        self.ui.lineEdit16.textChanged.connect(self.volume20)

        self.ui.lineEdit11.textChanged.connect(self.volume21)
        self.ui.lineEdit14.textChanged.connect(self.volume21)
        self.ui.lineEdit17.textChanged.connect(self.volume21)

        self.ui.lineEdit19.textChanged.connect(self.volume23)
        self.ui.lineEdit20.textChanged.connect(self.volume23)
        self.ui.lineEdit21.textChanged.connect(self.volume23)

        self.ui.lineEdit23.textChanged.connect(self.percentage)


        # signals for user interaction
        self.ui.radioButton40.clicked.connect(self.showSW_2p1)
        self.ui.radioButton41.clicked.connect(self.showSW_2p2)

        # кнопки, вызывающие показ окна с узловыми патологиями
        self.ui.AddNewNodePB.clicked.connect(self.onAddNewNodePBClicked)
        self.ui.Node1PB.clicked.connect(self.onUzel1Clicked)
        self.ui.Node2PB.clicked.connect(self.onUzel2Clicked)
        self.ui.Node3PB.clicked.connect(self.onUzel3Clicked)
        self.ui.Node4PB.clicked.connect(self.onUzel4Clicked)
        self.ui.Node5PB.clicked.connect(self.onUzel5Clicked)
        self.ui.Node6PB.clicked.connect(self.onUzel6Clicked)
        self.ui.Node7PB.clicked.connect(self.onUzel7Clicked)
        self.ui.Node8PB.clicked.connect(self.onUzel8Clicked)
        self.ui.Node9PB.clicked.connect(self.onUzel9Clicked)
        self.ui.Node10PB.clicked.connect(self.onUzel10Clicked)

        self.ui.DeleteNewNodePB.clicked.connect(self.onDeleteNewNodePBClicked)


    def percentage(self):
        prevysh = ( 100 * float(self.ui.lineEdit23.text()) / float(self.ui.lineEdit25.text()) ) - 100
        if prevysh > 0:
            self.ui.lineEditPercentage.setText(str(prevysh))
        else:
            self.ui.lineEditPercentage.setText("-")

    def volume19(self):
        perzad = self.ui.lineEdit9.text()
        shir = self.ui.lineEdit12.text()
        dlin = self.ui.lineEdit15.text()
        if perzad != "" and shir  != "" and dlin  != "" :
            perzadF = float(perzad)
            shirF = float(shir)
            dlinF = float(dlin)
            volume =  "%.1f" % (perzadF * shirF * dlinF * (0.479 / 1000))
            self.ui.lineEdit19.setText(str(volume))


    def volume20(self):
        perzad = self.ui.lineEdit10.text()
        shir = self.ui.lineEdit13.text()
        dlin = self.ui.lineEdit16.text()
        if perzad != "" and shir  != "" and dlin  != "" :
            perzadF = float(perzad)
            shirF = float(shir)
            dlinF = float(dlin)
            volume =  "%.1f" % (perzadF * shirF * dlinF * (0.479 / 1000))
            self.ui.lineEdit20.setText(str(volume))


    def volume21(self):
        perzad = self.ui.lineEdit11.text()
        shir = self.ui.lineEdit14.text()
        dlin = self.ui.lineEdit17.text()
        if perzad != "" and shir  != "" and dlin  != "" :
            perzadF = float(perzad)
            shirF = float(shir)
            dlinF = float(dlin)
            volume =  "%.1f" % (perzadF * shirF * dlinF * (0.479 / 1000))
            self.ui.lineEdit21.setText(str(volume))



    def volume23(self):
        pr = self.ui.lineEdit19.text()
        lev = self.ui.lineEdit20.text()
        persh = self.ui.lineEdit21.text()
        if pr != "" and lev != "" and persh  != "" :
            prF = float(pr)
            levF = float(lev)
            pershF = float(persh)
            volume =  "%.1f" % (prF + levF +pershF)
            self.ui.lineEdit23.setText(str(volume))



    def showSW_2p1(self):
        self.ui.stackedWidget_2.setCurrentIndex(0)

    def showSW_2p2(self):
        self.ui.stackedWidget_2.setCurrentIndex(1)

    # 9 - 21 25
    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit9, self.ui.lineEdit10, self.ui.lineEdit11, self.ui.lineEdit12, self.ui.lineEdit13 , self.ui.lineEdit14,  self.ui.lineEdit15, self.ui.lineEdit16, self.ui.lineEdit17, self.ui.lineEdit19, self.ui.lineEdit20, self.ui.lineEdit21, self.ui.lineEdit25 ]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoChitovidnayaZheleza.click()

    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


########################## Генерация отчета ##################################

    def generate_uzlov_patolog_html(self, html_template, table_view=True):
        """ table_view=True - генерируем в табличном виде,
            table_view=False - в строчном.
        """
        # отсортируем виджеты
        if not self._nodes:
            return html_template.replace('nodesTable', '')
        nodes = [ (k,v) for k,v in self._nodes.iteritems() ]
        sorted_nodes = sorted(nodes, key=lambda x: x[0])
        nodes_html = u''

        # генерим либо строки таблицы, либо наборы строк
        # для узловых патологий
        for i, node_widget in sorted_nodes:
            if table_view:
                node_report = node_widget.get_report_table_row()
            else:
                node_report = node_widget.get_report_body()
            # вставляем номер узла в каждый отчет
            node_report = node_report.replace('NewNodeNumber', str(i))
            nodes_html += node_report + '\n'

        # для табличного вида берем заголовок таблицы (первую строку)
        # и приклеиваем к нему остальные строки
        if table_view:
            result_html = u''
            table_html = self.ui.textBrowserShapkaNodes.toHtml()
            insert_pos = table_html.find('</table>')
            result_html += table_html[ :insert_pos]
            result_html += nodes_html
            result_html += table_html[insert_pos: ]
            return html_template.replace('nodesTable', result_html)

        # для строчного вида просто вставляем строки
        return html_template.replace('nodesTable', nodes_html)


    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()

        repTextHtml = self.reportTemplate
        #=================================================

        #stackedWidget_2
        mid_str = ""
        if self.ui.stackedWidget_2.currentIndex() == 0 :
            if self.ui.checkBox42.isChecked() == True :
                mid_str = self.ui.checkBox42.text() + " " + self.ui.comboBox46.currentText()
        else :
            if self.ui.checkBox43.isChecked() == True:
                midRB1 = self.ui.radioButton44.text() if self.ui.radioButton44.isChecked() else "" 
                midRB2 = self.ui.radioButton45.text() if self.ui.radioButton45.isChecked() else "" 
                midRB3 = self.ui.radioButtonBoth1.text() if self.ui.radioButtonBoth1.isChecked() else "" 
                mid_str = self.ui.checkBox43.text() + " "  + " " + self.ui.comboBox47a.currentText() + " " + self.ui.comboBox47b.currentText() + " " + midRB1 + midRB2 + midRB3 
        repTextHtml = (repTextHtml.replace("sw_2", mid_str))

        repTextHtml = (repTextHtml.replace("textEditUzlPat", self.ui.textEditUzlPat.toPlainText()))
        #mid7172
        #mid_str = self.ui.checkBox71.text() + " " + self.ui.comboBox72.currentText() if self.ui.checkBox71.isChecked() else "" 
        #repTextHtml = (repTextHtml.replace("mid7172", mid_str))

        #mid8486
        mid_str = self.ui.radioButton84.text() + " " + self.ui.comboBox85.currentText() + self.ui.textEdit86.toHtml() if self.ui.radioButton84.isChecked() else "" 
        repTextHtml = (repTextHtml.replace("mid8486", mid_str))

        mid_str = self.ui.comboBox72.currentText() if self.ui.radioButton68.isChecked() else ""
        repTextHtml = (repTextHtml.replace("comboBox72", mid_str))


        repTextHtml = (repTextHtml.replace("comboBox27", self.ui.comboBox27.currentText()))

        #combo31
        mid_str = self.ui.comboBox31.currentText() if self.ui.radioButton30.isChecked() else "" 
        repTextHtml = (repTextHtml.replace("comboBox31", mid_str))


        # заменяем имена ui элементов в шаблоне на их значения
        repTextHtml = common.replace_radiobuttons(  repTextHtml
                                                  , self.ui
                                                  , regexp=r'radioButton[\w]*[0-9]{1,3}[\w]{0,2}'
                                                  , debug=False )

        repTextHtml = common.replace_checkboxes(  repTextHtml
                                                  , self.ui
                                                  , regexp=r'checkBox[0-9]{1,3}'
                                                  , debug=False )

        repTextHtml = common.replace_lineedits( repTextHtml
                                                , self.ui
                                                , regexp=r'lineEdit[0-9]{1,3}'
                                                , debug=False )

        # ------------------- обработка узловых патологий ----------------------

        if self.ui.radioButtonNodesTableMode.isChecked():
            repTextHtml = self.generate_uzlov_patolog_html(repTextHtml)
        elif self.ui.radioButtonNodesTextMode.isChecked():
            # для генерации строчного вида
            repTextHtml = self.generate_uzlov_patolog_html(repTextHtml, table_view=False)

        # ------------------- сохр. и вставка картинки в отчет -----------------

        img_filename = os.path.abspath("Documents/uzi_obp_pochki/shitovidka/output.jpg")
        # TODO: походу эта функция не создает папки
        self.PaintPanel.pixmap.save(img_filename)

        if not os.path.exists(img_filename):
            print('Warning: %s is not found' % img_filename)
            encoded_string = base64.b64encode('error')
        else:
            with open(img_filename, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())

        imageNodesHtml = '<p style="margin-bottom: 0in; line-height: 100%"><img src="data:image/jpeg;base64,' + encoded_string + '" name="Image1" align="left" width="798" height="350" border="0"><br></p>'
        #replace nodesImage with imageNodesHtml
        if self.ui.checkBoxAddNodes.isChecked() == False:
            repTextHtml = repTextHtml.replace("nodesImage", "")
        repTextHtml = repTextHtml.replace("nodesImage", imageNodesHtml)

        #=================================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)






def main():
    app = QtGui.QApplication(sys.argv)
    dummy = {'patient_sex': u'м'}
    ex = ShitovidkaWidget(dummy)
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
