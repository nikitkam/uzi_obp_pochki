# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'TestWidgetDesign.ui'
#
# Created: Tue Nov  4 12:56:43 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_TestForm(object):
    def setupUi(self, TestForm):
        TestForm.setObjectName("TestForm")
        TestForm.resize(498, 249)
        self.PB1 = QtGui.QPushButton(TestForm)
        self.PB1.setGeometry(QtCore.QRect(100, 110, 83, 24))
        self.PB1.setObjectName("PB1")
        self.PB2 = QtGui.QPushButton(TestForm)
        self.PB2.setGeometry(QtCore.QRect(240, 110, 83, 24))
        self.PB2.setObjectName("PB2")

        self.retranslateUi(TestForm)
        QtCore.QMetaObject.connectSlotsByName(TestForm)

    def retranslateUi(self, TestForm):
        TestForm.setWindowTitle(QtGui.QApplication.translate("TestForm", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.PB1.setText(QtGui.QApplication.translate("TestForm", "1", None, QtGui.QApplication.UnicodeUTF8))
        self.PB2.setText(QtGui.QApplication.translate("TestForm", "2", None, QtGui.QApplication.UnicodeUTF8))

