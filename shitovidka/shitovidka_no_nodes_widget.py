#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys


from PySide import QtCore, QtGui

from ShitovidkaWidgetDesignNoNodes import Ui_ShitovidkaWidget

# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common



class  ShitovidkaWidget(QtGui.QWidget):

    def __init__(self, _settings):
        super(ShitovidkaWidget, self).__init__()
        self.initUI()

        if _settings['patient_sex'] == u'м':
            self.ui.lineEdit25.setText('25')
        else:
            self.ui.lineEdit25.setText('18')
        self._settings = _settings


    def initUI(self):
        self.ui = Ui_ShitovidkaWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoChitovidnayaZheleza, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        QtCore.QObject.connect(self.ui.pushButtonGotovoChitovidnayaZheleza, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)


        self.ui.lineEdit9.textChanged.connect(self.volume19)
        self.ui.lineEdit12.textChanged.connect(self.volume19)
        self.ui.lineEdit15.textChanged.connect(self.volume19)

        self.ui.lineEdit10.textChanged.connect(self.volume20)
        self.ui.lineEdit13.textChanged.connect(self.volume20)
        self.ui.lineEdit16.textChanged.connect(self.volume20)

        self.ui.lineEdit11.textChanged.connect(self.volume21)
        self.ui.lineEdit14.textChanged.connect(self.volume21)
        self.ui.lineEdit17.textChanged.connect(self.volume21)

        self.ui.lineEdit19.textChanged.connect(self.volume23)
        self.ui.lineEdit20.textChanged.connect(self.volume23)
        self.ui.lineEdit21.textChanged.connect(self.volume23)

        self.ui.lineEdit23.textChanged.connect(self.percentage)


        # signals for user interaction
        self.ui.radioButton40.clicked.connect(self.showSW_2p1)
        self.ui.radioButton41.clicked.connect(self.showSW_2p2)



    def percentage(self):
        prevysh = ( 100 * float(self.ui.lineEdit23.text()) / float(self.ui.lineEdit25.text()) ) - 100
        if prevysh > 0:
            self.ui.lineEditPercentage.setText(str(prevysh))
        else:
            self.ui.lineEditPercentage.setText("-")

    def volume19(self):
        perzad = self.ui.lineEdit9.text()
        shir = self.ui.lineEdit12.text()
        dlin = self.ui.lineEdit15.text()
        if perzad != "" and shir  != "" and dlin  != "" :
            perzadF = float(perzad)
            shirF = float(shir)
            dlinF = float(dlin)
            volume =  "%.1f" % (perzadF * shirF * dlinF * (0.479 / 1000))
            self.ui.lineEdit19.setText(str(volume))


    def volume20(self):
        perzad = self.ui.lineEdit10.text()
        shir = self.ui.lineEdit13.text()
        dlin = self.ui.lineEdit16.text()
        if perzad != "" and shir  != "" and dlin  != "" :
            perzadF = float(perzad)
            shirF = float(shir)
            dlinF = float(dlin)
            volume =  "%.1f" % (perzadF * shirF * dlinF * (0.479 / 1000))
            self.ui.lineEdit20.setText(str(volume))


    def volume21(self):
        perzad = self.ui.lineEdit11.text()
        shir = self.ui.lineEdit14.text()
        dlin = self.ui.lineEdit17.text()
        if perzad != "" and shir  != "" and dlin  != "" :
            perzadF = float(perzad)
            shirF = float(shir)
            dlinF = float(dlin)
            volume =  "%.1f" % (perzadF * shirF * dlinF * (0.479 / 1000))
            self.ui.lineEdit21.setText(str(volume))



    def volume23(self):
        pr = self.ui.lineEdit19.text()
        lev = self.ui.lineEdit20.text()
        persh = self.ui.lineEdit21.text()
        if pr != "" and lev != "" and persh  != "" :
            prF = float(pr)
            levF = float(lev)
            pershF = float(persh)
            volume =  "%.1f" % (prF + levF +pershF)
            self.ui.lineEdit23.setText(str(volume))



    def showSW_2p1(self):
        self.ui.stackedWidget_2.setCurrentIndex(0)

    def showSW_2p2(self):
        self.ui.stackedWidget_2.setCurrentIndex(1)

    # 9 - 21 25
    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit9, self.ui.lineEdit10, self.ui.lineEdit11, self.ui.lineEdit12, self.ui.lineEdit13 , self.ui.lineEdit14,  self.ui.lineEdit15, self.ui.lineEdit16, self.ui.lineEdit17, self.ui.lineEdit19, self.ui.lineEdit20, self.ui.lineEdit21, self.ui.lineEdit25 ]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoChitovidnayaZheleza.click()

    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


########################## Генерация отчета ##################################

    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()

        repTextHtml = self.reportTemplate
        #=================================================

        #stackedWidget_2
        mid_str = ""
        if self.ui.stackedWidget_2.currentIndex() == 0 :
            if self.ui.checkBox42.isChecked() == True :
                mid_str = self.ui.checkBox42.text() + " " + self.ui.comboBox46.currentText()
        else :
            if self.ui.checkBox43.isChecked() == True:
                midRB1 = self.ui.radioButton44.text() if self.ui.radioButton44.isChecked() else "" 
                midRB2 = self.ui.radioButton45.text() if self.ui.radioButton45.isChecked() else "" 
                midRB3 = self.ui.radioButtonBoth1.text() if self.ui.radioButtonBoth1.isChecked() else "" 
                mid_str = self.ui.checkBox43.text() + " "  + " " + self.ui.comboBox47a.currentText() + " " + self.ui.comboBox47b.currentText() + " " + midRB1 + midRB2 + midRB3 
        repTextHtml = (repTextHtml.replace("sw_2", mid_str))

        #mid7172
        #mid_str = self.ui.checkBox71.text() + " " + self.ui.comboBox72.currentText() if self.ui.checkBox71.isChecked() else "" 
        #repTextHtml = (repTextHtml.replace("mid7172", mid_str))

        #mid8486
        mid_str = self.ui.radioButton84.text() + " " + self.ui.comboBox85.currentText() + self.ui.textEdit86.toHtml() if self.ui.radioButton84.isChecked() else "" 
        repTextHtml = (repTextHtml.replace("mid8486", mid_str))

        mid_str = self.ui.comboBox72.currentText() if self.ui.radioButton68.isChecked() else ""
        repTextHtml = (repTextHtml.replace("comboBox72", mid_str))


        repTextHtml = (repTextHtml.replace("comboBox27", self.ui.comboBox27.currentText()))

        #combo31
        mid_str = self.ui.comboBox31.currentText() if self.ui.radioButton30.isChecked() else "" 
        repTextHtml = (repTextHtml.replace("comboBox31", mid_str))


        # заменяем имена ui элементов в шаблоне на их значения
        repTextHtml = common.replace_radiobuttons(  repTextHtml
                                                  , self.ui
                                                  , regexp=r'radioButton[\w]*[0-9]{1,3}[\w]{0,2}'
                                                  , debug=False )

        repTextHtml = common.replace_checkboxes(  repTextHtml
                                                  , self.ui
                                                  , regexp=r'checkBox[0-9]{1,3}'
                                                  , debug=False )

        repTextHtml = common.replace_lineedits( repTextHtml
                                                , self.ui
                                                , regexp=r'lineEdit[0-9]{1,3}'
                                                , debug=False )


        #=================================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)






def main():
    app = QtGui.QApplication(sys.argv)
    dummy = {'patient_sex': u'м'}
    ex = ShitovidkaWidget(dummy)
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
