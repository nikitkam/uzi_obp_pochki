#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys

from PySide import QtGui

from TestWidgetDesign import Ui_TestForm
from NewNodeWidgetDesign import Ui_NewNodeWidget

# external dependency
# TODO: проверить, будет ли работать с bbfreeze
try:
    import bs4
except ImportError as e:
    print("ERROR: External dependency! You should manually install lxlm lib! \n"\
          "('sudo apt-get install python-bs4' in console)")
    sys.exit(1)

# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common



class NewNodeWidget(QtGui.QWidget):
    def __init__(self, _settings, nodeNum):
        super(NewNodeWidget, self).__init__()
        self._settings = _settings
        self.initUI()
        self.setWindowTitle(u'Узел номер ' + str(nodeNum))


    def onClosePBClicked(self):
        self.close()


###################### обработчики кнопок '+' и '-' ############################
# Это кнопки, добавляющие (или удаляющие) новые значения в комбобоксы.
# После того, как значение добавлено, его необходимо сохранить между вызовами
# программы. В данный момент все сохраняется в конфигурационном файле.
# Во время работы программы эти значения доступны через переменную _settings,
# передающуюся между виджетами.

    def SaveCBSettings(self, cb):
        # сохраняем значения строк комбо-бокса (вместе с только-что добавленным)
        # в дикто-подобной структуре с настройками, которая потом хранится в файле.
        # Чтобы не придумывать название для ключей, ключи в этом дикте будут
        # генериться автоматически.
        # TODO: что будет, если изменится имя комбобокса в дизайне? жопа?
        settings_key = "_%s_vaues" % cb.objectName()
        self._settings[settings_key] = common.get_combobox_values(cb)
        self._settings.save(silent=True)


    def add_combobox_value(self, combobox):
        """ Функция выводит диалог, в который вводится текст. После нажатия
        на ОК, этот текст появляется в комбобоксе на последней строке (нижней).
        combobox - объект, в который будет вставляться текст.
        """
        text, ok = QtGui.QInputDialog.getText(self, u'', u'Добавление нового значения')
        if not ok:
            return

        BOTTOM_INDEX = combobox.count()
        combobox.insertItem(BOTTOM_INDEX, text)
        combobox.setCurrentIndex(BOTTOM_INDEX)
        self.SaveCBSettings(combobox)


    def remove_current_combobox_value(self, combobox):
        """ Удаляет из комбобокса текущее значение. """
        ci = combobox.currentIndex()
        combobox.removeItem(ci)
        self.SaveCBSettings(combobox)


    # комбобокс "Эхогенность"
    def onAddEhoPBClicked(self):
        ehogennost_cb = self.ui.comboBox105
        self.add_combobox_value(ehogennost_cb)

    def onRemoveEhoPBClicked(self):
        ehogennost_cb = self.ui.comboBox105
        self.remove_current_combobox_value(ehogennost_cb)

    # комбобокс "Васкуляризация"
    def onAddVaskPBClicked(self):
        vaskular_cb = self.ui.comboBox112
        self.add_combobox_value(vaskular_cb)

    def onRemoveVaskPBClicked(self):
        vaskular_cb = self.ui.comboBox112
        self.remove_current_combobox_value(vaskular_cb)

    # комбобокс "Звукопроводимость"
    def onAddZvukPBClicked(self):
        zvuk_cb = self.ui.comboBox114
        self.add_combobox_value(zvuk_cb)

    def onRemoveZvukPBClicked(self):
        zvuk_cb = self.ui.comboBox114
        self.remove_current_combobox_value(zvuk_cb)

################## конец обработчиков '+' и '-' #########################


    def get_report_body(self):
        """generate_report_from_template возвращает полноценную html страницу,
        в то время, как в shitovidka_widget нужно будет вставлять 10 кусков html
        отчета об узловых патологиях. Т.е. нужно вернуть лишь то, что под тэгом
        <body> в получившемся отчете
        """
        template = self.ui.ReportTemplateTE.toHtml()

        full_report = self.generate_report_from_template(template)
        # DEBUG: вывод получившегося результата
        # self.ui.ReportTemplateTE.insertHtml(full_report)

        soup = bs4.BeautifulSoup(full_report)
        # TODO: пока возвращаем вместе с тэгом <body> - нужно будет убрать это.
        return unicode(soup.body)


    def get_report_table_row(self):
        """ Возвращает html строку таблицы (тэг <tr> ... </tr> с содержимым),
        заполненную значениями из виджета.
        """
        template = self.ui.textBrowserTableRow.toPlainText()
        html_table_row = self.generate_report_from_template(template)
        return html_table_row


    # TODO: diff с tool/zhelchny_widget.py,
    # вынести в common
    def generate_report_from_template(self, html_template):
        """ шаблон хранится в textedit self.ui.ReportTemplateTE """
        # заменяем имена графических элементов в шаблоне отчета
        # на их значения
        html_template = common.replace_lineedits(html_template, self.ui)
        html_template = common.replace_comboboxes(html_template, self.ui)
        html_template = common.replace_radiobuttons(html_template, self.ui)
        html_template = common.replace_checkboxes(html_template, self.ui)


        # заменяем подстроки "stackedWidget_[0-9]" на текст из радиобаттонов
        stacked_widgets = re.findall('stackedWidget_[0-9]', html_template)
        for sw_name in stacked_widgets:
            # получаем объект self.ui.stackedWidget_X
            sw = self.ui.__getattribute__(sw_name)
            # получаем page, на котором расположены радио-баттоны
            cw = sw.currentWidget()
            # если есть дочерние объекты, значит радио-баттоны отображаются на виджете
            # и их нужно обрабатывать
            children = cw.children()
            if children:
                # получаем имена объектов, расположенных на странице stackedWidget.
                obj_names = [ x.objectName() for x in children ]
                # собираем имена радио-баттонов (РБ), получаем объекты
                radio_buttons = [ self.ui.__getattribute__(name)
                                      for name in obj_names
                                          if re.match('radioButton[0-9]_[0-9]{1,3}', name) ]
                # собираем чекнутые РБ (должен быть 1)
                checked = [ rb for rb in radio_buttons if rb.isChecked() ]
                try:
                    sole_checked_rb_text = checked[0].text()
                except IndexError:
                    sole_checked_rb_text = ''
                # заменяем в шаблоне
                html_template = html_template.replace(sw_name, sole_checked_rb_text)
            else:
                html_template = html_template.replace(sw_name, '')

        return html_template



########################### UI дела #################################
    # TODO: убрать дублирование кода с main_menu_window
    def FillComboBox(self, combobox):
        """ Заполнение комбо-бокса значениями из файла настроек """

        settings_key = "_%s_vaues" % combobox.objectName()
        values_dict = self._settings.get(settings_key)
        if not values_dict:
            print('Key error: key=%s' % settings_key)
            return

        if type(values_dict) != dict:
            print('error: typecheck failed!')
            return

        # чистим ComboBox
        combobox.clear()
        # заполняем
        for index, text in values_dict.iteritems():
            combobox.insertItem(index, text)


    def initUI(self):
        self.ui = Ui_NewNodeWidget()
        self.ui.setupUi(self)

        # заполняем комбобоксы Эхогенность, Васкуляризация
        # Звукопроводимость (которые с '+' и '-') сохраненными значениями
        # из файла с настройками.
        ehogennost_cb = self.ui.comboBox105
        vaskular_cb = self.ui.comboBox112
        zvuk_cb = self.ui.comboBox114
        self.FillComboBox(ehogennost_cb)
        self.FillComboBox(vaskular_cb)
        self.FillComboBox(zvuk_cb)

        # signals
        self.ui.ClosePB.clicked.connect(self.onClosePBClicked)

        self.ui.lineEdit93.textChanged.connect(self.volume1)
        self.ui.lineEdit94.textChanged.connect(self.volume1)
        self.ui.lineEdit95.textChanged.connect(self.volume1)

        # кнопки '+' и '-', добавляющие строчки в комбобоксы
        self.ui.AddEhoPB.clicked.connect(self.onAddEhoPBClicked)
        self.ui.RemoveEhoPB.clicked.connect(self.onRemoveEhoPBClicked)

        self.ui.AddVaskPB.clicked.connect(self.onAddVaskPBClicked)
        self.ui.RemoveVaskPB.clicked.connect(self.onRemoveVaskPBClicked)

        self.ui.AddZvukPB.clicked.connect(self.onAddZvukPBClicked)
        self.ui.RemoveZvukPB.clicked.connect(self.onRemoveZvukPBClicked)



    def volume1(self):
        peredZad = self.ui.lineEdit93.text()
        shirina = self.ui.lineEdit94.text()
        dlina = self.ui.lineEdit95.text()
        if peredZad != "" and shirina != "" and dlina != "":
            peredZadF = float(peredZad)
            shirinaF = float(shirina)
            dlinaF = float(dlina)
            volume =  "%.1f" % (peredZadF * shirinaF * dlinaF * 0.479 / 1000)
            self.ui.lineEdit96.setText(str(volume))



class TestWidget(QtGui.QWidget):
    def __init__(self):
        super(TestWidget, self).__init__()
        self.initUI()
        self.w = NewNodeWidget()

    def onPB1Clicked(self):
        self.w.show()


    def onPB2Clicked(self):
        template = self.w.generate_report_from_template()
        self.w.ui.stackedWidget.setCurrentIndex(1)
        self.w.ui.ReportTemplateTE.clear()
        self.w.ui.ReportTemplateTE.insertHtml(template)
        self.w.show()


    def initUI(self):
        self.ui = Ui_TestForm()
        self.ui.setupUi(self)

        # signals
        self.ui.PB1.clicked.connect(self.onPB1Clicked)
        self.ui.PB2.clicked.connect(self.onPB2Clicked)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = TestWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
