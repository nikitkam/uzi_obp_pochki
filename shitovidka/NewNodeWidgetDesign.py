# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'NewNodeWidgetDesign.ui'
#
# Created: Tue Dec  2 15:41:27 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_NewNodeWidget(object):
    def setupUi(self, NewNodeWidget):
        NewNodeWidget.setObjectName("NewNodeWidget")
        NewNodeWidget.resize(628, 866)
        self.gridLayout = QtGui.QGridLayout(NewNodeWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.stackedWidget = QtGui.QStackedWidget(NewNodeWidget)
        font = QtGui.QFont()
        font.setFamily("Ubuntu")
        font.setPointSize(11)
        self.stackedWidget.setFont(font)
        self.stackedWidget.setObjectName("stackedWidget")
        self.page = QtGui.QWidget()
        self.page.setObjectName("page")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.page)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.widget_11 = QtGui.QWidget(self.page)
        self.widget_11.setObjectName("widget_11")
        self.gridLayout_2 = QtGui.QGridLayout(self.widget_11)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.line_62 = QtGui.QFrame(self.widget_11)
        self.line_62.setFrameShape(QtGui.QFrame.VLine)
        self.line_62.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_62.setObjectName("line_62")
        self.gridLayout_2.addWidget(self.line_62, 10, 5, 5, 1)
        self.comboBox110 = QtGui.QComboBox(self.widget_11)
        self.comboBox110.setObjectName("comboBox110")
        self.comboBox110.addItem("")
        self.comboBox110.addItem("")
        self.gridLayout_2.addWidget(self.comboBox110, 26, 2, 1, 1)
        self.comboBox102 = QtGui.QComboBox(self.widget_11)
        self.comboBox102.setObjectName("comboBox102")
        self.comboBox102.addItem("")
        self.comboBox102.addItem("")
        self.comboBox102.addItem("")
        self.gridLayout_2.addWidget(self.comboBox102, 12, 2, 1, 1)
        self.comboBox112 = QtGui.QComboBox(self.widget_11)
        self.comboBox112.setObjectName("comboBox112")
        self.comboBox112.addItem("")
        self.comboBox112.addItem("")
        self.comboBox112.addItem("")
        self.gridLayout_2.addWidget(self.comboBox112, 28, 2, 1, 1)
        self.lineEdit95 = QtGui.QLineEdit(self.widget_11)
        self.lineEdit95.setObjectName("lineEdit95")
        self.gridLayout_2.addWidget(self.lineEdit95, 6, 3, 1, 1)
        self.comboBox101 = QtGui.QComboBox(self.widget_11)
        self.comboBox101.setObjectName("comboBox101")
        self.comboBox101.addItem("")
        self.comboBox101.addItem("")
        self.comboBox101.addItem("")
        self.gridLayout_2.addWidget(self.comboBox101, 11, 2, 1, 1)
        self.line_36 = QtGui.QFrame(self.widget_11)
        self.line_36.setFrameShape(QtGui.QFrame.HLine)
        self.line_36.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_36.setObjectName("line_36")
        self.gridLayout_2.addWidget(self.line_36, 7, 0, 1, 8)
        self.label97 = QtGui.QLabel(self.widget_11)
        self.label97.setObjectName("label97")
        self.gridLayout_2.addWidget(self.label97, 11, 0, 1, 1)
        self.line_37 = QtGui.QFrame(self.widget_11)
        self.line_37.setFrameShape(QtGui.QFrame.VLine)
        self.line_37.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_37.setObjectName("line_37")
        self.gridLayout_2.addWidget(self.line_37, 10, 1, 5, 1)
        self.lineEdit96 = QtGui.QLineEdit(self.widget_11)
        self.lineEdit96.setObjectName("lineEdit96")
        self.gridLayout_2.addWidget(self.lineEdit96, 8, 2, 1, 1)
        self.line_45 = QtGui.QFrame(self.widget_11)
        self.line_45.setFrameShape(QtGui.QFrame.VLine)
        self.line_45.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_45.setObjectName("line_45")
        self.gridLayout_2.addWidget(self.line_45, 25, 1, 2, 1)
        self.widget_5 = QtGui.QWidget(self.widget_11)
        self.widget_5.setObjectName("widget_5")
        self.gridLayout_3 = QtGui.QGridLayout(self.widget_5)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.radioButton106 = QtGui.QRadioButton(self.widget_5)
        self.radioButton106.setChecked(True)
        self.radioButton106.setObjectName("radioButton106")
        self.gridLayout_3.addWidget(self.radioButton106, 0, 0, 1, 1)
        self.radioButton107 = QtGui.QRadioButton(self.widget_5)
        self.radioButton107.setObjectName("radioButton107")
        self.gridLayout_3.addWidget(self.radioButton107, 1, 0, 1, 1)
        self.gridLayout_2.addWidget(self.widget_5, 20, 2, 1, 1)
        self.label_2 = QtGui.QLabel(self.widget_11)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 5, 2, 1, 1)
        self.label_4 = QtGui.QLabel(self.widget_11)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 4, 4, 1, 1)
        self.checkBox2 = QtGui.QCheckBox(self.widget_11)
        self.checkBox2.setObjectName("checkBox2")
        self.gridLayout_2.addWidget(self.checkBox2, 22, 2, 1, 1)
        self.line_61 = QtGui.QFrame(self.widget_11)
        self.line_61.setFrameShape(QtGui.QFrame.VLine)
        self.line_61.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_61.setObjectName("line_61")
        self.gridLayout_2.addWidget(self.line_61, 16, 5, 2, 1)
        self.line_63 = QtGui.QFrame(self.widget_11)
        self.line_63.setFrameShape(QtGui.QFrame.VLine)
        self.line_63.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_63.setObjectName("line_63")
        self.gridLayout_2.addWidget(self.line_63, 1, 0, 1, 1)
        self.label_5 = QtGui.QLabel(self.widget_11)
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 5, 4, 1, 1)
        self.label_6 = QtGui.QLabel(self.widget_11)
        self.label_6.setObjectName("label_6")
        self.gridLayout_2.addWidget(self.label_6, 6, 4, 1, 1)
        self.label91 = QtGui.QLabel(self.widget_11)
        self.label91.setObjectName("label91")
        self.gridLayout_2.addWidget(self.label91, 4, 0, 1, 1)
        self.comboBox103 = QtGui.QComboBox(self.widget_11)
        self.comboBox103.setObjectName("comboBox103")
        self.comboBox103.addItem("")
        self.comboBox103.addItem("")
        self.gridLayout_2.addWidget(self.comboBox103, 13, 2, 1, 1)
        self.label = QtGui.QLabel(self.widget_11)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 4, 2, 1, 1)
        self.label101 = QtGui.QLabel(self.widget_11)
        self.label101.setObjectName("label101")
        self.gridLayout_2.addWidget(self.label101, 16, 0, 1, 1)
        self.lineEdit93 = QtGui.QLineEdit(self.widget_11)
        self.lineEdit93.setObjectName("lineEdit93")
        self.gridLayout_2.addWidget(self.lineEdit93, 4, 3, 1, 1)
        self.line_28 = QtGui.QFrame(self.widget_11)
        self.line_28.setFrameShape(QtGui.QFrame.VLine)
        self.line_28.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_28.setObjectName("line_28")
        self.gridLayout_2.addWidget(self.line_28, 2, 1, 5, 1)
        self.comboBox104 = QtGui.QComboBox(self.widget_11)
        self.comboBox104.setObjectName("comboBox104")
        self.comboBox104.addItem("")
        self.comboBox104.addItem("")
        self.gridLayout_2.addWidget(self.comboBox104, 14, 2, 1, 1)
        self.label111 = QtGui.QLabel(self.widget_11)
        self.label111.setObjectName("label111")
        self.gridLayout_2.addWidget(self.label111, 28, 0, 1, 1)
        self.line_31 = QtGui.QFrame(self.widget_11)
        self.line_31.setFrameShape(QtGui.QFrame.VLine)
        self.line_31.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_31.setObjectName("line_31")
        self.gridLayout_2.addWidget(self.line_31, 8, 5, 1, 1)
        self.line_29 = QtGui.QFrame(self.widget_11)
        self.line_29.setFrameShape(QtGui.QFrame.VLine)
        self.line_29.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_29.setObjectName("line_29")
        self.gridLayout_2.addWidget(self.line_29, 2, 5, 1, 1)
        self.label108 = QtGui.QLabel(self.widget_11)
        self.label108.setObjectName("label108")
        self.gridLayout_2.addWidget(self.label108, 25, 0, 1, 1)
        self.widget_12 = QtGui.QWidget(self.widget_11)
        self.widget_12.setObjectName("widget_12")
        self.label94 = QtGui.QLabel(self.widget_12)
        self.label94.setGeometry(QtCore.QRect(10, 0, 52, 27))
        self.label94.setObjectName("label94")
        self.textBrowser95 = QtGui.QTextBrowser(self.widget_12)
        self.textBrowser95.setGeometry(QtCore.QRect(70, 0, 50, 27))
        self.textBrowser95.setMinimumSize(QtCore.QSize(50, 27))
        self.textBrowser95.setMaximumSize(QtCore.QSize(50, 27))
        self.textBrowser95.setObjectName("textBrowser95")
        self.gridLayout_2.addWidget(self.widget_12, 8, 0, 1, 1)
        self.line_30 = QtGui.QFrame(self.widget_11)
        self.line_30.setFrameShape(QtGui.QFrame.VLine)
        self.line_30.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_30.setObjectName("line_30")
        self.gridLayout_2.addWidget(self.line_30, 4, 5, 3, 1)
        self.line_56 = QtGui.QFrame(self.widget_11)
        self.line_56.setFrameShape(QtGui.QFrame.VLine)
        self.line_56.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_56.setObjectName("line_56")
        self.gridLayout_2.addWidget(self.line_56, 30, 5, 1, 1)
        self.line_21 = QtGui.QFrame(self.widget_11)
        self.line_21.setFrameShape(QtGui.QFrame.HLine)
        self.line_21.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_21.setObjectName("line_21")
        self.gridLayout_2.addWidget(self.line_21, 3, 2, 1, 6)
        self.line_41 = QtGui.QFrame(self.widget_11)
        self.line_41.setFrameShape(QtGui.QFrame.VLine)
        self.line_41.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_41.setObjectName("line_41")
        self.gridLayout_2.addWidget(self.line_41, 16, 1, 2, 1)
        self.checkBox1 = QtGui.QCheckBox(self.widget_11)
        self.checkBox1.setObjectName("checkBox1")
        self.gridLayout_2.addWidget(self.checkBox1, 21, 2, 1, 1)
        self.line_40 = QtGui.QFrame(self.widget_11)
        self.line_40.setFrameShape(QtGui.QFrame.HLine)
        self.line_40.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_40.setObjectName("line_40")
        self.gridLayout_2.addWidget(self.line_40, 15, 0, 1, 8)
        self.line_32 = QtGui.QFrame(self.widget_11)
        self.line_32.setFrameShape(QtGui.QFrame.VLine)
        self.line_32.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_32.setObjectName("line_32")
        self.gridLayout_2.addWidget(self.line_32, 8, 1, 1, 1)
        self.line_59 = QtGui.QFrame(self.widget_11)
        self.line_59.setFrameShape(QtGui.QFrame.VLine)
        self.line_59.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_59.setObjectName("line_59")
        self.gridLayout_2.addWidget(self.line_59, 25, 5, 2, 1)
        self.comboBox114 = QtGui.QComboBox(self.widget_11)
        self.comboBox114.setObjectName("comboBox114")
        self.comboBox114.addItem("")
        self.comboBox114.addItem("")
        self.comboBox114.addItem("")
        self.comboBox114.addItem("")
        self.gridLayout_2.addWidget(self.comboBox114, 30, 2, 1, 1)
        self.line_48 = QtGui.QFrame(self.widget_11)
        self.line_48.setFrameShape(QtGui.QFrame.HLine)
        self.line_48.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_48.setObjectName("line_48")
        self.gridLayout_2.addWidget(self.line_48, 29, 0, 1, 8)
        self.widget = QtGui.QWidget(self.widget_11)
        self.widget.setObjectName("widget")
        self.gridLayout_4 = QtGui.QGridLayout(self.widget)
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.radioButton102 = QtGui.QRadioButton(self.widget)
        self.radioButton102.setChecked(True)
        self.radioButton102.setObjectName("radioButton102")
        self.gridLayout_4.addWidget(self.radioButton102, 0, 0, 1, 1)
        self.radioButton103 = QtGui.QRadioButton(self.widget)
        self.radioButton103.setObjectName("radioButton103")
        self.gridLayout_4.addWidget(self.radioButton103, 1, 0, 1, 1)
        self.gridLayout_2.addWidget(self.widget, 16, 2, 2, 1)
        self.lineEdit94 = QtGui.QLineEdit(self.widget_11)
        self.lineEdit94.setObjectName("lineEdit94")
        self.gridLayout_2.addWidget(self.lineEdit94, 5, 3, 1, 1)
        self.line_47 = QtGui.QFrame(self.widget_11)
        self.line_47.setFrameShape(QtGui.QFrame.HLine)
        self.line_47.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_47.setObjectName("line_47")
        self.gridLayout_2.addWidget(self.line_47, 27, 0, 1, 8)
        self.label113 = QtGui.QLabel(self.widget_11)
        self.label113.setObjectName("label113")
        self.gridLayout_2.addWidget(self.label113, 30, 0, 1, 1)
        self.line_44 = QtGui.QFrame(self.widget_11)
        self.line_44.setFrameShape(QtGui.QFrame.HLine)
        self.line_44.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_44.setObjectName("line_44")
        self.gridLayout_2.addWidget(self.line_44, 24, 0, 1, 8)
        self.line_51 = QtGui.QFrame(self.widget_11)
        self.line_51.setFrameShape(QtGui.QFrame.VLine)
        self.line_51.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_51.setObjectName("line_51")
        self.gridLayout_2.addWidget(self.line_51, 30, 1, 1, 1)
        self.label104 = QtGui.QLabel(self.widget_11)
        self.label104.setObjectName("label104")
        self.gridLayout_2.addWidget(self.label104, 19, 0, 1, 1)
        self.comboBox109 = QtGui.QComboBox(self.widget_11)
        self.comboBox109.setObjectName("comboBox109")
        self.comboBox109.addItem("")
        self.comboBox109.addItem("")
        self.gridLayout_2.addWidget(self.comboBox109, 25, 2, 1, 1)
        self.line_42 = QtGui.QFrame(self.widget_11)
        self.line_42.setFrameShape(QtGui.QFrame.HLine)
        self.line_42.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_42.setObjectName("line_42")
        self.gridLayout_2.addWidget(self.line_42, 18, 0, 1, 8)
        self.line_50 = QtGui.QFrame(self.widget_11)
        self.line_50.setFrameShape(QtGui.QFrame.VLine)
        self.line_50.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_50.setObjectName("line_50")
        self.gridLayout_2.addWidget(self.line_50, 28, 1, 1, 1)
        self.line_39 = QtGui.QFrame(self.widget_11)
        self.line_39.setFrameShape(QtGui.QFrame.HLine)
        self.line_39.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_39.setObjectName("line_39")
        self.gridLayout_2.addWidget(self.line_39, 9, 0, 1, 8)
        self.line_35 = QtGui.QFrame(self.widget_11)
        self.line_35.setFrameShape(QtGui.QFrame.HLine)
        self.line_35.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_35.setObjectName("line_35")
        self.gridLayout_2.addWidget(self.line_35, 0, 0, 1, 8)
        self.line_58 = QtGui.QFrame(self.widget_11)
        self.line_58.setFrameShape(QtGui.QFrame.VLine)
        self.line_58.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_58.setObjectName("line_58")
        self.gridLayout_2.addWidget(self.line_58, 28, 5, 1, 1)
        self.label_3 = QtGui.QLabel(self.widget_11)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 6, 2, 1, 1)
        self.checkBox3 = QtGui.QCheckBox(self.widget_11)
        self.checkBox3.setObjectName("checkBox3")
        self.gridLayout_2.addWidget(self.checkBox3, 23, 2, 1, 1)
        self.line_43 = QtGui.QFrame(self.widget_11)
        self.line_43.setFrameShape(QtGui.QFrame.VLine)
        self.line_43.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_43.setObjectName("line_43")
        self.gridLayout_2.addWidget(self.line_43, 19, 1, 5, 1)
        self.line_60 = QtGui.QFrame(self.widget_11)
        self.line_60.setFrameShape(QtGui.QFrame.VLine)
        self.line_60.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_60.setObjectName("line_60")
        self.gridLayout_2.addWidget(self.line_60, 19, 5, 5, 1)
        self.comboBox105 = QtGui.QComboBox(self.widget_11)
        self.comboBox105.setObjectName("comboBox105")
        self.comboBox105.addItem("")
        self.comboBox105.addItem("")
        self.comboBox105.addItem("")
        self.comboBox105.addItem("")
        self.comboBox105.addItem("")
        self.gridLayout_2.addWidget(self.comboBox105, 19, 2, 1, 1)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.AddVaskPB = QtGui.QPushButton(self.widget_11)
        self.AddVaskPB.setMaximumSize(QtCore.QSize(30, 30))
        self.AddVaskPB.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/iconPrefix/plus_sign.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.AddVaskPB.setIcon(icon)
        self.AddVaskPB.setObjectName("AddVaskPB")
        self.horizontalLayout_5.addWidget(self.AddVaskPB)
        self.RemoveVaskPB = QtGui.QPushButton(self.widget_11)
        self.RemoveVaskPB.setMaximumSize(QtCore.QSize(30, 30))
        self.RemoveVaskPB.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/iconPrefix/minus_sign.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.RemoveVaskPB.setIcon(icon1)
        self.RemoveVaskPB.setObjectName("RemoveVaskPB")
        self.horizontalLayout_5.addWidget(self.RemoveVaskPB)
        self.gridLayout_2.addLayout(self.horizontalLayout_5, 28, 3, 1, 1)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.AddEhoPB = QtGui.QPushButton(self.widget_11)
        self.AddEhoPB.setMaximumSize(QtCore.QSize(30, 30))
        self.AddEhoPB.setText("")
        self.AddEhoPB.setIcon(icon)
        self.AddEhoPB.setObjectName("AddEhoPB")
        self.horizontalLayout_6.addWidget(self.AddEhoPB)
        self.RemoveEhoPB = QtGui.QPushButton(self.widget_11)
        self.RemoveEhoPB.setMaximumSize(QtCore.QSize(30, 30))
        font = QtGui.QFont()
        font.setFamily("Ubuntu")
        font.setPointSize(11)
        self.RemoveEhoPB.setFont(font)
        self.RemoveEhoPB.setText("")
        self.RemoveEhoPB.setIcon(icon1)
        self.RemoveEhoPB.setObjectName("RemoveEhoPB")
        self.horizontalLayout_6.addWidget(self.RemoveEhoPB)
        self.gridLayout_2.addLayout(self.horizontalLayout_6, 19, 3, 1, 1)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.AddZvukPB = QtGui.QPushButton(self.widget_11)
        self.AddZvukPB.setMaximumSize(QtCore.QSize(30, 30))
        self.AddZvukPB.setText("")
        self.AddZvukPB.setIcon(icon)
        self.AddZvukPB.setObjectName("AddZvukPB")
        self.horizontalLayout_7.addWidget(self.AddZvukPB)
        self.RemoveZvukPB = QtGui.QPushButton(self.widget_11)
        self.RemoveZvukPB.setMaximumSize(QtCore.QSize(30, 30))
        self.RemoveZvukPB.setText("")
        self.RemoveZvukPB.setIcon(icon1)
        self.RemoveZvukPB.setObjectName("RemoveZvukPB")
        self.horizontalLayout_7.addWidget(self.RemoveZvukPB)
        self.gridLayout_2.addLayout(self.horizontalLayout_7, 30, 3, 1, 1)
        self.verticalLayout_2.addWidget(self.widget_11)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.ClosePB = QtGui.QPushButton(self.page)
        self.ClosePB.setObjectName("ClosePB")
        self.horizontalLayout.addWidget(self.ClosePB)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.stackedWidget.addWidget(self.page)
        self.page_2 = QtGui.QWidget()
        self.page_2.setObjectName("page_2")
        self.verticalLayout = QtGui.QVBoxLayout(self.page_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.ReportTemplateTE = QtGui.QTextEdit(self.page_2)
        self.ReportTemplateTE.setObjectName("ReportTemplateTE")
        self.verticalLayout.addWidget(self.ReportTemplateTE)
        self.widget_nodes_text = QtGui.QWidget(self.page_2)
        self.widget_nodes_text.setObjectName("widget_nodes_text")
        self.gridLayout_5 = QtGui.QGridLayout(self.widget_nodes_text)
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.textBrowserTableRow = QtGui.QTextBrowser(self.widget_nodes_text)
        self.textBrowserTableRow.setObjectName("textBrowserTableRow")
        self.gridLayout_5.addWidget(self.textBrowserTableRow, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.widget_nodes_text)
        self.stackedWidget.addWidget(self.page_2)
        self.gridLayout.addWidget(self.stackedWidget, 0, 0, 1, 1)

        self.retranslateUi(NewNodeWidget)
        self.stackedWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(NewNodeWidget)
        NewNodeWidget.setTabOrder(self.lineEdit93, self.lineEdit94)
        NewNodeWidget.setTabOrder(self.lineEdit94, self.lineEdit95)
        NewNodeWidget.setTabOrder(self.lineEdit95, self.textBrowser95)
        NewNodeWidget.setTabOrder(self.textBrowser95, self.lineEdit96)
        NewNodeWidget.setTabOrder(self.lineEdit96, self.comboBox101)
        NewNodeWidget.setTabOrder(self.comboBox101, self.comboBox102)
        NewNodeWidget.setTabOrder(self.comboBox102, self.comboBox103)
        NewNodeWidget.setTabOrder(self.comboBox103, self.comboBox104)
        NewNodeWidget.setTabOrder(self.comboBox104, self.radioButton102)
        NewNodeWidget.setTabOrder(self.radioButton102, self.radioButton103)
        NewNodeWidget.setTabOrder(self.radioButton103, self.comboBox105)
        NewNodeWidget.setTabOrder(self.comboBox105, self.AddEhoPB)
        NewNodeWidget.setTabOrder(self.AddEhoPB, self.RemoveEhoPB)
        NewNodeWidget.setTabOrder(self.RemoveEhoPB, self.radioButton106)
        NewNodeWidget.setTabOrder(self.radioButton106, self.radioButton107)
        NewNodeWidget.setTabOrder(self.radioButton107, self.checkBox1)
        NewNodeWidget.setTabOrder(self.checkBox1, self.checkBox2)
        NewNodeWidget.setTabOrder(self.checkBox2, self.checkBox3)
        NewNodeWidget.setTabOrder(self.checkBox3, self.comboBox109)
        NewNodeWidget.setTabOrder(self.comboBox109, self.comboBox110)
        NewNodeWidget.setTabOrder(self.comboBox110, self.comboBox112)
        NewNodeWidget.setTabOrder(self.comboBox112, self.AddVaskPB)
        NewNodeWidget.setTabOrder(self.AddVaskPB, self.RemoveVaskPB)
        NewNodeWidget.setTabOrder(self.RemoveVaskPB, self.comboBox114)
        NewNodeWidget.setTabOrder(self.comboBox114, self.AddZvukPB)
        NewNodeWidget.setTabOrder(self.AddZvukPB, self.RemoveZvukPB)
        NewNodeWidget.setTabOrder(self.RemoveZvukPB, self.ClosePB)
        NewNodeWidget.setTabOrder(self.ClosePB, self.ReportTemplateTE)
        NewNodeWidget.setTabOrder(self.ReportTemplateTE, self.textBrowserTableRow)

    def retranslateUi(self, NewNodeWidget):
        NewNodeWidget.setWindowTitle(QtGui.QApplication.translate("NewNodeWidget", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox110.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "ровные", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox110.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "неровные", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox102.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "в/полюс", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox102.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "н/полюс", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox102.setItemText(2, QtGui.QApplication.translate("NewNodeWidget", "ц/отдел", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox112.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "не выражена", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox112.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "ободочный кровоток", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox112.setItemText(2, QtGui.QApplication.translate("NewNodeWidget", "внутриузловой кровоток", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox101.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "правая доля", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox101.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "левая доля", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox101.setItemText(2, QtGui.QApplication.translate("NewNodeWidget", "перешеек", None, QtGui.QApplication.UnicodeUTF8))
        self.label97.setText(QtGui.QApplication.translate("NewNodeWidget", "Локализация", None, QtGui.QApplication.UnicodeUTF8))
        self.radioButton106.setText(QtGui.QApplication.translate("NewNodeWidget", "с эхонегативным компонентом", None, QtGui.QApplication.UnicodeUTF8))
        self.radioButton107.setText(QtGui.QApplication.translate("NewNodeWidget", "с изоэхогенным компонентом", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("NewNodeWidget", "ширина", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("NewNodeWidget", "мм", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBox2.setText(QtGui.QApplication.translate("NewNodeWidget", "с единичным кальцинатом", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("NewNodeWidget", "мм", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("NewNodeWidget", "мм", None, QtGui.QApplication.UnicodeUTF8))
        self.label91.setText(QtGui.QApplication.translate("NewNodeWidget", "Размер (п/з-шдл), мм", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox103.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "передняя поверхность", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox103.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "задняя поверхность", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("NewNodeWidget", "передне-задний", None, QtGui.QApplication.UnicodeUTF8))
        self.label101.setText(QtGui.QApplication.translate("NewNodeWidget", "Эхоструктура", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox104.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "медиальный край", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox104.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "латеральный край", None, QtGui.QApplication.UnicodeUTF8))
        self.label111.setText(QtGui.QApplication.translate("NewNodeWidget", "Васкуляризация", None, QtGui.QApplication.UnicodeUTF8))
        self.label108.setText(QtGui.QApplication.translate("NewNodeWidget", "Контуры", None, QtGui.QApplication.UnicodeUTF8))
        self.label94.setText(QtGui.QApplication.translate("NewNodeWidget", "Объем,", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser95.setHtml(QtGui.QApplication.translate("NewNodeWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">см<span style=\" vertical-align:super;\">3</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBox1.setText(QtGui.QApplication.translate("NewNodeWidget", "с гипоэхогенным ободком", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox114.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "не изменена", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox114.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "с усилением по заднему контуру", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox114.setItemText(2, QtGui.QApplication.translate("NewNodeWidget", "с ослаблением по заднему контуру", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox114.setItemText(3, QtGui.QApplication.translate("NewNodeWidget", "акустическая тень", None, QtGui.QApplication.UnicodeUTF8))
        self.radioButton102.setText(QtGui.QApplication.translate("NewNodeWidget", "однородная", None, QtGui.QApplication.UnicodeUTF8))
        self.radioButton103.setText(QtGui.QApplication.translate("NewNodeWidget", "неоднородная", None, QtGui.QApplication.UnicodeUTF8))
        self.label113.setText(QtGui.QApplication.translate("NewNodeWidget", "Звукопроводимость", None, QtGui.QApplication.UnicodeUTF8))
        self.label104.setText(QtGui.QApplication.translate("NewNodeWidget", "Эхогенность", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox109.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "четкие", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox109.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "нечеткие", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("NewNodeWidget", "длина", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBox3.setText(QtGui.QApplication.translate("NewNodeWidget", "с множественными кальцинатами", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox105.setItemText(0, QtGui.QApplication.translate("NewNodeWidget", "изоэхогенный", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox105.setItemText(1, QtGui.QApplication.translate("NewNodeWidget", "гипоэхогенный", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox105.setItemText(2, QtGui.QApplication.translate("NewNodeWidget", "эхонегативный", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox105.setItemText(3, QtGui.QApplication.translate("NewNodeWidget", "повышенной эхогенности", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox105.setItemText(4, QtGui.QApplication.translate("NewNodeWidget", "гиперэхогенный", None, QtGui.QApplication.UnicodeUTF8))
        self.ClosePB.setText(QtGui.QApplication.translate("NewNodeWidget", "close", None, QtGui.QApplication.UnicodeUTF8))
        self.ReportTemplateTE.setHtml(QtGui.QApplication.translate("NewNodeWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans Serif\'; font-size:12pt; font-weight:600;\">Узел NewNodeNumber: </span><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">размер - lineEdit93 X lineEdit94 X lineEdit95 мм, объем - lineEdit96 см</span><span style=\" font-family:\'Sans Serif\'; font-size:12pt; vertical-align:super;\">3</span><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">, </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">локализация - comboBox101, comboBox102, comboBox103, comboBox104,</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">эхоструктура - radioButton102 radioButton103,</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">эхогенность - comboBox105 radioButton106 radioButton107 checkBox1 checkBox2 checkBox3,</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">контуры - comboBox109 comboBox110,</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">васкуляризация - comboBox112,</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Sans Serif\'; font-size:12pt;\">звукопроводимость - comboBox114.</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowserTableRow.setHtml(QtGui.QApplication.translate("NewNodeWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;tr&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Узел NewNodeNumber &lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;lineEdit93 lineEdit94 lineEdit95&lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;lineEdit96&lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;comboBox101 comboBox102 comboBox103 comboBox104&lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;radioButton102 radioButton103&lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;comboBox105 radioButton106 radioButton107 checkBox1 checkBox2 checkBox3&lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;comboBox109 comboBox110&lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;comboBox112&lt;/p&gt;&lt;/td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;td&gt;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;comboBox114&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

import icons_rc
