# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ReportViewerWidgetDesign.ui'
#
# Created: Wed Jan 21 13:33:09 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_ReportViewer(object):
    def setupUi(self, ReportViewer):
        ReportViewer.setObjectName("ReportViewer")
        ReportViewer.resize(1034, 726)
        self.centralwidget = QtGui.QWidget(ReportViewer)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textBrowser = QtGui.QTextBrowser(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Ubuntu")
        font.setPointSize(11)
        self.textBrowser.setFont(font)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        ReportViewer.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(ReportViewer)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1034, 19))
        self.menubar.setObjectName("menubar")
        ReportViewer.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(ReportViewer)
        self.statusbar.setObjectName("statusbar")
        ReportViewer.setStatusBar(self.statusbar)

        self.retranslateUi(ReportViewer)
        QtCore.QMetaObject.connectSlotsByName(ReportViewer)

    def retranslateUi(self, ReportViewer):
        ReportViewer.setWindowTitle(QtGui.QApplication.translate("ReportViewer", "Просмотр отчета", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("ReportViewer", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

