#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, array

from PySide import QtCore, QtGui

from MochevoiPuzirWidgetDesign import Ui_MochevoiWidget

class MochevoiWidget(QtGui.QWidget):
    def __init__(self):
        super(MochevoiWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_MochevoiWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoMochevoiPuzir, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        QtCore.QObject.connect(self.ui.pushButtonGotovoMochevoiPuzir, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)
        self.ui.comboBox9.currentIndexChanged.connect(self.comboBox9React)
        self.ui.lineEdit7.textChanged.connect(self.check7)
        self.ui.lineEdit29.textChanged.connect(self.countPercentage)

    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit7, self.ui.lineEdit26, self.ui.lineEdit29, self.ui.lineEdit32]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoMochevoiPuzir.click()

    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)

    def comboBox9React(self, currentIndex):
        if currentIndex == 1:
            self.ui.widget_21.setEnabled(True)
        else :
            self.ui.widget_21.setDisabled(True)

    def countPercentage(self, inputString):
        if inputString == "":
            return(0)
        curVal = 0 
        try:
            curVal = float(inputString)
        except ValueError:
            print("Warning: inputed value is not a float value")
        ostVal = float(inputString)
        wholeVal = float(self.ui.lineEdit26.text())
        percentage = str(100 * ostVal / wholeVal)
        self.ui.lineEdit32.setText(percentage[0:5])


    def check7(self, inputString):
        if inputString == "":
            return(0)
        curVal = 0 
        try:
            curVal = float(inputString)
        except ValueError:
            print("Warning: inputed value is not a float value")
        if (float(inputString) >= 2 and float(inputString) <= 4 ):
            self.ui.comboBox9.setCurrentIndex(0)
        else:
            if float(inputString) > 4 :
                self.ui.comboBox9.setCurrentIndex(1)
            else :
                self.ui.comboBox9.setCurrentIndex(2)

    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate
        #=================================================
        #line_edits
        repTextHtml = (repTextHtml.replace("lineEdit7", self.ui.lineEdit7.text()))
        repTextHtml = (repTextHtml.replace("lineEdit19", self.ui.lineEdit19.text()))
        repTextHtml = (repTextHtml.replace("lineEdit26", self.ui.lineEdit26.text()))
        repTextHtml = (repTextHtml.replace("lineEdit29", self.ui.lineEdit29.text()))
        repTextHtml = (repTextHtml.replace("lineEdit32", self.ui.lineEdit32.text()))

        #textEdit
        repTextHtml = (repTextHtml.replace("textEdit24", self.ui.textEdit24.toPlainText()))
        repTextHtml = (repTextHtml.replace("textEdit35", self.ui.textEdit35.toPlainText()))

        #comboBoxes
        repTextHtml = (repTextHtml.replace("comboBox3", self.ui.comboBox3.currentText(),1))
        repTextHtml = (repTextHtml.replace("comboBox4", self.ui.comboBox4.currentText(),1))
        repTextHtml = (repTextHtml.replace("comboBox9", self.ui.comboBox9.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox18", self.ui.comboBox18.currentText()))

        #checkBox
        mid_str = self.ui.checkBox5.text() if self.ui.checkBox5.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox5", mid_str)
        mid_str = self.ui.checkBox_1.text() if self.ui.checkBox_1.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox_1", mid_str)
        mid_str = self.ui.checkBox_2.text() if self.ui.checkBox_2.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox_2", mid_str)
        mid_str = self.ui.checkBox_3.text() if self.ui.checkBox_3.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox_3", mid_str)
        mid_str = self.ui.checkBox_4.text() if self.ui.checkBox_4.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox_4", mid_str)


        #radiobuttons
        mid_str = self.ui.radioButton13.text() if self.ui.radioButton13.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton13", mid_str)
        mid_str = self.ui.radioButton14.text() if self.ui.radioButton14.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton14", mid_str)
        mid_str = self.ui.radioButton16.text() if self.ui.radioButton16.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton16", mid_str)
        mid_str = self.ui.radioButton17.text() if self.ui.radioButton17.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton17", " ")
        mid_str = self.ui.radioButton21.text() if self.ui.radioButton21.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton21", mid_str)
        mid_str = self.ui.radioButton22.text() if self.ui.radioButton22.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton22", " ")

        if self.ui.widget_21.isEnabled() == True:
            mid_str = self.ui.radioButton10.text() if self.ui.radioButton10.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton10", mid_str)
            mid_str = self.ui.radioButton11.text() if self.ui.radioButton11.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton11", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton10", "")
            repTextHtml = repTextHtml.replace("radioButton11", "")




        #=================================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)



def main():
    app = QtGui.QApplication(sys.argv)
    ex = MochevoiWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()