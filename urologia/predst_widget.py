#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, array
import os

from PySide import QtCore, QtGui

from PredstZhelezaWidgetDesign import Ui_PredstZhelezaWidget


# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common


class  PredstZhelezaWidget(QtGui.QWidget):
    def __init__(self):
        super(PredstZhelezaWidget, self).__init__()
        self.initUI()



    def initUI(self):
        self.ui = Ui_PredstZhelezaWidget()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # store html report template to use it multiple times
        self.reportTemplate = self.ui.textBrowserReport.toHtml()

        # signals to update report preview
        QtCore.QObject.connect(self.ui.pushButtonGotovoPredstZheleza, QtCore.SIGNAL("clicked(bool)"), self.updateReportPreview)
        QtCore.QObject.connect(self.ui.tabWidget, QtCore.SIGNAL("currentChanged(int)"), self.pushGotovoOnTabTextSwitched)
        QtCore.QObject.connect(self.ui.pushButtonGotovoPredstZheleza, QtCore.SIGNAL("clicked(bool)"), self.switchTabToText)
        # back button signal
        QtCore.QObject.connect(self.ui.pushButtonBackToClicker, QtCore.SIGNAL("clicked(bool)"), self.backToClicker)

        self.ui.lineEdit6.textChanged.connect(self.volume1)
        self.ui.lineEdit9.textChanged.connect(self.volume1)
        self.ui.lineEdit12.textChanged.connect(self.volume1)

        self.ui.lineEdit15.textChanged.connect(self.volume2)

        self.ui.lineEdit15.textChanged.connect(self.check3)


    def volume1(self):
        peredZad = self.ui.lineEdit6.text()
        pop = self.ui.lineEdit9.text()
        verhNiz = self.ui.lineEdit12.text()
        if peredZad != "" and pop != "" and verhNiz  != "" :
            peredZadF = float(peredZad)
            popF = float(pop)
            verhNizF = float(verhNiz)
            volume =  "%.1f" % (peredZadF * popF * verhNizF * 0.52 / 1000)
            self.ui.lineEdit15.setText(str(volume)) 


    def volume2(self):
        volume = self.ui.lineEdit15.text()
        if volume != "" :
            volumeF = float(volume)
            PSA = volumeF * 0.12
            self.ui.lineEdit20.setText(str(PSA))           


    def InstallInputValidators(self):
        floatValidator = QtGui.QDoubleValidator(self)
        text_edits = [self.ui.lineEdit6, self.ui.lineEdit9, self.ui.lineEdit12, self.ui.lineEdit15, self.ui.lineEdit20 ]
        map(lambda x: x.setValidator(floatValidator), text_edits)


    def pushGotovoOnTabTextSwitched(self):
        if self.ui.tabWidget.currentIndex() == 1:
            self.ui.pushButtonGotovoPredstZheleza.click()
 
    def backToClicker(self):
        self.ui.tabWidget.setCurrentIndex(0)

    def switchTabToText(self):
        self.ui.tabWidget.setCurrentIndex(1)


    def check3(self):
        common.check_le_and_switch_cb(
            self.ui.lineEdit15
            , self.ui.comboBox2
            , check_value=25
            , index_less=0
            , index_bigger=1 ) 



    def updateReportPreview(self):
        # delate data from text browser
        self.ui.textBrowserReport.clear()
        repTextHtml = self.reportTemplate
        #=================================================
        #line_edits
        repTextHtml = (repTextHtml.replace("lineEdit6", self.ui.lineEdit6.text()))
        repTextHtml = (repTextHtml.replace("lineEdit9", self.ui.lineEdit9.text()))
        repTextHtml = (repTextHtml.replace("lineEdit12", self.ui.lineEdit12.text()))
        repTextHtml = (repTextHtml.replace("lineEdit15", self.ui.lineEdit15.text()))
        repTextHtml = (repTextHtml.replace("lineEdit20", self.ui.lineEdit20.text()))
        repTextHtml = (repTextHtml.replace("lineEdit38", self.ui.lineEdit38.text()))
        repTextHtml = (repTextHtml.replace("lineEdit47", self.ui.lineEdit47.text()))
        repTextHtml = (repTextHtml.replace("lineEdit52", self.ui.lineEdit52.text()))
        repTextHtml = (repTextHtml.replace("lineEdit55", self.ui.lineEdit55.text()))


        #comboBoxes
        repTextHtml = (repTextHtml.replace("comboBox2", self.ui.comboBox2.currentText(),1))
        repTextHtml = (repTextHtml.replace("comboBox3", self.ui.comboBox3.currentText(),1))
        repTextHtml = (repTextHtml.replace("comboBox29", self.ui.comboBox29.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox31", self.ui.comboBox31.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox32", self.ui.comboBox32.currentText()))
        #repTextHtml = (repTextHtml.replace("comboBox51", self.ui.comboBox51.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox54", self.ui.comboBox54.currentText()))
        repTextHtml = (repTextHtml.replace("comboBox56", self.ui.comboBox56.currentText()))

        #radiobuttons
        mid_str = self.ui.radioButton36.text() if self.ui.radioButton36.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton36", mid_str)

        mid_str = self.ui.radioButton37.text() if self.ui.radioButton37.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton37", mid_str)

        mid_str = self.ui.radioButton40_1.text() if self.ui.radioButton40_1.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton40_1", mid_str)

        mid_str = self.ui.radioButton40_2.text() if self.ui.radioButton40_2.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton40_2", mid_str)

        mid_str = self.ui.radioButton44_1.text() if self.ui.radioButton44_1.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton44_1", mid_str)

        mid_str = self.ui.radioButton44_2.text() if self.ui.radioButton44_2.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton44_2", mid_str)

        mid_str = self.ui.radioButton49.text() if self.ui.radioButton49.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton49", mid_str)

        mid_str = self.ui.radioButton50.text() if self.ui.radioButton50.isChecked() else " " 
        repTextHtml = repTextHtml.replace("radioButton50", " ")

        #checkBoox
        mid_str = self.ui.checkBox1.text() if self.ui.checkBox1.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox1", mid_str)
        mid_str = self.ui.checkBox2.text() if self.ui.checkBox2.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox2", mid_str)
        mid_str = self.ui.checkBox3.text() if self.ui.checkBox3.isChecked() else " " 
        repTextHtml = repTextHtml.replace("checkBox3", mid_str)



        if self.ui.widget_21.isEnabled() == True:
            mid_str = self.ui.radioButton41.text() if self.ui.radioButton41.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton41", mid_str)
            mid_str = self.ui.radioButton42.text() if self.ui.radioButton42.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton42", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton41", "")
            repTextHtml = repTextHtml.replace("radioButton42", "")


        if self.ui.widget_22.isEnabled() == True:
            mid_str = self.ui.radioButton45.text() if self.ui.radioButton45.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton45", mid_str)
            mid_str = self.ui.radioButton46.text() if self.ui.radioButton46.isChecked() else " " 
            repTextHtml = repTextHtml.replace("radioButton46", mid_str)
        else : 
            repTextHtml = repTextHtml.replace("radioButton45", "")
            repTextHtml = repTextHtml.replace("radioButton46", "")

        if self.ui.comboBox51.isEnabled() == True:
        	repTextHtml = (repTextHtml.replace("comboBox51", self.ui.comboBox51.currentText()))
        else:
        	repTextHtml = (repTextHtml.replace("comboBox51", ""))
      

        #=================================================
        self.ui.textBrowserReport.insertHtml(repTextHtml)



def main():
    app = QtGui.QApplication(sys.argv)
    ex = PredstZhelezaWidget()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()