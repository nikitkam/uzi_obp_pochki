#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

# добавляем в sys.path папку, из которой можно по абсолютному
# пути импортить модули. Типа _common_dir.common
DIR_PATH = os.path.dirname(__file__)
MAIN_DIR_PATH = os.path.abspath(os.path.join(DIR_PATH, '..'))
print('[...] adding %s into sys path' % MAIN_DIR_PATH)

if MAIN_DIR_PATH not in sys.path:
    sys.path.append(MAIN_DIR_PATH)

