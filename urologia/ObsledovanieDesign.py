# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ObsledovanieDesign.ui'
#
# Created: Wed Apr  1 10:47:50 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Obsledovanie(object):
    def setupUi(self, Obsledovanie):
        Obsledovanie.setObjectName("Obsledovanie")
        Obsledovanie.resize(1052, 606)
        self.centralwidget = QtGui.QWidget(Obsledovanie)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.stackedWidgetMain = QtGui.QStackedWidget(self.centralwidget)
        self.stackedWidgetMain.setObjectName("stackedWidgetMain")
        self.verticalLayout.addWidget(self.stackedWidgetMain)
        self.gridLayout.addLayout(self.verticalLayout, 0, 2, 2, 1)
        self.widget_4 = QtGui.QWidget(self.centralwidget)
        self.widget_4.setObjectName("widget_4")
        self.gridLayout_11 = QtGui.QGridLayout(self.widget_4)
        self.gridLayout_11.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_11.setObjectName("gridLayout_11")
        spacerItem = QtGui.QSpacerItem(2, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_11.addItem(spacerItem, 1, 1, 1, 1)
        self.pushButtonEnd = QtGui.QPushButton(self.widget_4)
        self.pushButtonEnd.setObjectName("pushButtonEnd")
        self.gridLayout_11.addWidget(self.pushButtonEnd, 2, 1, 1, 1)
        self.pushButtonExit = QtGui.QPushButton(self.widget_4)
        self.pushButtonExit.setObjectName("pushButtonExit")
        self.gridLayout_11.addWidget(self.pushButtonExit, 3, 1, 1, 1)
        self.pushButtonAddImage = QtGui.QPushButton(self.widget_4)
        self.pushButtonAddImage.setObjectName("pushButtonAddImage")
        self.gridLayout_11.addWidget(self.pushButtonAddImage, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.widget_4, 1, 1, 1, 1)
        self.widgetFormButtons = QtGui.QWidget(self.centralwidget)
        self.widgetFormButtons.setObjectName("widgetFormButtons")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.widgetFormButtons)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.pushButtonPochki = QtGui.QPushButton(self.widgetFormButtons)
        self.pushButtonPochki.setCheckable(False)
        self.pushButtonPochki.setObjectName("pushButtonPochki")
        self.verticalLayout_2.addWidget(self.pushButtonPochki)
        self.pushButtonMochevoi = QtGui.QPushButton(self.widgetFormButtons)
        self.pushButtonMochevoi.setCheckable(False)
        self.pushButtonMochevoi.setObjectName("pushButtonMochevoi")
        self.verticalLayout_2.addWidget(self.pushButtonMochevoi)
        self.pushButtonPredstatelnaya = QtGui.QPushButton(self.widgetFormButtons)
        self.pushButtonPredstatelnaya.setCheckable(False)
        self.pushButtonPredstatelnaya.setObjectName("pushButtonPredstatelnaya")
        self.verticalLayout_2.addWidget(self.pushButtonPredstatelnaya)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        self.gridLayout.addWidget(self.widgetFormButtons, 0, 1, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        Obsledovanie.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(Obsledovanie)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1052, 25))
        self.menubar.setObjectName("menubar")
        Obsledovanie.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(Obsledovanie)
        self.statusbar.setObjectName("statusbar")
        Obsledovanie.setStatusBar(self.statusbar)

        self.retranslateUi(Obsledovanie)
        self.stackedWidgetMain.setCurrentIndex(-1)
        QtCore.QObject.connect(self.pushButtonEnd, QtCore.SIGNAL("clicked()"), self.widgetFormButtons.hide)
        QtCore.QMetaObject.connectSlotsByName(Obsledovanie)

    def retranslateUi(self, Obsledovanie):
        Obsledovanie.setWindowTitle(QtGui.QApplication.translate("Obsledovanie", "Составление отчета обследования", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonEnd.setText(QtGui.QApplication.translate("Obsledovanie", "Завершить \n"
"составление отчета", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonExit.setText(QtGui.QApplication.translate("Obsledovanie", "Выход", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonAddImage.setText(QtGui.QApplication.translate("Obsledovanie", "Добавить\n"
" изображение", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonPochki.setText(QtGui.QApplication.translate("Obsledovanie", "Почки", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonMochevoi.setText(QtGui.QApplication.translate("Obsledovanie", "Мочевой пузырь", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonPredstatelnaya.setText(QtGui.QApplication.translate("Obsledovanie", "Предстательная\n"
" железа", None, QtGui.QApplication.UnicodeUTF8))

