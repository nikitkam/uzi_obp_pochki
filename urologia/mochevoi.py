#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import base64

from PySide import QtGui, QtCore

from ObsledovanieDesign   import Ui_Obsledovanie

from mochevoi_widget import MochevoiWidget



# если папка _common_parts не в sys.path
try:
    import _common_parts.common as common
except ImportError:
    import find_common
    import _common_parts.common as common

from _common_parts.final_report_widget import FinalReportWidget


class Mochevoi(QtGui.QMainWindow):
    def __init__(self,store):
        super(Mochevoi, self).__init__()
        self.insertSettings = store
        self.initUI()

    def initUI(self):
        self.ui = Ui_Obsledovanie()
        self.ui.setupUi(self)
        self.endReportFlag = False

        # add widgets 
        self.mochevoi_widget = MochevoiWidget()
        self.ui.stackedWidgetMain.addWidget(self.mochevoi_widget)
        self.mochevoi_widget.updateReportPreview()

        self.final_report = FinalReportWidget(self.insertSettings)
        self.ui.stackedWidgetMain.addWidget(self.final_report)

        self.ui.pushButtonPochki.hide()
        self.ui.pushButtonPredstatelnaya.hide()
        self.ui.pushButtonMochevoi.hide()
        

        self.ui.pushButtonEnd.clicked.connect(self.onPushButtonEndClicked)
        self.ui.pushButtonExit.clicked.connect(self.close)
        self.ui.pushButtonAddImage.clicked.connect(self.addImageAction)
        # hide add image button
        self.ui.pushButtonAddImage.hide()

    def onPushButtonEndClicked(self):
        if self.endReportFlag == False :
            #show add image button
            self.ui.pushButtonAddImage.show()

            self.endReportFlag = True
            self.ui.stackedWidgetMain.setCurrentWidget(self.final_report)
            self.ui.pushButtonEnd.setText(QtGui.QApplication.translate("Obsledovanie", "Отправить \n" "на печать", None, QtGui.QApplication.UnicodeUTF8))
            htmlToInsert =   self.final_report.ui.textBrowser.toHtml()

            # замена идентификаторов в шаблоне типа shapkaString на значения из настроек и
            # значения, введенные в inter_dialog пользователем
            htmlToInsert = common.process_html_template(htmlToInsert, self.insertSettings)
            self.final_report.ui.textBrowser.clear()
            self.final_report.ui.textBrowser.insertHtml(htmlToInsert)            
            htmlToInsert =  self.mochevoi_widget.ui.textBrowserReport.toHtml()
            self.final_report.ui.textBrowser.insertHtml(htmlToInsert)
            htmlToInsert =  self.final_report.ui.textBrowserDoctorName.toHtml()
            htmlToInsert = (htmlToInsert.replace("doctorNameString", self.insertSettings['DoctorName']))
            self.final_report.ui.textBrowser.insertHtml(htmlToInsert)
        else :
            # close the file and open libre office to work with new generated report file
            print("Printing the file!")
            printer = QtGui.QPrinter()
            self.final_report.ui.textBrowser.print_(printer)


    def addImageAction(self):
        img_filename = QtGui.QFileDialog.getOpenFileName()[0]
        if not os.path.exists(img_filename):
            print('Warning: %s is not found' % img_filename)
            encoded_string = base64.b64encode('error')
        else :
            with open(img_filename, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                
        imageHtml = '<html><p><img src="data:image/jpeg;base64,' + encoded_string + '" name="Image1" align="left" border="0"></p></html>'
        htmlToInsert =  self.final_report.ui.textBrowser.toHtml()
        htmlToInsert = htmlToInsert + imageHtml
        self.final_report.ui.textBrowser.clear()
        self.final_report.ui.textBrowser.insertHtml(htmlToInsert)

def main():
    inpDict = []
    app = QtGui.QApplication(sys.argv)
    main_window = Mochevoi(inpDict)
    main_window.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
