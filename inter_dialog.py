#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from PySide import QtCore, QtGui
from PySide.QtCore import QRegExp

from InterDialogDesign import Ui_InterDialog

import _common_parts.common as common


class InterDialog(QtGui.QDialog):
    accepted = QtCore.Signal(dict)
    def __init__(self, settings):
        super(InterDialog, self).__init__()
        self._settings = settings
        self.VISUALIZATION_CB_KEY = 'VisualizationCBValues'
        self.initUI()
        self.ui.DoctorEditL.setText(settings['DoctorName'])
        self.ui.UziEditL.setText(settings['Uzi'])


    def onOkPBClicked(self):
        patient_fio = self.ui.PatientLE.text()
        self._settings['patient_fio'] = patient_fio
        patient_sex = self.ui.SexComboBox.currentText()
        self._settings['patient_sex'] = patient_sex
        patient_age = self.ui.AgeLE.text()
        self._settings['patient_age'] = patient_age
        protocol_num = self.ui.ProtocolNumLE.text()
        self._settings['protocol_num'] = protocol_num
        visualization = self.ui.VisualizationCB.currentText()
        self._settings['visualization'] = visualization

        self.SaveCBSettings(self.ui.VisualizationCB)
        self.accepted.emit(self._settings)
        self.close()

    def onCancelPBClicked(self):
        CANCEL_PRESSED_STATUS = 1
        self.done(CANCEL_PRESSED_STATUS)


    def SaveCBSettings(self, cb):
        # сохраняем значения строк комбо-бокса (вместе с только-что добавленным)
        self._settings[self.VISUALIZATION_CB_KEY] = common.get_combobox_values(cb)
        self._settings.save(silent=True)

    def onAddVisPBClicked(self):
        """ Добавление условий визуализации в комбо-бокс"""
        text, ok = QtGui.QInputDialog.getText(self, u'Добавление нового значения',
            u'Условия визуализации')
        if not ok:
            return

        cb = self.ui.VisualizationCB
        BOTTOM_INDEX = cb.count()
        cb.insertItem(BOTTOM_INDEX, text)
        cb.setCurrentIndex(BOTTOM_INDEX)
        self.SaveCBSettings(cb)


    def onRemoveVisPBClicked(self):
        """ Удаление условий визуализации из комбо-бокса"""
        cb = self.ui.VisualizationCB
        ci = cb.currentIndex()
        cb.removeItem(ci)
        self.SaveCBSettings(cb)


################################ Ui ################################

    def InstallInputValidators(self):
        names_validator = QtGui.QRegExpValidator(QRegExp(r"[\w- ]{0,64}"), self)
        self.ui.PatientLE.setValidator(names_validator)

        # пока тут будет год рождения.
        age_validator  = QtGui.QRegExpValidator(QRegExp(r"[\d]{0,4}"), self)
        self.ui.AgeLE.setValidator(age_validator)


    # TODO: убрать дублирование кода с main_menu_window
    def FillVisualisationCB(self, combo_box, visualization_dict):
        """ Заполнение комбо-бокса 'Условия визуализации' """
        if type(visualization_dict) != dict:
            print('error: typecheck failed!')
            return

        cb = combo_box
        # чистим ComboBox
        cb.clear()
        # заполняем
        for index, text in visualization_dict.iteritems():
            cb.insertItem(index, text)


    def initUI(self):
        self.ui = Ui_InterDialog()
        self.ui.setupUi(self)
        self.InstallInputValidators()

        # заполняем комбо-бокс "Условия визуализации"
        if self.VISUALIZATION_CB_KEY in self._settings:
            cb_values = self._settings[self.VISUALIZATION_CB_KEY]
            self.FillVisualisationCB(self.ui.VisualizationCB, cb_values)
        self.ui.OkPB.clicked.connect(self.onOkPBClicked)
        self.ui.CancelPB.clicked.connect(self.onCancelPBClicked)
        self.ui.AddVisPB.clicked.connect(self.onAddVisPBClicked)
        self.ui.RemoveVisPB.clicked.connect(self.onRemoveVisPBClicked)


def main():
    app = QtGui.QApplication(sys.argv)
    store_ = dict()
    store_['DoctorName'] = u'Ходоркин Владимир'
    store_['Uzi']        = u'Some Aloka model'
    dialog = InterDialog(store_)
    sys.exit(dialog.exec_())


if __name__ == '__main__':
    main()
