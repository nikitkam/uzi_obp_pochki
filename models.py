#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, types, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Sequence


__DeclBase = None

def _getDeclBase():
    global __DeclBase
    if __DeclBase == None:
        __DeclBase = declarative_base()
    return __DeclBase

class Examination(_getDeclBase()):
    __tablename__ = 'examination'

    ex_id        = Column(types.Integer, Sequence('examination_id_seq'), primary_key=True)

    ex_date        = Column(types.Date(), nullable=True)
    ex_time        = Column(types.Time(), nullable=True)
    ex_type        = Column(types.Unicode(8), nullable=True)

    doctor_name    = Column(types.Unicode(64), nullable=True)
    patient_name   = Column(types.Unicode(64), nullable=True)
    sex            = Column(types.Unicode(1), nullable=True)

    report_html_blob  = Column(types.LargeBinary, nullable=True)

    def __repr__(self):
        #repr_ex_type   = self.ex_type.encode('utf-8') if self.ex_type else None
        return "<Examination(doctor_name=%s, patient_name=%s, date=%s, examination_type=%s)>"  % \
                    (self.doctor_name.encode('utf-8'), self.patient_name.encode('utf-8'), str(self.ex_date), self.ex_type.encode('utf-8'))



